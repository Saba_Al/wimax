#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "assert.h"
#include "ns3/service-flow.h"
#include <iostream>
#include <sstream>
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/cost-Erceg.h"
#include "../model/bandwidth-manager.h"
#include "ns3/ipv4-flow-probe.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/flow-id-tag.h"
#include "ns3/udp-echo-helper.h"
#define MAX_USERS 3005
NS_LOG_COMPONENT_DEFINE("PLI");
#define FLOW_NUM 14


int setup_udp_onoff(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice> ss[],
		NodeContainer bsNode, Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size,
		std::string data_rate, int startPort, WimaxHelper wimaxAlcona,
		ServiceFlow::SchedulingType sfType,
		int req_latency = 0) {

		/*PacketSinkHelper* udpServer[nbSS];
		ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
		OnOffHelper* udpClient[nbSS];
		ApplicationContainer* clientApps = new ApplicationContainer[nbSS];*/

	  UdpEchoServerHelper* udpServer[nbSS];
		UdpEchoClientHelper* udpClient[nbSS];
		ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
		ApplicationContainer* clientApps = new ApplicationContainer[nbSS];

		/*echoServer.Install(bsNode);
		for (int i=0;i<nbSS;i++){
			echoClient[i].Install(ssNodes.Get(i));
		}
*/
		int port = 100;
			uint32_t startTime = 2000;
			 //double stopTime = 7;*/
			int tmpfid = fid;
			for (int i = 0; i < nbSS; i++) {
				cout << "nbSS: " << nbSS << endl;
				cout << "setup_udp_onoff, i: " << i << ", SchedulingType: " << sfType << endl;
				port = startPort + ((i + 1) * 10);
				Address sinkLocalAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
				udpServer[i] = new UdpEchoServerHelper(port);
				serverApps[i] = udpServer[i]->Install(bsNode.Get(0));
				serverApps[i].Start(Seconds(0.0));
				serverApps[i].Stop(Seconds(duration)); //duration

				udpClient[i] = new UdpEchoClientHelper(BSinterface.GetAddress(0),port);
				//	AddressValue remoteAddress(
//						InetSocketAddress(BSinterface.GetAddress(0), port));
		//		udpClient[i]->SetAttribute("RemoteAddress", BSinterface.GetAddress(0));
		//		udpClient[i]->SetAttribute("Port",port);
			//	udpClient[i]->SetConstantRate(DataRate(data_rate));
				udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
			//	udpClient[i]->
				udpClient[i]->SetAttribute("Interval",TimeValue(MilliSeconds((pkt_size*8000)/(1024*std::atoi(data_rate.c_str())))));
				cout << "data rate: " << 1024*std::atoi(data_rate.c_str()) << ", packet size: " << pkt_size << ", interval: " << MilliSeconds((pkt_size*8000)/(1024*std::atoi(data_rate.c_str()))) << endl;
			 // NS_ASSERT(false);
			//	udpClient[i]->UdpEchoClientHelper(BSinterface.GetAddress(0),port)-
				udpClient[i]->SetAttribute("MaxPackets",UintegerValue(100));

				udpClient[i]->SetAttribute("FlowId", UintegerValue(tmpfid));
			//	udpClient[i]->SetAttribute("OnTime", StringValue(on_time));
			//	udpClient[i]->SetAttribute("OffTime", StringValue(off_time));

				cout << "cc" << endl;
				cout << "size: " << ssNodes.GetN() << endl;
				clientApps[i] = udpClient[i]->Install(ssNodes.Get(i));
			//	clientApps[i]->GetObject<UdpEchoClient>()->SetDataSize(64);
				clientApps[i].Start(MilliSeconds(startTime));
				clientApps[i].Stop(Seconds(duration));
				startTime = startTime + 100; //100; //95;
				//stopTime = startTime + 5;
				tmpfid++;
			}

			startTime = 2000;
			for (int i = 0; i < nbSS; i++) {
			//	cout << "setup_udp_onoff fid: " << fid << "service flow# " << i << endl;
				IpcsClassifierRecord ulClassifierBe(SSinterfaces.GetAddress(i),
						Ipv4Mask("255.255.255.255"), BSinterface.GetAddress(0),
						Ipv4Mask("255.255.255.255"), 0, 65000, startPort + ((i + 1) * 10),
						startPort + ((i + 1) * 10), 17,1);

				ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
						ServiceFlow::SF_DIRECTION_UP, sfType, ulClassifierBe);

				if (sfType == ServiceFlow::SF_TYPE_UGS ) {
					ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
					ulServiceFlowBe.SetMaximumLatency(req_latency);
				//	ulServiceFlowBe.SetSduSize((uint8_t)pkt_size);
					ulServiceFlowBe.SetMaxTrafficBurst(pkt_size);
					ulServiceFlowBe.SetStartTime(startTime);
					std::cout<< "data_rate" << std::atoi(data_rate.c_str()) << ", interval: " << (pkt_size*8000)/(1024*std::atoi(data_rate.c_str())) << std::endl;
					ulServiceFlowBe.SetUnsolicitedGrantInterval((pkt_size*8000)/(1024*std::atoi(data_rate.c_str())));//
				//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
					ulServiceFlowBe.SetToleratedJitter(10);
					ulServiceFlowBe.SetTrafficPriority(1);
					ulServiceFlowBe.SetSfid(fid);

				} else if (sfType == ServiceFlow::SF_TYPE_RTPS) {
					ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);//2
					ulServiceFlowBe.SetMaximumLatency(req_latency);
					//ulServiceFlowBe.SetSduSize(49);
				//	ulServiceFlowBe.SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
					ulServiceFlowBe.SetUnsolicitedPollingInterval(10);
				//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
					ulServiceFlowBe.SetToleratedJitter(10);
					ulServiceFlowBe.SetTrafficPriority(1);
					} else if (sfType == ServiceFlow::SF_TYPE_NRTPS) {
						ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);//2
					}
				ss[i]->AddServiceFlow(ulServiceFlowBe);
				startTime = startTime + 100;
		/*		if (sfType == ServiceFlow::SF_TYPE_UGS && fid != 1)
				{
					ServiceFlow *sss = *(ss[i]->GetServiceFlowManager()->GetServiceFlows(ServiceFlow::SF_TYPE_UGS).begin());
					cout<<"mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm, sfid:     " << sss->GetSfid() << endl;
				}*/
			}

			startTime = 2000;
				//classifier
				for (int i = 0; i < nbSS; i++) {
					IpcsClassifierRecord	DlClassifier = IpcsClassifierRecord(Ipv4Address("0.0.0.0"),
							Ipv4Mask("0.0.0.0"), SSinterfaces.GetAddress(i),
							Ipv4Mask("255.255.255.255"), 0, 65000,0 ,//startPort + 10 * (i + 1)
							65000, 17, 1);//startPort + 10 * (i + 1)
				ServiceFlow	DlServiceFlow = wimaxAlcona.CreateServiceFlow(ServiceFlow::SF_DIRECTION_DOWN,
							sfType/*ServiceFlow::SF_TYPE_RTPS*/, DlClassifier);
				if (sfType == ServiceFlow::SF_TYPE_UGS) {
					DlServiceFlow.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
					DlServiceFlow.SetMaximumLatency(req_latency);
				//	ulServiceFlowBe.SetSduSize((uint8_t)pkt_size);
					DlServiceFlow.SetMaxTrafficBurst(pkt_size);
					DlServiceFlow.SetStartTime(startTime);
					std::cout<< "data_rate" << std::atoi(data_rate.c_str()) << ", interval: " << (pkt_size*8000)/(1024*std::atoi(data_rate.c_str())) << std::endl;
					DlServiceFlow.SetUnsolicitedGrantInterval((pkt_size*8000)/(1024*std::atoi(data_rate.c_str())));//
				//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
					DlServiceFlow.SetToleratedJitter(10);
					DlServiceFlow.SetTrafficPriority(1);
					DlServiceFlow.SetSfid(fid);
				}
				/*	if (sfType == ServiceFlow::SF_TYPE_UGS) {
					  //DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
			//			DlServiceFlow[i].SetMaximumLatency(req_latency);
					//	DlServiceFlow[i].SetSduSize(49);
					//	DlServiceFlow[i].SetStartTime(startTime);
						DlServiceFlow.SetMaximumLatency(req_latency);
						DlServiceFlow.SetUnsolicitedGrantInterval(1);//(pkt_size*8000)/(1024*std::atoi(data_rate.c_str()))
					//	DlServiceFlow[i].SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
						DlServiceFlow.SetRequestTransmissionPolicy(0x00110000);

						DlServiceFlow.SetToleratedJitter(10);
						DlServiceFlow.SetTrafficPriority(1);
					}*/ else if (sfType == ServiceFlow::SF_TYPE_RTPS) {
					//	DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
						DlServiceFlow.SetMaximumLatency(req_latency);
						//DlServiceFlow[i].SetSduSize(49);
						DlServiceFlow.SetUnsolicitedPollingInterval(10);
						//DlServiceFlow[i].SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str())*1024*2);
						DlServiceFlow.SetRequestTransmissionPolicy(0x00110100);
						DlServiceFlow.SetToleratedJitter(10);
						DlServiceFlow.SetTrafficPriority(1);
						//DlServiceFlow[i].SetModulation(WimaxPhy::MODULATION_TYPE_QAM64_23);
					}/*else if (sfType == ServiceFlow::SF_TYPE_NRTPS) {
						DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
						DlServiceFlow[i].SetMaximumLatency(req_latency);
						//DlServiceFlow[i].SetSduSize(49);
					//	DlServiceFlow[i].SetUnsolicitedPollingInterval(10);
						DlServiceFlow[i].SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str())*1024);
					//	DlServiceFlow[i].SetRequestTransmissionPolicy(0x00110100);
					//	DlServiceFlow[i].SetToleratedJitter(10);
						DlServiceFlow[i].SetTrafficPriority(2);
						//DlServiceFlow[i].SetModulation(WimaxPhy::MODULATION_TYPE_QAM64_23);
					}*/
					//m_minReservedTrafficRate = (data_rate);
					startTime = startTime + 100;
					ss[i]->AddServiceFlow(DlServiceFlow);
				}

			return port;
		}


/*int setup_udp_onoff(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice> ss[],
		NodeContainer bsNode, Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size,
		std::string data_rate, int startPort, WimaxHelper wimaxAlcona,
		ServiceFlow::SchedulingType sfType,
		int req_latency = 0) {

	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	uint32_t startTime = 2000;
	 //double stopTime = 7;
	int tmpfid = fid;
	for (int i = 0; i < nbSS; i++) {
		cout << "nbSS: " << nbSS << endl;
		cout << "setup_udp_onoff, i: " << i << ", SchedulingType: " << sfType << endl;
		port = startPort + ((i + 1) * 10);
		Address sinkLocalAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory",
				sinkLocalAddress);
		serverApps[i] = udpServer[i]->Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(duration)); //duration

		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory", Address());
		AddressValue remoteAddress(
				InetSocketAddress(BSinterface.GetAddress(0), port));
		udpClient[i]->SetAttribute("Remote", remoteAddress);
		udpClient[i]->SetConstantRate(DataRate(data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId", UintegerValue(tmpfid));
		udpClient[i]->SetAttribute("OnTime", StringValue(on_time));
		udpClient[i]->SetAttribute("OffTime", StringValue(off_time));

		cout << "cc" << endl;
		cout << "size: " << ssNodes.GetN() << endl;
		clientApps[i] = udpClient[i]->Install(ssNodes.Get(i));
		clientApps[i].Start(MilliSeconds(startTime));
		clientApps[i].Stop(Seconds(duration));
		startTime = startTime + 100; //100; //95;
		//stopTime = startTime + 5;
		tmpfid++;
	}

	startTime = 2000;
	for (int i = 0; i < nbSS; i++) {
	//	cout << "setup_udp_onoff fid: " << fid << "service flow# " << i << endl;
		IpcsClassifierRecord ulClassifierBe(SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + ((i + 1) * 10),
				startPort + ((i + 1) * 10), 17,1);

		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, sfType, ulClassifierBe);

		if (sfType == ServiceFlow::SF_TYPE_UGS ) {
			ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
			ulServiceFlowBe.SetMaximumLatency(req_latency);
		//	ulServiceFlowBe.SetSduSize((uint8_t)pkt_size);
			ulServiceFlowBe.SetMaxTrafficBurst(pkt_size);
			ulServiceFlowBe.SetStartTime(startTime);
			std::cout<< "data_rate" << std::atoi(data_rate.c_str()) << ", interval: " << (pkt_size*8000)/(1024*std::atoi(data_rate.c_str())) << std::endl;
			ulServiceFlowBe.SetUnsolicitedGrantInterval((pkt_size*8000)/(1024*std::atoi(data_rate.c_str())));//
		//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
			ulServiceFlowBe.SetToleratedJitter(10);
			ulServiceFlowBe.SetTrafficPriority(1);
			ulServiceFlowBe.SetSfid(fid);

		} else if (sfType == ServiceFlow::SF_TYPE_RTPS) {
			ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);//2
			ulServiceFlowBe.SetMaximumLatency(req_latency);
			//ulServiceFlowBe.SetSduSize(49);
		//	ulServiceFlowBe.SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
			ulServiceFlowBe.SetUnsolicitedPollingInterval(10);
		//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
			ulServiceFlowBe.SetToleratedJitter(10);
			ulServiceFlowBe.SetTrafficPriority(1);
			} else if (sfType == ServiceFlow::SF_TYPE_NRTPS) {
				ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);//2
			}
		ss[i]->AddServiceFlow(ulServiceFlowBe);
		startTime = startTime + 100;
//		if (sfType == ServiceFlow::SF_TYPE_UGS && fid != 1)
//		{
//			ServiceFlow *sss = *(ss[i]->GetServiceFlowManager()->GetServiceFlows(ServiceFlow::SF_TYPE_UGS).begin());
//			cout<<"mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm, sfid:     " << sss->GetSfid() << endl;
//		}
	}
	return port;
}*/

void NodeConfigurationCommonSetupUDP(NodeContainer &bsNodes, string usecase, WimaxHelper &wimax,
		int numUsers, int &created_nodes, double areaRadius,
		InternetStackHelper &stack, Ipv4AddressHelper &address,
		WimaxHelper::SchedulerType scheduler,
		WimaxPhy::ModulationType maxModulation, double frameDur, uint32_t channelBandwidth, /*inputs*/
		NodeContainer &ssNodes, Ptr<SubscriberStationNetDevice> *ss,
		Ipv4InterfaceContainer &SSinterfaces /*outputs*/) {
	/*------------------------------*/
	//For each application, we need to create common variables here (common part between UL and DL of each application):
	//ssNodes = new NodeContainer;
	NS_LOG_UNCOND ("Assigning IP Adresss for UDP nodes " << created_nodes << " to " << created_nodes+numUsers-1 << "...");

	ssNodes.Create(numUsers);
	NetDeviceContainer ssDevs;
	ssDevs = wimax.Install(ssNodes, WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
			WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler);

	stack.Install(ssNodes);
	SSinterfaces = address.Assign(ssDevs);

	//node positions
	MobilityHelper mobility;
	std::ostringstream convert;
	convert << areaRadius;

	mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator", "X",
			StringValue("0.0"), "Y", StringValue("0.0"), "rho",
			StringValue(convert.str())); //rho = 5000, My_Ro   //DoubleValue(areaRadius)); //
	vector<double> ssAntennaHeightVector;
	vector<double> ssTxFreqVector;
	vector<string> ssNodeNameVector;
	for (int j = 0; j < numUsers; j++) {
		ssAntennaHeightVector.push_back(12);
		ssTxFreqVector.push_back(1827);
		ostringstream name;
		name << usecase << "_Node_" << j;
		ssNodeNameVector.push_back(name.str());
	}
	mobility.m_antennaHeightVector = ssAntennaHeightVector;
	mobility.m_nodeNameVector = ssNodeNameVector;
	mobility.m_txFreqVector = ssTxFreqVector;
	mobility.Install(ssNodes);


	double txPowerDbm = 24;
	//Cost231PropagationLossModel propModel;
	ErcegPropagationLossModel propModel;

	for (int i = 0; i < numUsers; i++) {
		ss[i] = ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>();
		Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(ss[i]->GetPhy());
	  myPhy->SetTxPower(txPowerDbm);
	  myPhy->SetTxGain(12);
	  myPhy->SetRxGain(12);


		Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(
				ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>()->GetPhy());

		ssPhy->SetChannelBandwidth(channelBandwidth);
		DoubleValue noiseFigure;
		ssPhy->GetAttribute("NoiseFigure",noiseFigure);
		ssPhy->ActivateLoss(false);
		NS_LOG_UNCOND("ssNode " << i << " position is: " << ssNodes.Get(i)->GetObject<MobilityModel>()->GetPosition());
		double distance = ssNodes.Get(i)->GetObject<MobilityModel>()->GetDistanceFrom(bsNodes.Get(0)->GetObject<MobilityModel>());
		NS_LOG_UNCOND("Distance of ssNode " << i << "to BS is: " << distance);
		double rxPower = txPowerDbm + propModel.GetLoss (ssNodes.Get(i)->GetObject<MobilityModel>(), bsNodes.Get(0)->GetObject<MobilityModel>());
		double Nwb = -174 + 10*std::log(ssPhy->GetBandwidth())/2.303 + noiseFigure.Get();
		double SNR = rxPower - Nwb;
		NS_LOG_DEBUG("receive power: " << rxPower);

		NS_LOG_DEBUG("SNR Value for " << ssNodes.Get(i)->GetObject<MobilityModel>()->m_nodeName << " is " << SNR);

	  //FARIBA: Find the SNR Thresholds from MATLAB and assign the corresponding SNR:
		if (SNR < 6.4345 || maxModulation==WimaxPhy::MODULATION_TYPE_BPSK_12) //2.3045 < SNR &&
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_BPSK_12);
		}
		else if ( (6.4345 <= SNR && SNR < 9.4939) || maxModulation==WimaxPhy::MODULATION_TYPE_QPSK_12)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_12);
		}
		else if ( (9.4939 <= SNR && SNR < 12.2789) || maxModulation==WimaxPhy::MODULATION_TYPE_QPSK_34)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_34);
		}
		else if ( (12.2789 <= SNR && SNR < 15.500) || maxModulation==WimaxPhy::MODULATION_TYPE_QAM16_12)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_12);
		}
		else if ( (15.500 <= SNR && SNR < 20.300) || maxModulation==WimaxPhy::MODULATION_TYPE_QAM16_34)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_34);
		}
		else if ( (20.300 <= SNR && SNR < 21.500) || maxModulation==WimaxPhy::MODULATION_TYPE_QAM64_23)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_23);
		}
		else //if ( 21.500 <= SNR )
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_34);
		}
		NS_LOG_DEBUG("Modulation" << ss[i]->GetModulationType() );

		ssPhy->SetFrameDuration(Seconds(frameDur));
	}
	created_nodes = created_nodes + numUsers;
	NS_LOG_UNCOND("SSinterfaces 0:" << SSinterfaces.GetAddress(0));


}
int main(int argc, char *argv[]) {
  bool enableUplink = false, enableDownlink = false;
	int duration = 3, schedType = 0, port = 100;
	int numUsers = 2;
	int maxModulationIndex = 1;
	int channelBandwidth = 5000000;
	double frameDur = 0.005;
	double UL2DLRatio = 1.5; //1.5
	double areaRadius = 2.0; //0.56434; //2 //kilometer
	int fidUGSUL = 0;//, fidUGSDL = 1;
	int START_DL_FID = 7;
	int END_DL_FID = 13;
	double bsTxPower = 36;
	int pktSize = 64;
	string flowNames[FLOW_NUM] = {"UGS","BE", "UGS", "BE"};


//	RngSeedManager::SetSeed(12345);

	CommandLine cmd;
	cmd.AddValue("scheduler", "type of scheduler to use with the network devices",schedType);
	cmd.AddValue("maxmod", "Maximum allowed modulation", maxModulationIndex);
	cmd.AddValue("duration", "duration of the simulation in seconds", duration);
	cmd.AddValue("ulrat", "Uplink to Downlink Ratio (double)", UL2DLRatio);
	cmd.AddValue("chbw", "Channel bandwidth in Hz", channelBandwidth);
	cmd.AddValue("framedur", "Frame duration in seconds.", frameDur);
	cmd.AddValue("NW", "Number of nodes.", numUsers);
	cmd.AddValue("DL", "Enable DL transmission?", enableDownlink);
	cmd.AddValue("UL", "Enable UL transmission?", enableUplink);
	cmd.AddValue("rad", "Cell Radius (km).", areaRadius);
	cmd.AddValue("pkt", "Packet Size (Byte).", pktSize);
	cmd.Parse(argc, argv);

//	LogComponentEnable("UplinkSchedulerSimple", LOG_LEVEL_DEBUG);
	LogComponentEnable("PLI",LOG_LEVEL_DEBUG);
	LogComponentEnable("UdpEchoClientApplication", LOG_LEVEL_INFO);
	LogComponentEnable("UdpEchoServerApplication",LOG_LEVEL_ALL);
	LogComponentEnable("Buffer", LOG_LEVEL_DEBUG);
	LogComponentEnable("BSScheduler", LOG_LEVEL_INFO);


	WimaxHelper::SchedulerType scheduler;
		switch (schedType) {
		case 0:
			scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
			break;
		case 1:
			scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
			break;
		case 2:
			scheduler = WimaxHelper::SCHED_TYPE_RTPS;
			break;
		default:
			scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
			break;
		}

		WimaxPhy::ModulationType maxModulation;
		switch (maxModulationIndex){
			//values are:
		case 0:
			maxModulation = WimaxPhy::MODULATION_TYPE_BPSK_12;
			break;
		case 1:
			maxModulation = WimaxPhy::MODULATION_TYPE_QPSK_12;
			break;
		case 2:
			maxModulation = WimaxPhy::MODULATION_TYPE_QPSK_34;
			break;
		case 3:
			maxModulation = WimaxPhy::MODULATION_TYPE_QAM16_12;
			break;
		case 4:
			maxModulation = WimaxPhy::MODULATION_TYPE_QAM16_34;
			break;
		case 5:
			maxModulation = WimaxPhy::MODULATION_TYPE_QAM64_23;
			break;
		case 6:
			maxModulation = WimaxPhy::MODULATION_TYPE_QAM64_34;
			break;
		case 7:
			maxModulation = WimaxPhy::MODULATION_TYPE_QAM64_56;
			break;
		}

		WimaxHelper wimax;
		NodeContainer bsNodes;
		bsNodes.Create(1);
		int created_nodes = 1;

		NetDeviceContainer bsDevs;
		bsDevs = wimax.Install(bsNodes, WimaxHelper::DEVICE_TYPE_BASE_STATION,
				WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler);
		Ptr<BaseStationNetDevice> bs;
		bs = bsDevs.Get(0)->GetObject<BaseStationNetDevice>();
		bs->GetBandwidthManager()->SetULtoDLRatio(UL2DLRatio);
		bs->GetBandwidthManager()->SetSubframeRatio();
		//BS Position
		Ptr<ConstantPositionMobilityModel> BSPosition;
		BSPosition = CreateObject<ConstantPositionMobilityModel>();

		BSPosition->SetPosition(Vector(0, 0, 0));
		BSPosition->m_txFreq = 1827;
		BSPosition->m_antennaHeight = 30;
		BSPosition->m_nodeName = "The_BS";
		Ptr<SimpleOfdmWimaxPhy> bsPhy = 	DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(
						bsDevs.Get(0)->GetObject<BaseStationNetDevice>()->GetPhy());
		bsPhy->SetTxPower(bsTxPower);
		bsPhy->SetTxGain(16);
		bsPhy->SetRxGain(16);
		bsPhy->SetChannelBandwidth(channelBandwidth);
		bsPhy->SetFrameDuration(Seconds(frameDur));

		bsNodes.Get(0)->AggregateObject(BSPosition);
		//BSPosition.m_txFreqVector = 1827;

		Ptr<ConstantPositionMobilityModel> bp = bsNodes.Get(0)->GetObject<ConstantPositionMobilityModel>();
		cout << "BS Location is: " << bp->GetPosition() << endl;

		NodeContainer ssNodes;
		Ptr<SubscriberStationNetDevice> ss[numUsers];
		Ipv4InterfaceContainer SSinterfaces;

		InternetStackHelper stack;
		stack.Install(bsNodes);
		Ipv4AddressHelper address;
		address.SetBase("10.1.1.0", "255.255.255.0");
		Ipv4InterfaceContainer BSinterface = address.Assign(bsDevs);
		NodeConfigurationCommonSetupUDP(bsNodes, "UGS", wimax, numUsers, created_nodes,
				areaRadius, stack, address, scheduler, maxModulation, frameDur, channelBandwidth, ssNodes, ss,
				SSinterfaces);

		if (enableUplink) {
				//NodeConfigurationCommonSetup(wimax,5,stack,address,scheduler,ssNodes,ss,SSinterfaces);
				//Control is TCP:
		//		appnumBeforeWASAUL = bsNodes.Get(0)->GetNApplications();
				if (numUsers > 0) {
					NS_LOG_UNCOND("Setting up UL UDP for UGS flows");
					//cout<< numWASAUsers << std::endl;
			//		NS_ASSERT(false);
					port = setup_udp_onoff(numUsers, fidUGSUL, ssNodes,
							SSinterfaces, ss, bsNodes, BSinterface, duration,
							"ns3::ConstantRandomVariable[Constant=1]",
							"ns3::ConstantRandomVariable[Constant=0]", pktSize, "128kb/s", port, wimax,
							ServiceFlow::SF_TYPE_UGS,0);
				}
		//		appnumAfterWASAUL = bsNodes.Get(0)->GetNApplications();
		}

		// Flow Monitor
		FlowMonitorHelper flowmonHelper;
		Ptr<FlowMonitor> monitor =  flowmonHelper.InstallAll();
		Simulator::Stop(Seconds(duration + 0.1)); //Seconds(1));
		NS_LOG_UNCOND ("Starting simulation for " << numUsers << " Protection Users, " );
		NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink
				<< ", UL/DL Frame-ratio = " << bs->GetNrUlSymbols() << "/" << (double)bs->GetNrDlSymbols() << "="
				<< bs->GetNrUlSymbols()/(double)bs->GetNrDlSymbols() << ", Max allowed modulation=" << maxModulation);
		Simulator::Run();
		NS_LOG_UNCOND ("Printing statistics for " << numUsers << " Protection Users, ");
		NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink
				<< ", UL/DL Frame-ratio = " << bs->GetNrUlSymbols() << "/" << (double)bs->GetNrDlSymbols() << "="
				<< bs->GetNrUlSymbols()/(double)bs->GetNrDlSymbols() << ", Max allowed modulation=" << maxModulation);

		/******************************GET STATISTICS**********************************************************/

		/**********************  Using FlowMon****************************************************************/

	  /*Ptr<IpcsClassifier> cl =  flowmonHelper.GetClassifier();
	  cl->*/
		//double tptUl=0.0,tptDl=0.0;
		double tpt;
	  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier > (flowmonHelper.GetClassifier ());
	  map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
	  cout << "Stat size: " << stats.size() << endl;
	  for (map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
	  {
	  	//iter->second.
	  	//Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);
	  	//ns3::Ipv4FlowClassifier::FiveTuple t =  classifier->FindFlow(iter->first);
	  	tpt = iter->second.rxPackets * 64*  8.0 /(iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds());
/*	  	if (t.sourceAddress.IsEqual(Ipv4Address("10.1.1.1")))
	  		tptDl+=tpt;
			else if  (t.destinationAddress.IsEqual(Ipv4Address("10.1.1.1")))
				tptUl+=tpt;*/
			//else
				//NS_LOG_UNCOND("!!!The Following FlowMon is neither DL nor UL!!!");
	  	ofstream delayfile("delayhist.xml",ios::out|ios::binary);
	  	Histogram H=iter->second.delayHistogram;
	  	H.SerializeToXmlStream(delayfile,1,"hhhhhh");
	  	delayfile.close();
	  	NS_LOG_UNCOND("FlowMon lastDelay:" << iter->second.lastDelay.GetSeconds());
	  	NS_LOG_UNCOND("Time First Rx Packet: " << iter->second.timeFirstRxPacket.GetSeconds());
	  	NS_LOG_UNCOND("Time Last Rx Packet: " << iter->second.timeLastRxPacket.GetSeconds());
	  	NS_LOG_UNCOND("FlowMon Delay Avg ():" << iter->second.delaySum.GetSeconds() / iter->second.rxPackets);
	  	NS_LOG_UNCOND("FlowMon LosstPackets:" << iter->second.lostPackets);
	  	NS_LOG_UNCOND("FlowMon Received Packets:" << iter->second.rxPackets);
	  	cout << (iter->second.packetsDropped).size() << endl;
	  	for (std::vector<uint32_t>::const_iterator i = (iter->second.packetsDropped).begin(); i != (iter->second.packetsDropped).end(); i++)
	  	cout << "FlowMon Dropped Packets:" << *i << endl;

			/*
			 NS_LOG_UNCOND("FlowMon ID: " << iter->first << " Src Addr: " << t.sourceAddress	<< " Dst Addr: " << t.destinationAddress);
			NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
			NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
			NS_LOG_UNCOND("Lost Packets = " << iter->second.lostPackets);
			NS_LOG_UNCOND("Size of iter->second.packetsDropped = " << iter->second.packetsDropped.size());
			int drnum=0;
			for (vector<unsigned int>::const_iterator dr=iter->second.packetsDropped.begin(); dr!=iter->second.packetsDropped.end();dr++)
			{
				NS_LOG_UNCOND("Dropped Pkts due to reason#" << drnum << "=" << (int)*dr);
				drnum++;
			}*/
			NS_LOG_UNCOND("Throughput: " << tpt / 1024  << /*", " << tptDl/1024 << ", " << tptUl/1024 << */" Kbps");

		}
	  monitor->SerializeToXmlFile ("PLI.flowmon1", false, false);
	  if (enableUplink) {
	  			//Print Statistics:
	  				int reliable_received_all_nodes[FLOW_NUM];
	  				Time Delays_all_nodes[FLOW_NUM];
	  			//	Time ReliableDelays_all_nodes[FLOW_NUM];
	  				int Recv_all_nodes[FLOW_NUM];
	  				double avg_Delays_all_nodes[FLOW_NUM];
	  				//double avg_ReliableDelays_all_nodes[FLOW_NUM];

	  				for (int flowid = 0; flowid < FLOW_NUM; flowid++) {
	  					reliable_received_all_nodes[flowid] = 0;
	  					Delays_all_nodes[flowid] = Time(0.0);
	  					//ReliableDelays_all_nodes[flowid] = Time(0.0);
	  					Recv_all_nodes[flowid] = 0;

	  					avg_Delays_all_nodes[flowid] = 0;
	  				//	avg_ReliableDelays_all_nodes[flowid] = 0;

	  					/*
	  					 min_Delays_all_nodes[flowid] = 0;
	  					 min_ReliableDelays_all_nodes[flowid] = 0;

	  					 var_Delays_all_nodes[flowid] = 0;
	  					 var_ReliableDelays_all_nodes[flowid] = 0;
	  					 */
	  				}
					int Sent[FLOW_NUM]; //, Recv[FLOW_NUM];
					int phyDropped[FLOW_NUM];
					double Recv_Bytes[FLOW_NUM]={0};
				//	int totalUlSent = 0, totalDlSent =0, totalPhyDlDropped=0;
					for (int flowid = 0; flowid < FLOW_NUM; flowid++)
					{
						Sent[flowid] = 0;
						phyDropped[flowid] = 0;
					}
					if (enableUplink) {
						Ptr<Node> myNode = bsNodes.Get(0);
						for (int flowid = 0; flowid < START_DL_FID; flowid++) {
							//NS_LOG_UNCOND(sectNum << 	":" << flowid  );
							reliable_received_all_nodes[flowid] += myNode->reliable_received[flowid];
							//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
							Delays_all_nodes[flowid] += myNode->Delays[flowid];
							//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
							//ReliableDelays_all_nodes[flowid] += myNode->ReliableDelays[flowid];
							//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
							Recv_all_nodes[flowid] += myNode->Recv[flowid];
							Recv_Bytes[flowid] += myNode->receivedBytes[flowid];
							//NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
						}
						//UL Sent for UGS
						int tmpfid = fidUGSUL;
						for (int i = 0; i < numUsers; i++) {
							Sent[tmpfid] += ssNodes.Get(i)->m_Sent/*sentBytes*/[fidUGSUL]; //m_Sent[flowid]
									}
					}
					cout << "flow ID\t\tflow Name\tsent\t\tall rcvd\tall delay\tphy drop\tthroughput"
											<< endl;
						//cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
						int cnt = -1;
						int totalUlRcvd = 0;
						int totalDlRcvd = 0;
					//	int totalULRcvdBytes = 0, totalDLRcvdBytes = 0;
						double Throughput[FLOW_NUM] = {0};
					//	double DLThroughput[FLOW_NUM] = 0;
						if (enableUplink)
							cnt = 0;
						else
							cnt = START_DL_FID;
						for (int flowid = cnt; flowid < END_DL_FID; flowid++) {
		//					avg_ReliableDelays_all_nodes[flowid] =
		//							ReliableDelays_all_nodes[flowid].ToDouble(Time::MS)
		//									/ (double) reliable_received_all_nodes[flowid];
							avg_Delays_all_nodes[flowid] = Delays_all_nodes[flowid].ToDouble(Time::MS)
									/ (double) Recv_all_nodes[flowid];
							Throughput[flowid] = Recv_Bytes[flowid]*8/0.3;

							if (flowid<START_DL_FID)
								totalUlRcvd += Recv_all_nodes[flowid];
							else
								totalDlRcvd += Recv_all_nodes[flowid];

							cout << flowid << "\t\t (" << flowNames[flowid] << ")\t\t" << Sent[flowid] << "\t\t"
								//	<< reliable_received_all_nodes[flowid] << "\t\t"
								//	<< avg_ReliableDelays_all_nodes[flowid] << "\t\t"
									<< Recv_all_nodes[flowid] << "\t\t" << avg_Delays_all_nodes[flowid] << "\t\t"
									<< phyDropped[flowid] << "\t\t" << Throughput[flowid]/1024
									<< endl;
						}
					}

	  return 0;
}
