/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */

//
// Default network topology includes a base station (BS) and 2
// subscriber station (SS).

//      +-----+
//      | SS0 |
//      +-----+
//     10.1.1.1
//      -------
//        ((*))
//
//                  10.1.1.7
//               +------------+
//               |Base Station| ==((*))
//               +------------+
//
//        ((*))
//       -------
//      10.1.1.2
//       +-----+
//       | SS1 |
//       +-----+

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>

NS_LOG_COMPONENT_DEFINE ("WimaxCapacityExample");

using namespace std;
using namespace ns3;

//TCP Traffic
int Uplink_TCP_Traffic(WimaxHelper &wimax, int port,int duration, std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList,
NodeContainer bsNodes, NodeContainer ssNodes , NetDeviceContainer ss, std::string on_time, std::string off_time,
int fid, ServiceFlow::SchedulingType schedulinType,std::string data_rate, uint32_t pktSize) //int numUsers,std::vector<Ipv4InterfaceContainer>
{
	port+=10;
	//Collect an adjacency list of nodes for the p2p topology
	// std::vector<NodeContainer> MonnodeAdjacencyList (numUsers);
	// for(uint32_t i=0; i<MonnodeAdjacencyList.size (); ++i)
	// {
	// MonnodeAdjacencyList[i] = NodeContainer (bsNodes, ssNodes.Get (i));
	// }
	OnOffHelper clientHelper ("ns3::TcpSocketFactory", Address ());
	clientHelper.SetAttribute
	("OnTime", StringValue (on_time));
	clientHelper.SetAttribute
	("OffTime", StringValue (off_time));//off_time
	//clientHelper.SetAttribute("FlowId", UintegerValue (fid));
	//clientHelper.SetAttribute("MaxBytes", UintegerValue (pktSize));
	clientHelper.SetConstantRate (DataRate (data_rate));
	clientHelper.SetAttribute ("PacketSize", UintegerValue (pktSize));

	Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
	PacketSinkHelper sinkHelperUL("ns3::TcpSocketFactory", sinkLocalAddress);
	ApplicationContainer sinkApp = sinkHelperUL.Install (bsNodes);

	sinkApp.Start (Seconds (1.0));
	sinkApp.Stop (Seconds (duration));


	//normally wouldn't need a loop here but the server IP address is different
	//on each p2p subnet
	// ApplicationContainer MonclientApps;
	ApplicationContainer clientApps; // = new ApplicationContainer[numUsers];
	for(uint32_t i=0; i< ssNodes.GetN (); ++i)
	{
	AddressValue remoteAddress (InetSocketAddress (interfaceAdjacencyList[i].GetAddress(0), port));//.GetAddress (0)
	clientHelper.SetAttribute ("Remote", remoteAddress);
	// clientHelper.SetAttribute ("FlowId", UintegerValue (i));
	clientApps.Add(clientHelper.Install (ssNodes.Get (i)));// MonclientApps.Add
	IpcsClassifierRecord UlClassifier = IpcsClassifierRecord(interfaceAdjacencyList[i].GetAddress(1),//.GetAddress (1)
	Ipv4Mask ("255.255.255.252"),
	Ipv4Address ("0.0.0.0"), //BSinterface.GetAddress (0),
	Ipv4Mask ("0.0.0.0"),
	0,
	65000,
	port,
	port,
	17,
	1);
	ServiceFlow UlServiceFlowUgs = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP, schedulinType,UlClassifier);
	ss.Get(i)-> GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (UlServiceFlowUgs);
	//UniformVariable t;
	}
	clientApps.Start (Seconds (2.0)); //t.GetValue()));
	clientApps.Stop (Seconds (duration));
	return port;
}

//void ReadData (std::string filename, int &ssNum, int &bsNum, Ptr<ListPositionAllocator> ssPositionAlloc, Ptr<ListPositionAllocator> bsPositionAlloc,


int main (int argc, char *argv[])
{
  bool verbose = false;
  int port = 100;
  int duration = 10, schedType = 0, modType  = 1;
  //, ssNum=1, bsNum=0, ;

  LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
  LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink

  WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
  WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;

  vector<Node_Data> BS_Info;
  vector<Node_Data> CPEs_Info;
  Xml_Read("20131030PTMP ALCONA WATER TREATMENT PLANT.xml", BS_Info, CPEs_Info);
  NS_LOG_UNCOND("BS_Info.size()=" << BS_Info.size()<< "," << "CPEs_Info.size()=" << CPEs_Info.size());

  for (unsigned int i=0; i < CPEs_Info.size(); i++)
  {
	  cout << "Name:" << CPEs_Info.at(i).location << "x,y,z" << CPEs_Info.at(i).latitude << "," << CPEs_Info.at(i).longtitude << "," << CPEs_Info.at(i).TxAntennaHeight;
  }

  CommandLine cmd;
  cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
  cmd.AddValue ("duration", "duration of the simulation in seconds", duration);
  cmd.AddValue ("verbose", "turn on all WimaxNetDevice log components", verbose);
  cmd.AddValue ("modType", "Type of modulation", modType);

  cmd.Parse (argc, argv);

  NS_LOG_UNCOND("Start...");
  switch (schedType)
    {
    case 0:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
      break;
    case 1:
      scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
      break;
    case 2:
      scheduler = WimaxHelper::SCHED_TYPE_RTPS;
      break;
    default:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
    }

  switch (modType)
    {
    case 0:
      modulation = WimaxPhy::MODULATION_TYPE_BPSK_12;
      break;
    case 1:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_12;
      break;
    case 2:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_34;
      break;
    case 3:
      modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;
      break;
    case 4:
      modulation = WimaxPhy::MODULATION_TYPE_QAM16_34;
      break;
    case 5:
      modulation = WimaxPhy::MODULATION_TYPE_QAM64_23;
      break;
    case 6:
      modulation = WimaxPhy::MODULATION_TYPE_QAM64_34;
      break;
      default:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_12;
    }

  NodeContainer ssNodesAlcona[3];

  vector<Node_Data> CPEs_Alcona[3];

  WimaxHelper wimaxAlcona[3];

  InternetStackHelper stack;
  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  NetDeviceContainer ssDevsAlcona[3];

 // Ipv4InterfaceContainer SSinterfaces_Alcona[3];
  NS_LOG_UNCOND("A");
  for (unsigned int i=0; i<CPEs_Info.size(); i++)
  {
	  CPEs_Alcona[CPEs_Info.at(i).sectNum-1].push_back(CPEs_Info.at(i));
  }
//  Ptr<std::vector<double> > ssAntennaHeight= Create <std::vector<double> > ();
//  Ptr<std::vector<double> > ssTxGain = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > ssRxGain = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > ssTxPower = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > bsAntennaHeight = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > bsTxGain = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > bsRxGain = Create <std::vector<double> > ();
//  Ptr<std::vector<double> > bsTxPower = Create <std::vector<double> > ();
//  Ptr<std::vector<unsigned int> > ctrFreq = Create <std::vector<unsigned int> > ();

  NS_LOG_UNCOND("Assigning...");
  //std::string a="a";
//  ReadData(a, ssNum, bsNum, ssPositionAlloc, bsPositionAlloc, ssAntennaHeight, ssTxGain, ssRxGain, ssTxPower, bsAntennaHeight, bsTxGain, bsRxGain, bsTxPower, ctrFreq);

  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  unsigned int num = CPEs_Alcona[sectNum].size();
	  ssNodesAlcona[sectNum].Create (num);
	  vector <double> ssAntennaHeightVector;
	  vector <string> ssNodeNameVector;
	  vector <double> ssTxFreqVector;
	  Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject <ListPositionAllocator>();
	  for (unsigned int j=0;j< num; j++)
	  {
	  	  ssPositionAlloc ->Add(Vector(CPEs_Alcona[sectNum].at(j).latitude, CPEs_Alcona[sectNum].at(j).longtitude, (CPEs_Alcona[sectNum].at(j).elevation)/1000
));
	  	  ssAntennaHeightVector.push_back(CPEs_Alcona[sectNum].at(j).TxAntennaHeight);
	  	  ssNodeNameVector.push_back(CPEs_Alcona[sectNum].at(j).location);
	  	  ssTxFreqVector.push_back(CPEs_Alcona[sectNum].at(j).TxFreq);
	  }

	  MobilityHelper ssMobility;

	  ssMobility.SetPositionAllocator(ssPositionAlloc);
	  ssMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  ssMobility.m_antennaHeightVector = ssAntennaHeightVector;
	  ssMobility.m_nodeNameVector = ssNodeNameVector;
	  ssMobility.m_txFreqVector = ssTxFreqVector;
	  ssMobility.Install(ssNodesAlcona[sectNum]);

	  //print
		  for (unsigned int k=0;k<num;k++)
		  {
			  Ptr<MobilityModel> position = ssNodesAlcona[sectNum].Get(k)->GetObject<MobilityModel> ();
			  NS_ASSERT (position != 0);
			  Vector pos = position->GetPosition ();
			  std::cout << "CPE#" << k << " at Alcona Sector#" << sectNum << ":, x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << ", ant=" << position->m_antennaHeight << std::endl;
		  } //for k

	  //print
	  for (unsigned int k=0;k<num;k++)
	  {
		  Ptr<MobilityModel> position = ssNodesAlcona[sectNum].Get(k)->GetObject<MobilityModel> ();
		  NS_ASSERT (position != 0);
		  Vector pos = position->GetPosition ();
		  std::cout << "CPE#" << k << " at Alcona Sector#" << sectNum << ":, x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << ", ant=" << position->m_antennaHeight << std::endl;
	  } //for k

	  ssDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install(	ssNodesAlcona[sectNum],
			  	  	  	  	  	  	  	  	  	  	  	  	WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
			  	  	  	  	  	  	  	  	  	  	  	    WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
			  	  	  	  	  	  	  	  	  	  	  	    scheduler, &CPEs_Alcona[sectNum], true);
	  for (unsigned int j=0;j< num; j++)
	  {
		  ssDevsAlcona[sectNum].Get(j)->GetObject<SubscriberStationNetDevice> ()->SetModulationType (modulation);
	  }

	  stack.Install (ssNodesAlcona[sectNum]);


	 // SSinterfaces_Alcona[sectNum] = address.Assign (ssDevsAlcona[sectNum]);

  } // for sectNum

	std::vector<NodeContainer> AdjacencyList[3];
	for (int sectNum =0; sectNum < 3; sectNum++)
		AdjacencyList[sectNum] = std::vector<NodeContainer> (ssNodesAlcona[sectNum].GetN());

  NS_LOG_UNCOND("Done assigning SSNodesAlcona.");
  vector<Node_Data> BS_Alcona[3];
  Ipv4InterfaceContainer BSinterface_Alcona[3];
  for (unsigned int i=0; i<3; i++)
  {
	  NS_LOG_UNCOND("i="<<i);
	  BS_Alcona[i].push_back(BS_Info[i]);
  }
  NodeContainer bsNodesAlcona[3];
  NetDeviceContainer bsDevsAlcona[3];

  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  NS_LOG_UNCOND("sectNum="<<sectNum);
	  bsNodesAlcona[sectNum].Create (1);
	  vector <double> bsAntennaHeightVector;
	  vector <string> bsNodeNameVector;
	  vector <double> bsTxFreqVector;
	  Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject <ListPositionAllocator>();
	  bsPositionAlloc ->Add(Vector(BS_Info.at(sectNum).latitude, BS_Info.at(sectNum).longtitude, (BS_Info.at(sectNum).elevation)/1000));
	  NS_LOG_UNCOND("here 1");
	  bsAntennaHeightVector.push_back(BS_Info.at(sectNum).TxAntennaHeight);
	  bsNodeNameVector.push_back(BS_Info.at(sectNum).location);
	  bsTxFreqVector.push_back(BS_Info.at(sectNum).TxFreq);
	  MobilityHelper bsMobility;
	  bsMobility.SetPositionAllocator(bsPositionAlloc);
	  bsMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  bsMobility.m_antennaHeightVector = bsAntennaHeightVector;
	  bsMobility.m_nodeNameVector = bsNodeNameVector;
	  bsMobility.m_txFreqVector = bsTxFreqVector;
	  bsMobility.Install(bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("here 2");
	  bsDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install (bsNodesAlcona[sectNum].Get(0), WimaxHelper::DEVICE_TYPE_BASE_STATION,
	 		  	  	  	  	  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
	 		  	  	  	  	  scheduler, &BS_Alcona[sectNum], true);
	  NS_LOG_UNCOND("here 3");
	  bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->
			  SetDuplex(BS_Alcona[sectNum].at(0).TxFreq,BS_Alcona[sectNum].at(0).RxFreq);

	  bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->SetChannelBandwidth(5e6);
	  NS_LOG_UNCOND("here 4");
	  stack.Install (bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("here 5");
	  BSinterface_Alcona[sectNum] = address.Assign (bsDevsAlcona[sectNum]);
  } // for base station Alcona sectNum
  NS_LOG_UNCOND("Done assigning BSNodesAlcona.");

  //Making Adjacency Lists

	for (int sectNum =0; sectNum < 3; sectNum++)
	{
		for(uint32_t i=0; i<AdjacencyList[sectNum].size (); ++i)
			{
				AdjacencyList[sectNum][i] = NodeContainer (bsNodesAlcona[sectNum], ssNodesAlcona[sectNum].Get (i));
			}
	}
	PointToPointHelper p2p;
	std::vector<NetDeviceContainer> deviceAdjacencyList[3];
	for (int sectNum=0; sectNum<3; sectNum++)
		deviceAdjacencyList[sectNum] = std::vector<NetDeviceContainer> (ssNodesAlcona[sectNum].GetN());
	for (int sectNum=0; sectNum<3; sectNum++)
	{
		for(uint32_t i=0; i<AdjacencyList[sectNum].size (); ++i)
		{
			deviceAdjacencyList[sectNum][i] = p2p.Install (AdjacencyList[sectNum][i]);
		}
	}
	int created_nodes = 1;
	// Later, we add IP addresses.
	//NS_LOG_UNCOND ("Assigning a subnet IP Address for TCP nodes " << created_nodes << " to " << created_nodes+numUsers-1 << "...");
	std::vector<Ipv4InterfaceContainer>  interfaceAdjacencyList [3];
	for (int sectNum=0; sectNum<3; sectNum++)
		interfaceAdjacencyList[sectNum] = std::vector<Ipv4InterfaceContainer> (ssNodesAlcona[sectNum].GetN());

	Ipv4AddressHelper ipv4;

	for (int sectNum=0; sectNum<3; sectNum++)
	{
		for( uint32_t i=0; i<interfaceAdjacencyList[sectNum].size (); ++i)
		{
			NS_LOG_UNCOND("C#"<<i);
			std::ostringstream subnet;
			int cc,dd;
			dd = ((i+1+created_nodes)%62) +1; //first 6 digit of dd
			cc = (i+1+created_nodes-dd+1)/62 + 1; // created_nodes
			dd *= 4;
			subnet << "10.1."<< cc << "." << dd ;
			NS_LOG_UNCOND("cn: " << created_nodes << ", i: " << i << ", cc: " << cc << ", dd:" << dd);
			ipv4.SetBase (subnet.str ().c_str (), "255.255.255.252");
			interfaceAdjacencyList[sectNum][i] = ipv4.Assign (deviceAdjacencyList[sectNum][i]);
		}
		created_nodes  = created_nodes  +   interfaceAdjacencyList[sectNum].size ();
	}


  //### Traffic Generation

  //### Uplink Traffic-TCP
  //###
  duration = 10;
  int sectNum = 0;
  std::string on_time = "ns3::ConstantRandomVariable[Constant=1]";
  std::string off_time = "ns3::ConstantRandomVariable[Constant=5]";
  int fid = 0;
  ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_RTPS;
  std::string data_rate = "3kb/s";
  uint32_t pktSize = 128;
  port = Uplink_TCP_Traffic(wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
		  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time, fid,
   schedulingType,data_rate,  pktSize);  //SSinterfaces_Alcona has been used instead of Adjacency_list, fid,


  //TODO: Make different helpers for each of the BS and its corresponding SSs.

  NS_LOG_UNCOND ("here 1");

  NS_LOG_UNCOND ("Starting simulation.....");
  Simulator::Run ();

//  for (unsigned int i=0; i < CPEs_Info.size(); i++)
//  {
//	  ss[i] = 0;
//  }

  //bs = 0;

  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");

  return 0;
}
