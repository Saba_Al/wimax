#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/config-store-module.h"
#include "ns3/flow-monitor.h"
#include "ns3/flow-monitor-helper.h"
#include "ns3/ipv4-global-routing-helper.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "assert.h"
#include "ns3/service-flow.h"
#include <iostream>
#include <sstream>
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/cost-Erceg.h"
#include "../model/bandwidth-manager.h"
#include "ns3/ipv4-flow-probe.h"
#include "ns3/ipv4-flow-classifier.h"
#include "ns3/flow-id-tag.h"

#define MAX_USERS 3005
NS_LOG_COMPONENT_DEFINE("WimaxSimpleExample");

using namespace ns3;

uint32_t MacTxDropCountUL=0, MacRxDropCountUL=0, PhyTxDropCountUL=0, PhyRxDropCountUL=0;
uint32_t MacTxDropCountDL=0, MacRxDropCountDL=0, PhyTxDropCountDL=0, PhyRxDropCountDL=0;


/*****************************************************************************/
//uplink udp:
//UdpServerHelper udpServerUL[MAX_USERS];
ApplicationContainer serverAppsUL[MAX_USERS];
//UdpClientHelper udpClientUL[MAX_USERS];
//ApplicationContainer clientAppsUL[MAX_USERS];
IpcsClassifierRecord UlClassifier[MAX_USERS];
ServiceFlow UlServiceFlow[MAX_USERS];

//downlink udp:
//UdpServerHelper udpServerDL[MAX_USERS];
ApplicationContainer serverAppsDL[MAX_USERS];
UdpClientHelper udpClientDL[MAX_USERS];
ApplicationContainer clientAppsDL[MAX_USERS];
IpcsClassifierRecord DlClassifier[MAX_USERS];
ServiceFlow DlServiceFlow[MAX_USERS];

//keep track of appnum per type
int appnumAfterWASADL = -1;
int appnumAfterWASAUL = -1;
int appnumAfterMonDL = -1;
int appnumAfterMonUL = -1;
int appnumAfterCtrlDL = -1;
int appnumAfterCtrlUL = -1;
int appnumAfterPtctDL = -1;
int appnumAfterPtctUL = -1;
int appnumAfterSMDL = -1;
int appnumAfterSMUL = -1;
int appnumBeforeWASADL = -1;
int appnumBeforeWASAUL = -1;
int appnumBeforeMonDL = -1;
int appnumBeforeMonUL = -1;
int appnumBeforeCtrlDL = -1;
int appnumBeforeCtrlUL = -1;
int appnumBeforePtctDL = -1;
int appnumBeforePtctUL = -1;
int appnumBeforeSMDL = -1;
int appnumBeforeSMUL = -1;

/********************************************************/

void TraceFuncPhyRxDrop(Ptr<PacketBurst> pb) {
	NS_LOG_FUNCTION("****TraceFuncPhyRxDrop****" << pb);
}

void TraceFuncPhyRxEnd(Ptr<NetDevice> dev, double SNR) {
	NS_LOG_FUNCTION("Device" << dev << " SNR=" << SNR);
	Ptr <SubscriberStationNetDevice> ss = DynamicCast<SubscriberStationNetDevice,NetDevice>(dev);

	//std::cout << "***SNR = " << SNR << std::endl;
	/*NS_LOG_FUNCTION("number of packets in burst:"<< b->GetNPackets()<<b->GetSize());

	  int j=0;
	  for (std::list<Ptr<Packet> >::iterator it=b->GetPackets().begin(); it != b->GetPackets().end(); ++it)
	  {
	  	j++;
	  	// To get a header from Ptr<Packet> p
	  	// first, copy the packet
	  	NS_LOG_FUNCTION("before copy");
	  	Ptr<Packet> q = it->operator->()->Copy();
	  	//q->Print(std::cout);
	  	NS_LOG_FUNCTION("packet" << j << q << "size" << q->GetSize());
	  	if (q==NULL)
	  		break;
	  	PacketMetadata::ItemIterator metadataIterator = q->BeginItem();
	  	PacketMetadata::Item item;
	  	NS_LOG_FUNCTION("before while...");
	  	int jj=0;
	  	while (metadataIterator.HasNext())
	  	{
	  			jj++;
	  			NS_LOG_FUNCTION("packet"<<j<<"Item"<<jj);
	  	    item = metadataIterator.Next();
	  	    NS_LOG_FUNCTION("item name: " << item.tid.GetName());

	  	    // for example, if we want to have an ip header
	  	    if(item.tid.GetName() == "ns3::Ipv4Header")
	  	    {
	  	    	 NS_LOG_FUNCTION("Got Ipv4Header!");
	  	        Callback<ObjectBase *> constructor = item.tid.GetConstructor();
	  	        //NS_ASSERT(! constructor. );

	  	        // Ptr<> and DynamicCast<> won't work here as all headers are from ObjectBase, not Object
	  	        ObjectBase *instance = constructor();
	  	        NS_ASSERT(instance != 0);

	  	        Ipv4Header *ipv4Header = dynamic_cast<Ipv4Header*> (instance);
	  	        NS_ASSERT(ipv4Header != 0);

	  	        Ipv4Address ipv4 = ipv4Header->GetSource();

	  	        NS_LOG_UNCOND("IPV4 Source is " << ipv4);
	  	       // ipv4Header->Deserialize(item.current);
	  	        // you can use the ip header then ...

	  	        // finished, clear the ip header
	  	        delete ipv4Header;
	  	    }
	  	}
	  }
*/
}

//http://personal.ee.surrey.ac.uk/Personal/K.Katsaros/media/ns3lab-sol/lab-5-solved.cc

void PrintTotalDropCounts()
{
  NS_LOG_FUNCTION(Simulator::Now().GetSeconds());
  NS_LOG_UNCOND("DIR\tMac-Tx\t\tMac-Rx\t\tPhy-Tx\t\tPhy-Rx");
  NS_LOG_UNCOND("UL\t" << MacTxDropCountUL << "\t\t"<< MacRxDropCountUL
  		<< "\t\t" <<PhyTxDropCountUL << "\t\t" << PhyRxDropCountUL);
  NS_LOG_UNCOND("DL\t" << MacTxDropCountDL << "\t\t"<< MacRxDropCountDL
  		<< "\t\t"<<PhyTxDropCountDL << "\t\t" << PhyRxDropCountDL << endl);
  Simulator::Schedule(Seconds(5.0), &PrintTotalDropCounts); //call every 5 seconds:
}


void MacTxDropUL(Ptr<const Packet> p)
{
  NS_LOG_FUNCTION("");
  MacTxDropCountUL++;
}

void MacRxDropUL(Ptr<const Packet> p)
{
  NS_LOG_FUNCTION("");
  MacRxDropCountUL++;
}

void
PhyTxDropUL(Ptr<PacketBurst> pb)
{
  NS_LOG_FUNCTION("");
  PhyTxDropCountUL++;
//  PhyTxDropCountUL+= pb->GetNPackets();
}

void
PhyRxDropUL(Ptr<PacketBurst> pb)
{
  NS_LOG_FUNCTION("");
  PhyRxDropCountUL++;
  //PhyRxDropCountUL+=pb->GetNPackets();
}

void MacTxDropDL(Ptr<const Packet> p)
{
  NS_LOG_FUNCTION("");
  MacTxDropCountDL++;
}

void MacRxDropDL(Ptr<const Packet> p)
{
  NS_LOG_FUNCTION("");
  MacRxDropCountDL++;
}

void
PhyTxDropDL(Ptr<PacketBurst> pb)
{
  NS_LOG_FUNCTION("");
  PhyTxDropCountDL++;
  //PhyTxDropCountDL+=pb->GetNPackets();
}

void
PhyRxDropDL(Ptr<PacketBurst> pb)
{
  NS_LOG_FUNCTION("");
  PhyRxDropCountDL++;
  //PhyRxDropCountDL+=pb->GetNPackets();
}

/*******************************************************************/
int setup_udp_onoff(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice> ss[],
		NodeContainer bsNode, Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size,
		std::string data_rate, int startPort, WimaxHelper wimaxAlcona,
		WimaxPhy::ModulationType modulation, ServiceFlow::SchedulingType sfType,
		int req_latency = 0) {
	//,PacketSinkHelper* udpServer

	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	uint32_t startTime = 2000;
	 //double stopTime = 7;*/
	for (int i = 0; i < nbSS; i++) {
		cout << "nbSS: " << nbSS << endl;
		cout << "setup_udp_onoff, i: " << i << endl;
		port = startPort + ((i + 1) * 10);
		// set server port to 100+(i*10)
		Address sinkLocalAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory",
				sinkLocalAddress);
		serverApps[i] = udpServer[i]->Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(duration)); //duration

		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory", Address());
		AddressValue remoteAddress(
				InetSocketAddress(BSinterface.GetAddress(0), port));
		udpClient[i]->SetAttribute("Remote", remoteAddress);
		//udpClient[i]->SetAttribute("MaxPackets", UintegerValue(1200));
		//udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		cout << "bb" << endl;
		udpClient[i]->SetConstantRate(DataRate(data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId", UintegerValue(fid));
		udpClient[i]->SetAttribute("OnTime", StringValue(on_time));
		udpClient[i]->SetAttribute("OffTime", StringValue(off_time));

		cout << "cc" << endl;
		cout << "size: " << ssNodes.GetN() << endl;
		clientApps[i] = udpClient[i]->Install(ssNodes.Get(i));

		clientApps[i].Start(MilliSeconds(startTime));
		clientApps[i].Stop(Seconds(duration));
		startTime = startTime + 100;
		//stopTime = startTime + 5;
	}
	int priority = 1;
	if (fid == 1)
		priority = 2;
	else if (fid == 3)
		priority = 1;
	startTime = 2000;
	for (int i = 0; i < nbSS; i++) {
		cout << "setup_udp_onoff fid: " << fid << "service flow# " << i << endl;
		IpcsClassifierRecord ulClassifierBe(SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + ((i + 1) * 10),
				startPort + ((i + 1) * 10), 17, priority);
		cout << "dd" << endl;
		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, sfType, ulClassifierBe);
		cout << "ee" << endl;
		//ulServiceFlowBe.SetModulation(modulation);
		//
		if (sfType == ServiceFlow::SF_TYPE_UGS ) {//&& fid != 1
			ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
			ulServiceFlowBe.SetMaximumLatency(req_latency);
			ulServiceFlowBe.SetSduSize((uint8_t)pkt_size);
			ulServiceFlowBe.SetStartTime(startTime);
			//	ulServiceFlowBe.SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
			std::cout<< "data_rate" << std::atoi(data_rate.c_str()) << ", interval: " << (pkt_size*8000)/(1024*std::atoi(data_rate.c_str())) << std::endl;
			//NS_ASSERT(false);
			ulServiceFlowBe.SetUnsolicitedGrantInterval((pkt_size*8000)/(1024*std::atoi(data_rate.c_str())));
		//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
			ulServiceFlowBe.SetToleratedJitter(10);
			ulServiceFlowBe.SetTrafficPriority(1);

		} else if (sfType == ServiceFlow::SF_TYPE_RTPS) {
		//	ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
			ulServiceFlowBe.SetMaximumLatency(req_latency);
			//ulServiceFlowBe.SetSduSize(49);
		//	ulServiceFlowBe.SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
			ulServiceFlowBe.SetUnsolicitedPollingInterval(10);
		//	ulServiceFlowBe.SetRequestTransmissionPolicy(0x00110111);
			ulServiceFlowBe.SetToleratedJitter(10);
			ulServiceFlowBe.SetTrafficPriority(1);
			}
		ss[i]->AddServiceFlow(ulServiceFlowBe);
		startTime = startTime + 100;
	//	cout << "sfid: " << ulServiceFlowBe.GetSfid() << ", fid: " << fid << endl;
		if (sfType == ServiceFlow::SF_TYPE_UGS && fid != 1)
		{
			ServiceFlow *sss = *(ss[i]->GetServiceFlowManager()->GetServiceFlows(ServiceFlow::SF_TYPE_UGS).begin());
			cout<<"mmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmmm, sfid:     " << sss->GetSfid() << endl;
		}
	}
	return port;
}

void NodeConfigurationCommonSetupUDP(NodeContainer &bsNodes, string usecase, WimaxHelper &wimax,
		int numUsers, int &created_nodes, double areaRadius,
		InternetStackHelper &stack, Ipv4AddressHelper &address,
		WimaxHelper::SchedulerType scheduler, WimaxPhy::ModulationType modulation,/*inputs*/
		NodeContainer &ssNodes, Ptr<SubscriberStationNetDevice> *ss,
		Ipv4InterfaceContainer &SSinterfaces /*outputs*/) {
	/*------------------------------*/
	//For each application, we need to create common variables here (common part between UL and DL of each application):
	//ssNodes = new NodeContainer;
	NS_LOG_UNCOND ("Assigning IP Adresss for UDP nodes " << created_nodes << " to " << created_nodes+numUsers-1 << "...");

	ssNodes.Create(numUsers);
	NetDeviceContainer ssDevs;
	ssDevs = wimax.Install(ssNodes, WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
			WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler);
//	for (int i=0;i<ssDevs.GetN();i++)
//		ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>()->GetBa
	stack.Install(ssNodes);
	SSinterfaces = address.Assign(ssDevs);

	//node positions
	MobilityHelper mobility;
	std::ostringstream convert;
	convert << areaRadius;

	mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator", "X",
			StringValue("0.0"), "Y", StringValue("0.0"), "rho",
			StringValue(convert.str())); //rho = 5000, My_Ro   //DoubleValue(areaRadius)); //
	vector<double> ssAntennaHeightVector;
	vector<double> ssTxFreqVector;
	vector<string> ssNodeNameVector;
	for (int j = 0; j < numUsers; j++) {
		ssAntennaHeightVector.push_back(12);
		ssTxFreqVector.push_back(1827);
		ostringstream name;
		name << usecase << "_Node_" << j;
		ssNodeNameVector.push_back(name.str());
	}
	mobility.m_antennaHeightVector = ssAntennaHeightVector;
	mobility.m_nodeNameVector = ssNodeNameVector;
	mobility.m_txFreqVector = ssTxFreqVector;
	mobility.Install(ssNodes);

	//ssNodes.Get(0)->GetNDevices()->
	double txPowerDbm = 30;
	Cost231PropagationLossModel propModel;

	for (int i = 0; i < numUsers; i++) {
		ss[i] = ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>();
		Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(ss[i]->GetPhy());
	  myPhy->SetTxPower(txPowerDbm);
	  myPhy->SetTxGain(12);
	  myPhy->SetRxGain(12);


		Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(
				ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>()->GetPhy());

		DoubleValue noiseFigure;
		ssPhy->GetAttribute("NoiseFigure",noiseFigure);
		double rxPower = txPowerDbm + propModel.GetLoss (ssNodes.Get(i)->GetObject<MobilityModel>(), bsNodes.Get(0)->GetObject<MobilityModel>());
		double Nwb = -174 + 10*std::log(ssPhy->GetBandwidth())/2.303 + noiseFigure.Get();
		double SNR = rxPower - Nwb;

		NS_LOG_DEBUG("SNR Value for " << ssNodes.Get(i)->GetObject<MobilityModel>()->m_nodeName << " is " << SNR);

	  //FARIBA: Find the SNR Thresholds from MATLAB and assign the corresponding SNR:
		if (SNR < 6.4345 ) //2.3045 < SNR &&
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_BPSK_12);
		}
		else if ( 6.4345 <= SNR && SNR < 9.4939 )
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_12);
		}
		else if ( 9.4939 <= SNR && SNR < 12.2789 )
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_34);
		}
		else if ( 12.2789 <= SNR && SNR < 15.500)
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_12);
		}
		else if ( 15.500 <= SNR && SNR < 20.300 )
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_34);
		}
		else/* if ( 20.300 <= SNR && SNR < 21.500)*/
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_23);
		}
		/*else if ( 21.500 <= SNR )
		{
			ss[i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_34);
		}*/
		NS_LOG_DEBUG("Modulation" << ss[i]->GetModulationType() );
		ssPhy->ActivateLoss(true);
		ssPhy->SetFrameDuration(Seconds(0.005));
	}
	created_nodes = created_nodes + numUsers;
	//NS_LOG_UNCOND("SSinterfaces 0:" << SSinterfaces.GetAddress(0));
}

int Uplink_UDP_Traffic(WimaxHelper &wimax, int port, int numUsers, int duration,
		Ipv4InterfaceContainer BSinterface, Ipv4InterfaceContainer SSinterfaces,
		NodeContainer bsNodes, NodeContainer ssNodes,
		Ptr<SubscriberStationNetDevice> ss[], double interval, int fid,
		uint32_t pktSize, UdpClientHelper* udpClient, UdpServerHelper* udpServer,
		ApplicationContainer* clientApps, ServiceFlow::SchedulingType sfType) {
	int port1 = port;
	for (int i = 0; i < numUsers; i++) {
		port = port + 10;
		udpServer[i] = UdpServerHelper(port); //udpServerUL
		serverAppsUL[i] = udpServer[i].Install(bsNodes.Get(0));
		serverAppsUL[i].Start(Seconds(0));
		serverAppsUL[i].Stop(Seconds(duration));
		udpClient[i] = UdpClientHelper(BSinterface.GetAddress(0), port);
		udpClient[i].SetAttribute("MaxPackets", UintegerValue(1200));
		udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //0.01
		udpClient[i].SetAttribute("FlowId", UintegerValue(fid));
		udpClient[i].SetAttribute("PacketSize", UintegerValue(pktSize));
		clientApps[i] = udpClient[i].Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(1.5));
		clientApps[i].Stop(Seconds(duration));
	}

	//classifier
	for (int i = 0; i < numUsers; i++) {
		cout << "setup_udp_onoff fid: " << fid << "service flow#= " << i << endl;
		UlClassifier[i] = IpcsClassifierRecord(SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, port1 + 10 * (i + 1),
				port1 + 10 * (i + 1), 17, 1);
		UlServiceFlow[i] = wimax.CreateServiceFlow(ServiceFlow::SF_DIRECTION_UP,
				sfType/*ServiceFlow::SF_TYPE_RTPS*/, UlClassifier[i]);
		//	uint32_t data_rate = pktSize / interval;
		//UlServiceFlow[i].SetMinReservedTrafficRate(data_rate);
		ss[i]->AddServiceFlow(UlServiceFlow[i]);
	}

	return port;
}

int Downlink_UDP_Traffic(WimaxHelper &wimax, int port, int numUsers,
		int duration, Ipv4InterfaceContainer SSinterfaces, NodeContainer bsNodes,
		NodeContainer ssNodes, Ptr<SubscriberStationNetDevice> ss[], int fid,
		uint32_t pktSize, double interval, UdpServerHelper *udpServerDL,
		ServiceFlow::SchedulingType sfType, int req_latency = 0) {
	int port1 = port;
	for (int i = 0; i < numUsers; i++) {
		port = port + 10;
		//create applications whose servers are on ssNodes
		udpServerDL[i] = UdpServerHelper(port);
		serverAppsDL[i] = udpServerDL[i].Install(ssNodes.Get(i));
		serverAppsDL[i].Start(Seconds(0.0));
		serverAppsDL[i].Stop(Seconds(duration));
		//create clients on BS for each application
		udpClientDL[i] = UdpClientHelper(SSinterfaces.GetAddress(i), port);
		udpClientDL[i].SetAttribute("MaxPackets", UintegerValue(120000));
		udpClientDL[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //0.01
		udpClientDL[i].SetAttribute("FlowId", UintegerValue(fid));
		udpClientDL[i].SetAttribute("PacketSize", UintegerValue(pktSize));
		clientAppsDL[i] = udpClientDL[i].Install(bsNodes.Get(0));
		Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable>();
		//NS_LOG_UNCOND("rrrrrrrrrrrrrrrrrr:" << uv->GetValue());
		clientAppsDL[i].Start(Seconds(1.5)); //+3*uv->GetValue()
		clientAppsDL[i].Stop(Seconds(duration));
	}

	//classifier
	for (int i = 0; i < numUsers; i++) {
		DlClassifier[i] = IpcsClassifierRecord(Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), 0, 65000, port1 + 10 * (i + 1),
				port1 + 10 * (i + 1), 17, 1);
		DlServiceFlow[i] = wimax.CreateServiceFlow(ServiceFlow::SF_DIRECTION_DOWN,
				sfType/*ServiceFlow::SF_TYPE_UGS*/, DlClassifier[i]);
	//	DlServiceFlow[i].SetModulation(WimaxPhy::MODULATION_TYPE_QAM64_23);
		/*uint32_t data_rate = pktSize/interval;
		 if (sfType == ServiceFlow::SF_TYPE_UGS)
		 {
		 DlServiceFlow[i].SetMinReservedTrafficRate(data_rate);
		 DlServiceFlow[i].SetMaximumLatency(req_latency);
		 }*/
		ss[i]->AddServiceFlow(DlServiceFlow[i]);
	}
	return port;
}

int Downlink_onoff_Traffic(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice> ss[],
		NodeContainer bsNode, Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size,
		std::string data_rate, int startPort, WimaxHelper wimax,
		WimaxPhy::ModulationType modulation, ServiceFlow::SchedulingType sfType,
		int req_latency = 0) {
	//int port1 = port;
	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	double startTime = 4.8;
	//double stopTime = 7;
	for (int i = 0; i < nbSS; i++) {
		cout << "nbSS" << nbSS << endl;
		cout << "setup_udp_onoff,i=" << i << endl;
		port = startPort + ((i + 1) * 10);
		// set server port to 100+(i*10)
		Address sinkLocalAddress(InetSocketAddress(Ipv4Address::GetAny(), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory",
				sinkLocalAddress);
		;
		serverApps[i] = udpServer[i]->Install(ssNodes.Get(i));
		serverApps[i].Start(Seconds(startTime-1));
		serverApps[i].Stop(Seconds(duration)); //duration

		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory", Address());
		AddressValue remoteAddress(
				InetSocketAddress(SSinterfaces.GetAddress(i), port));
		udpClient[i]->SetAttribute("Remote", remoteAddress);
		//udpClient[i]->SetAttribute("MaxPackets", UintegerValue(1200));
		//udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		cout << "bb" << endl;
		udpClient[i]->SetConstantRate(DataRate(data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId", UintegerValue(fid));
		udpClient[i]->SetAttribute("OnTime", StringValue(on_time));
		udpClient[i]->SetAttribute("OffTime", StringValue(off_time));

		cout << "cc" << endl;
		cout << "size" << ssNodes.GetN() << endl;
		clientApps[i] = udpClient[i]->Install(bsNode.Get(0));

		clientApps[i].Start(Seconds(startTime));
		clientApps[i].Stop(Seconds(duration));
		startTime = startTime + 0.1;
	}
	startTime = 2000;
	//classifier
	for (int i = 0; i < nbSS; i++) {
		DlClassifier[i] = IpcsClassifierRecord(Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), 0, 65000, startPort + 10 * (i + 1),
				startPort + 10 * (i + 1), 17, 1);
		DlServiceFlow[i] = wimax.CreateServiceFlow(ServiceFlow::SF_DIRECTION_DOWN,
				sfType/*ServiceFlow::SF_TYPE_RTPS*/, DlClassifier[i]);
		//DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str())); //IF UNCOMMENTED, we get buffer assertion for traffic fid=8
		if (sfType == ServiceFlow::SF_TYPE_UGS) {
			DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str())*1024);
			DlServiceFlow[i].SetMaximumLatency(req_latency);
		//	DlServiceFlow[i].SetSduSize(49);
		//	DlServiceFlow[i].SetStartTime(startTime);
			DlServiceFlow[i].SetUnsolicitedGrantInterval(1);//(pkt_size*8000)/(1024*std::atoi(data_rate.c_str()))
		//	DlServiceFlow[i].SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
			DlServiceFlow[i].SetRequestTransmissionPolicy(0x00110000);
			DlServiceFlow[i].SetToleratedJitter(10);
			DlServiceFlow[i].SetTrafficPriority(1);
		} else if (sfType == ServiceFlow::SF_TYPE_RTPS) {
			DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str()));
			DlServiceFlow[i].SetMaximumLatency(req_latency);
			//DlServiceFlow[i].SetSduSize(49);
			DlServiceFlow[i].SetUnsolicitedPollingInterval(10);
			DlServiceFlow[i].SetMaxSustainedTrafficRate(std::atoi(data_rate.c_str()));
			DlServiceFlow[i].SetRequestTransmissionPolicy(0x00110100);
			DlServiceFlow[i].SetToleratedJitter(10);
			DlServiceFlow[i].SetTrafficPriority(2);
			//DlServiceFlow[i].SetModulation(WimaxPhy::MODULATION_TYPE_QAM64_23);
		}
		//m_minReservedTrafficRate = (data_rate);
		startTime = startTime + 100;
		ss[i]->AddServiceFlow(DlServiceFlow[i]);
	}
	return port;
} //Downlink_onoff_Traffic

void printLocations(string name, Ptr<SubscriberStationNetDevice> *ss, int num) {
	for (int i = 0; i < num; i++) { //print locations
		Ptr<ConstantPositionMobilityModel> mm = ss[i]->GetNode()->GetObject<
				ConstantPositionMobilityModel>();
		//	Ptr<ConstantPositionMobilityModel> bp =
		//				bsNodes.Get(0)->GetObject<ConstantPositionMobilityModel>();
		cout << "ss" << name << "[" << i << "] Location is: ";
		if (mm)
			cout << mm->GetPosition() << endl;
		else
			cout << "???" << endl;
	}
}

int main(int argc, char *argv[]) {
	bool verbose = false, enableUplink = false, enableDownlink = false,
			enableRural = false, enableSuburban = false, enableUrban = false;
	int duration = 200, schedType = 0, port = 100, numAggregators = 0;
	int numWASAUsers = 0, numPtctUsers = 0, numMonUsers = 0, numCtrlUsers = 0,
			numSMUsers = 0;
	WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM64_23; //16_34; //64_23; //16_34; //  ; ;
	double compressionRatio = 1.0;
	double areaRadius = 2.0; //0.56434; //0.56434; //kilometer
	int fidWASAUL = 0, fidMonUL = 1, fidCtrlUL = 2, fidPtctUL = 3, fidSMUL = 4,
			fidAgg = 4;
	int START_DL_FID = 5;
	int fidWASADL = 5, fidMonDL = 6, fidCtrlDL = 7, fidPtctDL = 8;
	int END_DL_FID = 9;
	double bsTxPower = 36; //50;
	string flowNames[FLOW_NUM] = {"WASA","Mont","Ctrl","Ptct","SMRT","WASA","Mont","Ctrl","Ptct",""};
//int monDLInterval = 0.2; //with packet size 256//0.1 with packetsize 128,

//	RngSeedManager::SetSeed(12345);

	WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;

	CommandLine cmd;
	cmd.AddValue("scheduler", "type of scheduler to use with the network devices",schedType);
	cmd.AddValue("duration", "duration of the simulation in seconds", duration);
	cmd.AddValue("verbose", "turn on all WimaxNetDevice log components", verbose);
	cmd.AddValue("NW", "Number of nodes which have WASA data.", numWASAUsers);
	cmd.AddValue("NP", "Number of nodes which have PTCT data.", numPtctUsers);
	cmd.AddValue("NM", "Number of nodes which have MONT data.", numMonUsers);
	cmd.AddValue("NC", "Number of nodes which have CTRL data.", numCtrlUsers);
	cmd.AddValue("NS", "Number of nodes which have SMRT data.", numSMUsers);
	cmd.AddValue("DL", "Enable DL transmission?", enableDownlink);
	cmd.AddValue("UL", "Enable UL transmission?", enableUplink);
	cmd.AddValue("NAG", "Number of Aggregators.", numAggregators);
	cmd.AddValue("comp", "Aggregation Compression Ratio.", compressionRatio);
	cmd.AddValue("rad", "Cell Radius (km).", areaRadius);
	cmd.AddValue("RL", "Enable Rural area transmission?", enableRural);
	cmd.AddValue("SU", "Enable Suburban area transmission?", enableSuburban);
	cmd.AddValue("U", "Enable Urban area transmission?", enableUrban);
	cmd.Parse(argc, argv);

	//LogComponentEnable("Cost231PropagationLossModel",LOG_LEVEL_DEBUG);
//	LogComponentEnable("ErcegPropagationLossModel",LOG_LEVEL_ALL);
	//LogComponentEnable ("UdpClient", LOG_LEVEL_ALL);
//	LogComponentEnable("UdpServer", LOG_LEVEL_ALL);
	//LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
	//LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink
	//  LogComponentEnable ("Packet", LOG_LEVEL_ALL);
	// LogComponentEnable ("WimaxHelper",LOG_LEVEL_ALL);

    //LogComponentEnable("WimaxNetDevice",LOG_LEVEL_ALL);
   //LogComponentEnable("BaseStationNetDevice",LOG_LEVEL_ALL);  //for buffer assertion and frame information
//    LogComponentEnable("SubscriberStationNetDevice",LOG_LEVEL_INFO);
//	LogComponentEnable("Socket",LOG_LEVEL_ALL);
	//LogComponentEnable ("MobilityHelper",LOG_LEVEL_ALL); //Mobility Model
//	LogComponentEnable ("WimaxMacQueue", LOG_LEVEL_INFO);
//		LogComponentEnable("SimpleOfdmWimaxPhy", LOG_LEVEL_FUNCTION);///DEBUG for SNRs
//	  LogComponentEnable("WimaxMacQueue",LOG_LEVEL_ALL);
  //  LogComponentEnable("PacketMetadata",LOG_LEVEL_ALL);
//	LogComponentEnable("UplinkSchedulerSimple", LOG_LEVEL_DEBUG);
//	LogComponentEnable("UplinkSchedulerMBQoS", LOG_LEVEL_FUNCTION);
//	LogComponentEnable("WimaxSimpleExample",LOG_LEVEL_DEBUG);
//	LogComponentEnable("BSSchedulerSimple",LOG_LEVEL_DEBUG);
	//	LogComponentEnable("WimaxMacQueue",LOG_LEVEL_INFO);

//	WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
	switch (schedType) {
	case 0:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
		break;
	case 1:
		scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
		break;
	case 2:
		scheduler = WimaxHelper::SCHED_TYPE_RTPS;
		break;
	default:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
		break;
	}

	//environment and number of nodes
	int density;
	double onTimeSMUL = 0.1;	//0.1
	double offTimeSMUL = 4;
	double dataRateKBSMUL = 1.0;  //UL data rate for each smart meter (kbps)
	if (enableRural == true) {
		assert(enableUrban == false && enableSuburban == false);
		numMonUsers = 5;  //5
		numWASAUsers = 3;  //3
		numCtrlUsers = 3;  //3
		numPtctUsers = 1;  //1
		density = 10;
		numSMUsers = int(3.14 / 3.0 * areaRadius * areaRadius * density); //20
		assert(MAX_USERS>=numSMUsers);
	}
	if (enableSuburban == true) {
		assert(enableUrban == false && enableRural == false);
		numMonUsers = 11;
		numCtrlUsers = 8;
		numWASAUsers = 8;
		numPtctUsers = 4;
		density = 800;
		numSMUsers = 10; //int(3.14/3.0 * areaRadius * areaRadius * density);
		assert(MAX_USERS>=numSMUsers);
	}

	if (enableUrban == true) {
		assert(enableRural == false && enableSuburban == false);
		numMonUsers = 27;
		numCtrlUsers = 11;
		numWASAUsers = 11;
		numPtctUsers = 8;
		density = 9000; //1200; //2000;
		numSMUsers = int(3.14 / 3.0 * areaRadius * areaRadius * density);
		assert(MAX_USERS>=numSMUsers);
	}
	if (numAggregators > 0) {
		onTimeSMUL *= int(numSMUsers / numAggregators);
		assert(compressionRatio >= 0.0 && compressionRatio <= 1.0);
		dataRateKBSMUL *= int(compressionRatio * numSMUsers / numAggregators);
		numSMUsers = numAggregators;
	}
	offTimeSMUL = onTimeSMUL >= 4.1 ? 0.0 : 4.1 - onTimeSMUL;

	WimaxHelper wimax;

	if (verbose) {
		wimax.EnableLogComponents();  // Turn on all wimax logging
	}

	/*This is the field for defining common variables (the same for all applications), e.g. BS*/
	NodeContainer bsNodes;
	bsNodes.Create(1);
	int created_nodes = 1;




	NetDeviceContainer bsDevs;
	bsDevs = wimax.Install(bsNodes, WimaxHelper::DEVICE_TYPE_BASE_STATION,
			WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler);
//	wimax.SetPropagationLossModel(SimpleOfdmWimaxChannel::ERCEG_PROPAGATION);
	Ptr<BaseStationNetDevice> bs;
	bs = bsDevs.Get(0)->GetObject<BaseStationNetDevice>();

	cout << "The_BS Address= " << bs->GetAddress() << endl;
	//return 0;
  //bs->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_23);
	//Added by Fariba on 15 Oct. 2014, 8:16pm
	bs->GetBandwidthManager()->SetSubframeRatio();
	//BS Position
	Ptr<ConstantPositionMobilityModel> BSPosition;
	BSPosition = CreateObject<ConstantPositionMobilityModel>();

	BSPosition->SetPosition(Vector(0, 0, 0));
	BSPosition->m_txFreq = 1827;
	BSPosition->m_antennaHeight = 30;
	BSPosition->m_nodeName = "The_BS";
	Ptr<SimpleOfdmWimaxPhy> bsPhy = 	DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(
					bsDevs.Get(0)->GetObject<BaseStationNetDevice>()->GetPhy());
	bsPhy->SetTxPower(bsTxPower);
	bsPhy->SetTxGain(16);
	bsPhy->SetRxGain(16);
//	bsPhy->SetFrameDuration(Seconds(0.005));

	bsNodes.Get(0)->AggregateObject(BSPosition);
	//BSPosition.m_txFreqVector = 1827;

	Ptr<ConstantPositionMobilityModel> bp = bsNodes.Get(0)->GetObject<ConstantPositionMobilityModel>();
	//bp->m_txFreq = 1827;

	cout << "BS Location is: " << bp->GetPosition() << endl;

	cout << "-aa-" << endl;

	InternetStackHelper stack;
	stack.Install(bsNodes);
	Ipv4AddressHelper address;
	address.SetBase("10.1.1.0", "255.255.255.0");
	Ipv4InterfaceContainer BSinterface = address.Assign(bsDevs);

	bs->TraceConnectWithoutContext("BSTxDrop",MakeCallback(&MacTxDropDL));//MAC Drop
	bs->TraceConnectWithoutContext("BSRxDrop",MakeCallback(&MacRxDropUL));//MAC Drop
	bs->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropDL));
	bs->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropUL));


	/*COMMON part for UDP and TCP per application*/
	NS_LOG_UNCOND("Common UDP Protection Setup.");
	NodeContainer ssNodesPtct;
	Ptr<SubscriberStationNetDevice> ssPtct[numPtctUsers];
	Ipv4InterfaceContainer SSinterfacesPtct;
//	UdpServerHelper* udpServerPtctUL = new UdpServerHelper[numPtctUsers];
	//UdpClientHelper* udpClientPtctUL = new UdpClientHelper[numPtctUsers];
//	ApplicationContainer *clientAppsPtctUL = new ApplicationContainer[numPtctUsers];
	if (numPtctUsers > 0) {
		NodeConfigurationCommonSetupUDP(bsNodes, "Ptct", wimax, numPtctUsers, created_nodes,
				areaRadius, stack, address, scheduler, modulation, ssNodesPtct, ssPtct,
				SSinterfacesPtct);
		printLocations("Ptct", ssPtct, numPtctUsers);
		for (int i=0;i<numPtctUsers;i++)
		{
			//ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxEnd",MakeCallback(&TraceFuncPhyRxEnd));
			ssPtct[i]->TraceConnectWithoutContext("SSTxDrop",MakeCallback(&MacTxDropUL));
			ssPtct[i]->TraceConnectWithoutContext("SSRxDrop",MakeCallback(&MacRxDropUL));
			ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropUL));
			ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropDL));
		}
	}

	NS_LOG_UNCOND("Common UDP WASA Setup.");
	NodeContainer ssNodesWASA;
	Ptr<SubscriberStationNetDevice> ssWASA[numWASAUsers];
	Ipv4InterfaceContainer SSinterfacesWASA;
	if (numWASAUsers > 0) {
		NodeConfigurationCommonSetupUDP(bsNodes, "WASA", wimax, numWASAUsers, created_nodes,
				areaRadius, stack, address, scheduler, modulation, ssNodesWASA, ssWASA,
				SSinterfacesWASA);
		printLocations("WASA", ssWASA, numWASAUsers);
		for (int i=0;i<numWASAUsers;i++)
		{
			//ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxEnd",MakeCallback(&TraceFuncPhyRxEnd));
			ssWASA[i]->TraceConnectWithoutContext("SSTxDrop",MakeCallback(&MacTxDropUL));
			ssWASA[i]->TraceConnectWithoutContext("SSRxDrop",MakeCallback(&MacRxDropUL));
			ssWASA[i]->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropUL));
			ssWASA[i]->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropDL));
		}
	}

	NS_LOG_UNCOND("Common UDP Monitoring Setup.");
	NodeContainer ssNodesMon;
	Ptr<SubscriberStationNetDevice> ssMon[numMonUsers];
	Ipv4InterfaceContainer SSinterfacesMon;
//	UdpServerHelper* udpServerMontUL = new UdpServerHelper[numMonUsers];
//	UdpClientHelper* udpClientMontUL = new UdpClientHelper[numMonUsers];
	UdpServerHelper* udpServerMonDL = new UdpServerHelper[numMonUsers];
//	ApplicationContainer *clientAppsMontUL = new ApplicationContainer[numMonUsers];
	if (numMonUsers > 0) {
		NodeConfigurationCommonSetupUDP(bsNodes, "Mont", wimax, numMonUsers, created_nodes,
				areaRadius, stack, address, scheduler, modulation, ssNodesMon, ssMon,
				SSinterfacesMon);
		printLocations("Mont", ssMon, numMonUsers);
		for (int i=0;i<numMonUsers;i++)
		{
			//ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxEnd",MakeCallback(&TraceFuncPhyRxEnd));
			ssMon[i]->TraceConnectWithoutContext("SSTxDrop",MakeCallback(&MacTxDropUL));
			ssMon[i]->TraceConnectWithoutContext("SSRxDrop",MakeCallback(&MacRxDropDL));
			ssMon[i]->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropUL));
			ssMon[i]->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropDL));
		}
	}

	/***********DL to be added, UL and DL nodes should not be seperated.
	 //	NS_LOG_UNCOND("Common TCP Monitoring (DL) Setup.");
	 NodeContainer ssNodesMonDL;
	 Ptr<SubscriberStationNetDevice> ssMonDL[numMonUsers];
	 std::vector<Ipv4InterfaceContainer> MonDLinterfaceAdjacencyList (numMonUsers);
	 if (numMonUsers > 0)
	 {
	 NodeConfigurationCommonSetupTCP(wimax,numMonUsers,created_nodes,areaRadius,stack,address,scheduler,bsDevs,bsNodes,ssNodesMonDL,ssMonDL,MonDLinterfaceAdjacencyList);
	 }
	 */

	NS_LOG_UNCOND("Common UDP Control Setup.");
	NodeContainer ssNodesCtrl;
	Ptr<SubscriberStationNetDevice> ssCtrl[numCtrlUsers];
	Ipv4InterfaceContainer SSinterfacesCtrl;
	//std::vector<Ipv4InterfaceContainer> CtrlinterfaceAdjacencyList (numCtrlUsers);
	if (numCtrlUsers > 0) {
		NodeConfigurationCommonSetupUDP(bsNodes, "Ctrl", wimax, numCtrlUsers, created_nodes,
				areaRadius, stack, address, scheduler, modulation, ssNodesCtrl, ssCtrl,
				SSinterfacesCtrl);
		printLocations("Ctrl", ssCtrl, numCtrlUsers);
		for (int i=0;i<numCtrlUsers;i++)
		{
			//ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxEnd",MakeCallback(&TraceFuncPhyRxEnd));
			ssCtrl[i]->TraceConnectWithoutContext("SSTxDrop",MakeCallback(&MacTxDropUL));
			ssCtrl[i]->TraceConnectWithoutContext("SSRxDrop",MakeCallback(&MacRxDropUL));
			ssCtrl[i]->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropUL));
			ssCtrl[i]->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropDL));
		}
	}

	NodeContainer ssNodesSM;
	Ptr<SubscriberStationNetDevice> ssSM[numSMUsers];
	Ipv4InterfaceContainer SSinterfacesSM;
//	std::vector<Ipv4InterfaceContainer> SMinterfaceAdjacencyList (numSMUsers);
	NodeContainer ssNodesSMUL;
	Ptr<SubscriberStationNetDevice> ssSMUL[numSMUsers];
	Ipv4InterfaceContainer SSinterfacesSMUL;
	UdpServerHelper* udpServerSMUL = new UdpServerHelper[numSMUsers];
	UdpClientHelper* udpClientSMUL = new UdpClientHelper[numSMUsers];
	ApplicationContainer *clientAppsSMUL = new ApplicationContainer[numSMUsers];

	//UdpServerHelper udpServerPtctDL[numPtctUsers];
	/*if (numAggregators ==0)
	 {*/
	//NS_LOG_UNCOND("Common TCP Smart Meters Setup.");
	if (numSMUsers > 0) {
		//areaRadius = 0.1;
		NodeConfigurationCommonSetupUDP(bsNodes, "SM", wimax, numSMUsers, created_nodes,
				areaRadius, stack, address, scheduler, modulation, ssNodesSM, ssSM,
				SSinterfacesSM);
		printLocations("SM", ssSM, numSMUsers);
		for (int i=0;i<numSMUsers;i++)
		{
			//ssPtct[i]->GetPhy()->TraceConnectWithoutContext("PhyRxEnd",MakeCallback(&TraceFuncPhyRxEnd));
			ssSM[i]->TraceConnectWithoutContext("SSTxDrop",MakeCallback(&MacTxDropUL));
			ssSM[i]->TraceConnectWithoutContext("SSRxDrop",MakeCallback(&MacRxDropUL));
			ssSM[i]->GetPhy()->TraceConnectWithoutContext("PhyTxDrop",MakeCallback(&PhyTxDropUL));
			ssSM[i]->GetPhy()->TraceConnectWithoutContext("PhyRxDrop",MakeCallback(&PhyRxDropDL));
		}
	}
	//return 0;
	//}
	/*else
	 {
	 NS_LOG_UNCOND("Common UDP Smart Meters (UL Agg.) Setup.");
	 if (numMonUsers > 0)
	 {
	 NodeConfigurationCommonSetupUDP(wimax,numSMUsers,created_nodes,areaRadius,stack,address,scheduler,ssNodesSMUL,ssSMUL,SSinterfacesSMUL);
	 }
	 }*/
	/* END COMMON*/


	/*-------------Traffic Generation (UL and then DL)-----------------*/
	if (enableUplink) {
		//NodeConfigurationCommonSetup(wimax,5,stack,address,scheduler,ssNodes,ss,SSinterfaces);
		//Control is TCP:
		appnumBeforeWASAUL = bsNodes.Get(0)->GetNApplications();
		if (numWASAUsers > 0) {
			NS_LOG_UNCOND("Setting up UL UDP for WASA.");
			//cout<< numWASAUsers << std::endl;
	//		NS_ASSERT(false);
			port = setup_udp_onoff(numWASAUsers, fidWASAUL, ssNodesWASA,
					SSinterfacesWASA, ssWASA, bsNodes, BSinterface, duration,
					"ns3::ConstantRandomVariable[Constant=1]",
					"ns3::ConstantRandomVariable[Constant=5]", 256, "5kb/s", port, wimax,
					modulation, ServiceFlow::SF_TYPE_NRTPS,0);
		}
		appnumAfterWASAUL = bsNodes.Get(0)->GetNApplications();

		//Protection is UDP:
		appnumBeforePtctUL = bsNodes.Get(0)->GetNApplications();
		if (numPtctUsers > 0) {

			NS_LOG_UNCOND("Setting up UL UDP for Protection.");
			port = setup_udp_onoff(numPtctUsers, fidPtctUL, ssNodesPtct,
					SSinterfacesPtct, ssPtct, bsNodes, BSinterface, duration,
					"ns3::ExponentialRandomVariable[Mean=1]",
					"ns3::ExponentialRandomVariable[Mean=0.1]", 192, "150kb/s", port, wimax,
					modulation, ServiceFlow::SF_TYPE_UGS, 10);	//"150kb/s", pkt_size = 128
			//port = Uplink_UDP_Traffic(wimax, port, numPtctUsers, duration, BSinterface, SSinterfacesPtct, bsNodes, ssNodesPtct, ssPtct,2.0, fidPtctUL, 128, udpClientPtctUL, udpServerPtctUL, clientAppsPtctUL);
		}
		appnumAfterPtctUL = bsNodes.Get(0)->GetNApplications();

		//Monitoring UL is UDP:
		appnumBeforeMonUL = bsNodes.Get(0)->GetNApplications();

		 if (numMonUsers > 0)
		 {
			 NS_LOG_UNCOND("Setting up UL UDP for Monitoring.");
			 port = setup_udp_onoff(numMonUsers, fidMonUL, ssNodesMon, SSinterfacesMon,  ssMon,
					 bsNodes, BSinterface, duration,
					 "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=0]",
			 384,"300kb/s",//300, pkt_size = 256
			 port,wimax, modulation, ServiceFlow::SF_TYPE_UGS, 100);
			 //port = Uplink_UDP_Traffic(wimax, port, numMonUsers, duration, BSinterface, SSinterfacesMon, bsNodes, ssNodesMon, ssMon,0.0066, fidMonUL, 256, udpClientMontUL, udpServerMontUL, clientAppsMontUL, ServiceFlow::SF_TYPE_UGS);
		 }

		appnumAfterMonUL = bsNodes.Get(0)->GetNApplications();

		//Control is TCP:
		appnumBeforeCtrlUL = bsNodes.Get(0)->GetNApplications();
		if (numCtrlUsers > 0) {
			NS_LOG_UNCOND("Setting up UL UDP for control.");
			//UdpServerHelper* udpServerMontUL = new UdpServerHelper[numMonUsers];
			port = setup_udp_onoff(numCtrlUsers, fidCtrlUL, ssNodesCtrl,
					SSinterfacesCtrl, ssCtrl, bsNodes, BSinterface, duration,
					"ns3::ExponentialRandomVariable[Mean=1]",
					"ns3::ExponentialRandomVariable[Mean=0]", 128, "5kb/s", port, wimax,
					modulation, ServiceFlow::SF_TYPE_RTPS, 0);

			/*fid, tmpNodes3 , tmpIfs3, sstmp3,
			 bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
			 on_time, off_time, pkt_size, data_rate, startPort, wimaxAlcona[sectNum],
			 modulation);*/
			//(wimax, port, numCtrlUsers, duration, BSinterface, SSinterfacesCtrl,  bsNodes, ssNodesCtrl, ssCtrl,
			//"ns3::ExponentialRandomVariable[Mean=1]", "ns3::ExponentialRandomVariable[Mean=5]", 1, ServiceFlow::SF_TYPE_RTPS, "5kb/s", 128);
			//"ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=5]", 1, ServiceFlow::SF_TYPE_RTPS, "5kb/s", 128);
		}
		appnumAfterCtrlUL = bsNodes.Get(0)->GetNApplications();

		//SmartMetering is TCP:
		appnumBeforeSMUL = bsNodes.Get(0)->GetNApplications();
		if (numSMUsers > 0) {
			if (numAggregators == 0) {
				std::ostringstream onTimeSMULStr, offTimeSMULStr, dataRateKBSMULStr;
				onTimeSMULStr << "ns3::ConstantRandomVariable[Constant=" << onTimeSMUL
						<< "]";
				offTimeSMULStr << "ns3::ConstantRandomVariable[Constant=" << offTimeSMUL
						<< "]";
				dataRateKBSMULStr << dataRateKBSMUL << "kb/s";
				NS_LOG_UNCOND("Setting up UL UDP for smart metering, onTime: " << onTimeSMULStr.str() <<
						", offTime: " << offTimeSMULStr.str() << ", dataRate: " << dataRateKBSMULStr.str());
				port = setup_udp_onoff(numSMUsers, fidSMUL, ssNodesSM, SSinterfacesSM,
						ssSM, bsNodes, BSinterface, duration,
						(std::string) onTimeSMULStr.str(),
						(std::string) offTimeSMULStr.str(), 128,
						(std::string) dataRateKBSMULStr.str(), port, wimax, modulation,
						ServiceFlow::SF_TYPE_BE);
				/*port = Uplink_TCP_Traffic(wimax, port, numSMUsers, duration, BSinterface, SMinterfaceAdjacencyList, bsNodes, ssNodesSM, ssSM,
				 onTimeSMULStr.str(), offTimeSMULStr.str(), 3, ServiceFlow::SF_TYPE_RTPS, dataRateKBSMULStr.str(), 128); //SF_TYPE_BE*/
			} else {
				NS_LOG_UNCOND("Setting up UL UDP for smart metering (aggregation), dataRate: " << dataRateKBSMUL);
				port = Uplink_UDP_Traffic(wimax, port, numSMUsers, duration,
						BSinterface, SSinterfacesSMUL, bsNodes, ssNodesSMUL, ssSMUL,
						128.0 / dataRateKBSMUL, fidAgg, 128, udpClientSMUL, udpServerSMUL,
						clientAppsSMUL, ServiceFlow::SF_TYPE_BE);
			}
		}
		appnumAfterSMUL = bsNodes.Get(0)->GetNApplications();

	}

	if (enableDownlink) {
		//Control is TCP:
		appnumBeforeWASADL = bsNodes.Get(0)->GetNApplications();
		if (numWASAUsers > 0) {
			NS_LOG_UNCOND("Setting up DL UDP for WASA.");
			port = Downlink_onoff_Traffic(numWASAUsers, fidWASADL, ssNodesWASA,
					SSinterfacesWASA, ssWASA, bsNodes, BSinterface, duration,
					"ns3::ConstantRandomVariable[Constant=1]",
					"ns3::ConstantRandomVariable[Constant=5]",		  //5
					256, "1kb/s", port, wimax, modulation, ServiceFlow::SF_TYPE_NRTPS);
		}
		appnumAfterWASADL = bsNodes.Get(0)->GetNApplications();

		appnumBeforePtctDL = bsNodes.Get(0)->GetNApplications();
		if (numPtctUsers > 0) {
			NS_LOG_UNCOND("Setting up DL UDP for Protection.");

			port = Downlink_onoff_Traffic(numPtctUsers, fidPtctDL, ssNodesPtct,
					SSinterfacesPtct, ssPtct, bsNodes, BSinterface, duration,
					"ns3::ExponentialRandomVariable[Mean=1]",
					"ns3::ExponentialRandomVariable[Mean=0.1]", 128, "150kb/s", port,
					wimax, modulation, ServiceFlow::SF_TYPE_UGS, 10); //128
			//	port = Downlink_UDP_Traffic(wimax, port, numPtctUsers, duration, SSinterfacesPtct, bsNodes, ssNodesPtct,
			//		ssPtct, fidPtctDL, 128, udpServerPtctDL);
		}
		appnumAfterPtctDL = bsNodes.Get(0)->GetNApplications();

		//Monitoring is TCP:
		appnumBeforeMonDL = bsNodes.Get(0)->GetNApplications();
		double monDLInterval = 0.1;
		if (numMonUsers > 0) {
			NS_LOG_UNCOND("Setting up DL UDP for monitoring.");
			port = Downlink_UDP_Traffic(wimax, port, numMonUsers, duration,
					SSinterfacesMon, bsNodes, ssNodesMon, ssMon, fidMonDL, 128,
					monDLInterval, udpServerMonDL, ServiceFlow::SF_TYPE_BE, 100);
			/*port = Downlink_onoff_Traffic(numMonUsers, fidMonDL, ssNodesMon, SSinterfacesMon,
			 ssMon, bsNodes, BSinterface, duration,
			 "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=0]",
			 128,"10kb/s", port,wimax, modulation);*/
			/*port = Downlink_UDP_Traffic(wimax, port, numMonUsers, duration, SSinterfacesMont, bsNodes, ssNodesMon, ssMon,
			 "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=5]", 4, ServiceFlow::SF_TYPE_NRTPS, "10kb/s", 256);*/

		}
		appnumAfterMonDL = bsNodes.Get(0)->GetNApplications();

		//Control is TCP:
		appnumBeforeCtrlDL = bsNodes.Get(0)->GetNApplications();
		if (numCtrlUsers > 0) {
			NS_LOG_UNCOND("Setting up DL UDP for control.");
			port = Downlink_onoff_Traffic(numCtrlUsers, fidCtrlDL, ssNodesCtrl,
					SSinterfacesCtrl, ssCtrl, bsNodes, BSinterface, duration,
					"ns3::ConstantRandomVariable[Constant=1]",
					"ns3::ConstantRandomVariable[Constant=5]",			//5
					128, "1kb/s", port, wimax, modulation, ServiceFlow::SF_TYPE_RTPS);
		}
		appnumAfterCtrlDL = bsNodes.Get(0)->GetNApplications();

//		//SmartMetering is TCP:
//		appnumBeforeSMDL = bsNodes.Get(0)->GetNApplications();
//		if (numSMUsers > 0)
//		{
//			NS_LOG_UNCOND("Setting up DL TCP for smart metering.");
//			port = Downlink_TCP_Traffic(wimax, port, numSMUsers, duration, BSinterface, SMDLinterfaceAdjacencyList, bsNodes, ssNodesSMDL, ssSMDL,
//					"ns3::ConstantRandomVariable[Constant=0.1]", "ns3::ConstantRandomVariable[Constant=4]", 7, ServiceFlow::SF_TYPE_BE, "50b/s", 128);
//		}
//		appnumAfterSMDL = bsNodes.Get(0)->GetNApplications();
	}

	//connect:
	//Config::Connect()
//	Config::ConnectWithoutContext(
//			"/NodeList/*/DeviceList/*/$ns3::WimaxNetDevice/Phy/$ns3::SimpleOfdmWimaxPhy/PhyRxDrop",
//			MakeCallback(&TraceFuncPhyRxDrop));
//	Config::Connect(
//			"/NodeList/*/DeviceList/*/$ns3::WimaxNetDevice/Phy/$ns3::SimpleOfdmWimaxPhy/PhyRxEnd",
//			MakeCallback(&TraceFuncPhyRxEnd));

	// Flow Monitor
	FlowMonitorHelper flowmonHelper;
	Ptr<FlowMonitor> monitor =  flowmonHelper.InstallAll();
//	Ptr<Ipv4FlowProbe> ipv4fp= new Ipv4FlowProbe(monitor);

	//print MAC and Phy Drops every 5 seconds:
	Simulator::Schedule(Seconds(5.0), &PrintTotalDropCounts);
//	NS_ASSERT(false);
	//run
	Simulator::Stop(Seconds(duration + 0.1)); //Seconds(1));
	NS_LOG_UNCOND ("Starting simulation for " << numPtctUsers << " Protection Users, " <<
			numWASAUsers << " WASA Users, " << numMonUsers << " Monitoring Users, " <<
			numCtrlUsers << " Control Users, and " <<
			numSMUsers << " Smart Meters (SM UL only rate " << dataRateKBSMUL << "kbps)...");
	NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink<<numAggregators
			<< ", UL/DL Frame-ratio = " << bs->GetNrUlSymbols() << "/" << (double)bs->GetNrDlSymbols() << "="
			<< bs->GetNrUlSymbols()/(double)bs->GetNrDlSymbols() << ", Max allowed modulation=" << modulation);
	Simulator::Run();
	NS_LOG_UNCOND ("Printing statistics for " << numPtctUsers << " Protection Users, " <<
			numWASAUsers << " WASA Users, " << numMonUsers << " Monitoring Users, " <<
			numCtrlUsers << " Control Users, and " <<
			numSMUsers << " Smart Meters (SM UL only rate " << dataRateKBSMUL << "kbps)...");
	NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink<<numAggregators
			<< ", UL/DL Frame-ratio = " << bs->GetNrUlSymbols() << "/" << (double)bs->GetNrDlSymbols() << "="
			<< bs->GetNrUlSymbols()/(double)bs->GetNrDlSymbols() << ", Max allowed modulation=" << modulation);

	/******************************GET STATISTICS**********************************************************/

	/**********************  Using FlowMon****************************************************************/
	// Print per flow statistics
  monitor->CheckForLostPackets ();
	vector<Ptr<FlowProbe> > allProbes = monitor->GetAllProbes();
	NS_LOG_UNCOND("allProbes.size()="<<allProbes.size());
	fstream f;
	f.open("ipv4Probe.xml",fstream::out);
	int pnum =0;
	for (vector<Ptr<FlowProbe> >::const_iterator fit=allProbes.begin();fit!=allProbes.end();++fit)
	{
		Ptr<Ipv4FlowProbe> fit4 = DynamicCast<Ipv4FlowProbe>(*fit);
		pnum++;
		fit4->SerializeToXmlStream(f,0,pnum);
		//fit4->
		//fit->SerializeToXmlStream(cout,0,0);
	}
	f.close();


	  /*Ptr<IpcsClassifier> cl =  flowmonHelper.GetClassifier();
	  cl->*/
		double tptUl=0.0,tptDl=0.0;
		double tpt;
	  Ptr<Ipv4FlowClassifier> classifier = DynamicCast<Ipv4FlowClassifier > (flowmonHelper.GetClassifier ());
	  map<FlowId, FlowMonitor::FlowStats> stats = monitor->GetFlowStats ();
	  for (map<FlowId, FlowMonitor::FlowStats>::const_iterator iter = stats.begin (); iter != stats.end (); ++iter)
	  {
	  	//iter->second.
	  	Ipv4FlowClassifier::FiveTuple t = classifier->FindFlow (iter->first);
	  	//ns3::Ipv4FlowClassifier::FiveTuple t =  classifier->FindFlow(iter->first);
	  	tpt = iter->second.rxBytes * 8.0 / (iter->second.timeLastRxPacket.GetSeconds()-iter->second.timeFirstTxPacket.GetSeconds());
	  	if (t.sourceAddress.IsEqual(Ipv4Address("10.1.1.1")))
	  		tptDl+=tpt;
			else if  (t.destinationAddress.IsEqual(Ipv4Address("10.1.1.1")))
				tptUl+=tpt;
			//else
				//NS_LOG_UNCOND("!!!The Following FlowMon is neither DL nor UL!!!");

			/*
			 NS_LOG_UNCOND("FlowMon ID: " << iter->first << " Src Addr: " << t.sourceAddress	<< " Dst Addr: " << t.destinationAddress);
			NS_LOG_UNCOND("Tx Packets = " << iter->second.txPackets);
			NS_LOG_UNCOND("Rx Packets = " << iter->second.rxPackets);
			NS_LOG_UNCOND("Lost Packets = " << iter->second.lostPackets);
			NS_LOG_UNCOND("Size of iter->second.packetsDropped = " << iter->second.packetsDropped.size());
			int drnum=0;
			for (vector<unsigned int>::const_iterator dr=iter->second.packetsDropped.begin(); dr!=iter->second.packetsDropped.end();dr++)
			{
				NS_LOG_UNCOND("Dropped Pkts due to reason#" << drnum << "=" << (int)*dr);
				drnum++;
			}
			NS_LOG_UNCOND("Throughput: " << tpt / 1024  << " Kbps");
			*/
		}
	  monitor->SerializeToXmlFile ("Wimax-simple-UDP.flowmon1", false, false);
	  //	monitor->SetAttribute

  /************************************* Using Our method ****************************************/
	if (enableUplink) {
		/*for (int i=0; i < numPtctUsers; i++)
		 {
		 cout << i << endl;
		 Ptr<UdpServer> myserver = udpServerPtctUL[i].GetServer();
		 cout << "?" << endl;
		 cout << myserver->GetLost() << endl;
		 cout << losstPtct[i]  << endl;
		 losstPtct[i] = myserver->GetLost();
		 sumLosstPtct += losstPtct[i];
		 cout << "/1/" << endl;

		 rcvddPtct[i] = udpServerPtctUL[i].GetServer()->GetReceived();
		 sumRcvdPtct += rcvddPtct[i];
		 cout << "/2/" << endl;


		 //			Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(ssDevsAlcona[sectNum].Get(0)->GetObject<SubscriberStationNetDevice>()->GetPhy());
		 Ptr<UdpClient> cl = DynamicCast<UdpClient,Application>(clientAppsPtctUL[i].Get(0));
		 //cl = Ptr<UdpClient>(DynamicCast<UdpClient *,Application *>()));
		 senttPtctFromApp[i] = cl->m_sent;
		 sumSenttPtctFromApp += senttPtctFromApp[i];
		 cout << "/3/" << endl;

		 Ptr<Node> myNode = ssNodesPtct.Get(i);
		 senttPtct[i] = myNode->m_Sent[fidPtctUL];
		 sumSentPtct += senttPtct[i];
		 }*/

		/*	cout << "sumSentPtct =" << sumSentPtct << endl
		 << "sumSenttPtctFromApp=" << sumSenttPtctFromApp << endl
		 << "sumRcvdPtct=" << sumRcvdPtct << endl
		 << "sumLosstPtct=" << sumLosstPtct << endl;*/

		//Statistics for Monitoring Apps
		/*uint32_t losstMont[numMonUsers], sumLosstMont=0;
		 uint32_t senttMont[numMonUsers], sumSentMont=0;
		 uint32_t senttMontFromApp[numPtctUsers], sumSenttMontFromApp=0;
		 uint32_t rcvddMont[numMonUsers], sumRcvdMont=0;

		 for (int i=0; i < numMonUsers; i++)
		 {
		 cout << i << endl;
		 Ptr<UdpServer> myserver = udpServerMontUL[i].GetServer();
		 cout << "?" << endl;
		 //		cout << myserver->GetLost() << endl;
		 //		cout << losstMont[i]  << endl;
		 losstMont[i] = myserver->GetLost();
		 sumLosstMont += losstMont[i];
		 //		cout << "1" << endl;

		 rcvddMont[i] = udpServerMontUL[i].GetServer()->GetReceived();
		 sumRcvdMont += rcvddMont[i];
		 //		cout << "2" << endl;

		 Ptr<UdpClient> cl = DynamicCast<UdpClient,Application>(clientAppsMontUL[i].Get(0));
		 senttMontFromApp[i] = cl->m_sent;
		 sumSenttMontFromApp += senttMontFromApp[i];

		 Ptr<Node> myNode = ssNodesMon.Get(i);
		 senttMont[i] = myNode->m_Sent[fidMonUL];
		 sumSentMont += senttMont[i];
		 }
		 cout << "sumSentMont =" << sumSentMont << endl
		 << "sumSenttMontFromApp=" << sumSenttMontFromApp << endl
		 << "sumRcvdMont=" << sumRcvdMont << endl
		 <<	"sumLosstMont=" << sumLosstMont << endl;*/

		//Statistics for Smart Metering Apps
		uint32_t losstSM[numSMUsers], sumLosstSM = 0;
		uint32_t senttSM[numSMUsers], sumSentSM = 0;
		uint32_t rcvddSM[numSMUsers], sumRcvdSM = 0;

		if (numAggregators != 0) {
			for (int i = 0; i < numSMUsers; i++) {
				cout << i << endl;
				Ptr<UdpServer> myserver = udpServerSMUL[i].GetServer();
				//	cout << "?" << endl;
				cout << myserver->GetLost() << endl;
				cout << losstSM[i] << endl;
				losstSM[i] = myserver->GetLost();
				sumLosstSM += losstSM[i];
				//	cout << "1" << endl;

				rcvddSM[i] = udpServerSMUL[i].GetServer()->GetReceived();
				sumRcvdSM += rcvddSM[i];
				//	cout << "2" << endl;

				Ptr<Node> myNode = ssNodesSMUL.Get(i);
				senttSM[i] = myNode->m_Sent[fidSMUL];
				sumSentSM += senttSM[i];
			}
			cout << "sumSentSM =" << sumSentSM << endl << "sumRcvdSM=" << sumRcvdSM
					<< endl << "sumLosstSM=" << sumLosstSM << endl;
		}
	}




	//Print Statistics:
	int reliable_received_all_nodes[FLOW_NUM];
	//ns3::Time latency_all_nodes[FLOW_NUM];
	Time Delays_all_nodes[FLOW_NUM];
	Time ReliableDelays_all_nodes[FLOW_NUM];
	int Recv_all_nodes[FLOW_NUM];
	double avg_Delays_all_nodes[FLOW_NUM];
	double avg_ReliableDelays_all_nodes[FLOW_NUM];

	for (int flowid = 0; flowid < FLOW_NUM; flowid++) {
		reliable_received_all_nodes[flowid] = 0;
		Delays_all_nodes[flowid] = Time(0.0);
		ReliableDelays_all_nodes[flowid] = Time(0.0);
		Recv_all_nodes[flowid] = 0;

		avg_Delays_all_nodes[flowid] = 0;
		avg_ReliableDelays_all_nodes[flowid] = 0;

		/*
		 min_Delays_all_nodes[flowid] = 0;
		 min_ReliableDelays_all_nodes[flowid] = 0;

		 var_Delays_all_nodes[flowid] = 0;
		 var_ReliableDelays_all_nodes[flowid] = 0;
		 */
	}
	int Sent[FLOW_NUM]; //, Recv[FLOW_NUM];
	int phyDropped[FLOW_NUM];
	int totalUlSent = 0, totalDlSent =0, totalPhyDlDropped=0;
	for (int flowid = 0; flowid < FLOW_NUM; flowid++)
	{
		Sent[flowid] = 0;
		phyDropped[flowid] = 0;
	}
	if (enableUplink) {
		Ptr<Node> myNode = bsNodes.Get(0);
		for (int flowid = 0; flowid < START_DL_FID; flowid++) {
			//NS_LOG_UNCOND(sectNum << 	":" << flowid  );
			reliable_received_all_nodes[flowid] += myNode->reliable_received[flowid];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[flowid] += myNode->Delays[flowid];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[flowid] += myNode->ReliableDelays[flowid];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[flowid] += myNode->Recv[flowid];
			//NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
		}
		//UL Sent: Ptct
		for (int i = 0; i < numPtctUsers; i++) {
			Sent[fidPtctUL] += ssNodesPtct.Get(i)->m_Sent/*sentBytes*/[fidPtctUL]; //m_Sent[flowid]
					}
		//UL Sent: WASA
		for (int i = 0; i < numWASAUsers; i++) {
			Sent[fidWASAUL] += ssNodesWASA.Get(i)->m_Sent/*sentBytes*/[fidWASAUL]; //m_Sent[flowid]
			}
		//UL Sent: Mont
		for (int i = 0; i < numMonUsers; i++) {
			Sent[fidMonUL] += ssNodesMon.Get(i)->m_Sent/*sentBytes*/[fidMonUL]; //m_Sent[flowid]
		}
		//UL Sent: Ctrl
		for (int i = 0; i < numCtrlUsers; i++) {
			Sent[fidCtrlUL] += ssNodesCtrl.Get(i)->m_Sent/*sentBytes*/[fidCtrlUL]; //m_Sent[flowid]
		}
		//cout << "be ruye" << endl;
		//UL Sent: SM
		for (int i = 0; i < numSMUsers; i++) {
			Sent[fidSMUL] += ssNodesSM.Get(i)->m_Sent/*sentBytes*/[fidSMUL]; //m_Sent[flowid]
			/*Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy> (ssSM[i]->GetPhy());
			NS_LOG_UNCOND("PHY drops in node#"<<i<< "of SM="<<myPhy->m_drop);
			phyDropped[fidSMUL] += myPhy->m_drop;*/
		}
		totalUlSent = Sent[fidWASAUL]+Sent[fidMonUL]+Sent[fidCtrlUL]+Sent[fidSMUL]+Sent[fidPtctUL];

	} //enableUplink:Sent

	if (enableDownlink)
	{
		//DL WASA
		for (int i = 0; i < numWASAUsers; i++) {
			reliable_received_all_nodes[fidWASADL] +=
					ssNodesWASA.Get(i)->reliable_received[fidWASADL];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->Delays[fidWASADL];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[fidWASADL] +=
			ssNodesWASA.Get(i)->ReliableDelays[fidWASADL];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->Recv[fidWASADL];
			Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy> (ssWASA[i]->GetPhy());
			NS_LOG_UNCOND("PHY drops in node#"<<i<< "of WASA="<<myPhy->m_drop);
			phyDropped[fidWASADL] += myPhy->m_drop;
		}
		//DL Ptct
		for (int i = 0; i < numPtctUsers; i++) {
			reliable_received_all_nodes[fidPtctDL] +=
					ssNodesPtct.Get(i)->reliable_received[fidPtctDL];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->Delays[fidPtctDL];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[fidPtctDL] +=
					ssNodesPtct.Get(i)->ReliableDelays[fidPtctDL];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->Recv[fidPtctDL];
			Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy> (ssPtct[i]->GetPhy());
			NS_LOG_UNCOND("PHY drops in node#"<<i<< "of PTCT="<<myPhy->m_drop);
			phyDropped[fidPtctDL] += myPhy->m_drop;
		}
		//DL Mont
		for (int i = 0; i < numMonUsers; i++) {
			reliable_received_all_nodes[fidMonDL] +=
					ssNodesMon.Get(i)->reliable_received[fidMonDL];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[fidMonDL] += ssNodesMon.Get(i)->Delays[fidMonDL];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[fidMonDL] +=
					ssNodesMon.Get(i)->ReliableDelays[fidMonDL];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[fidMonDL] += ssNodesMon.Get(i)->Recv[fidMonDL];
			Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy> (ssMon[i]->GetPhy());
			NS_LOG_UNCOND("PHY drops in node#"<<i<< "of MONT="<<myPhy->m_drop);
			phyDropped[fidMonDL] += myPhy->m_drop;
		}
		//DL Ctrl
		for (int i = 0; i < numCtrlUsers; i++) {
			reliable_received_all_nodes[fidCtrlDL] +=
					ssNodesCtrl.Get(i)->reliable_received[fidCtrlDL];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->Delays[fidCtrlDL];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[fidCtrlDL] +=
					ssNodesCtrl.Get(i)->ReliableDelays[fidCtrlDL];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->Recv[fidCtrlDL];
			Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy> (ssCtrl[i]->GetPhy());
			NS_LOG_UNCOND("PHY drops in node#"<<i<< "of CTRL="<<myPhy->m_drop);
			phyDropped[fidCtrlDL] += myPhy->m_drop;
		}
		for (int flowid = START_DL_FID; flowid < END_DL_FID; flowid++) {
			Sent[flowid] += bsNodes.Get(0)->m_Sent/*sentBytes*/[flowid];
			totalDlSent += Sent[flowid];
		}
		totalPhyDlDropped = phyDropped[fidWASADL]+phyDropped[fidMonDL]+
		phyDropped[fidCtrlDL]+phyDropped[fidPtctDL];//phyDropped[fidSMDL]+
	}//enableDownlink

	cout << "flow ID\tsent\t\treliable rcvd\treliable delay\tall rcvd\tall delay\tphy drop"
			<< endl;
	//cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
	int cnt = -1;
	int totalUlRcvd = 0;
	int totalDlRcvd = 0;
	//int totalULRcvdBytes = 0, totalDLRcvdBytes = 0;
	//double ULThroughput = 0;
	//double DLThroughput = 0;
	if (enableUplink)
		cnt = 0;
	else
		cnt = START_DL_FID;
	for (int flowid = cnt; flowid < END_DL_FID; flowid++) {
		avg_ReliableDelays_all_nodes[flowid] =
				ReliableDelays_all_nodes[flowid].ToDouble(Time::MS)
						/ (double) reliable_received_all_nodes[flowid];
		avg_Delays_all_nodes[flowid] = Delays_all_nodes[flowid].ToDouble(Time::MS)
				/ (double) Recv_all_nodes[flowid];

		if (flowid<START_DL_FID)
			totalUlRcvd += Recv_all_nodes[flowid];
		else
			totalDlRcvd += Recv_all_nodes[flowid];

		cout << flowid << " (" << flowNames[flowid] << ")\t" << Sent[flowid] << "\t\t"
				<< reliable_received_all_nodes[flowid] << "\t\t"
				<< avg_ReliableDelays_all_nodes[flowid] << "\t\t"
				<< Recv_all_nodes[flowid] << "\t\t" << avg_Delays_all_nodes[flowid] << "\t\t"
				<< phyDropped[flowid]
				<< endl;
	}


	//Bytes:
	//totalULRcvdBytes = Recv_all_nodes[fidPtctUL] * 128 + Recv_all_nodes[fidMonUL] * 256 + Recv_all_nodes[fidCtrlUL] * 128 +
	//		Recv_all_nodes[fidWASAUL] * 256 + Recv_all_nodes[fidSMUL] * 128;
	//ULThroughput = totalULRcvdBytes/(double)100.0; //duration;

	//totalDLRcvdBytes = Recv_all_nodes[fidPtctDL] * 128 + Recv_all_nodes[fidMonDL] * 256 + Recv_all_nodes[fidCtrlDL] * 128 +
	//			Recv_all_nodes[fidWASADL] * 256;
	//DLThroughput = totalDLRcvdBytes/(double)100.0; //duration;

	cout << "UL recv/sent = " << totalUlRcvd << "/" << totalUlSent << "=" << (double)totalUlRcvd/totalUlSent << endl;
	cout << "DL recv/sent = " << totalDlRcvd << "/" << totalDlSent << "=" << (double)totalDlRcvd/totalDlSent << endl;

/*	cout << "ULThroughput = " << ULThroughput << endl << "DLThroughput = " << DLThroughput << endl
			<< "TotalThroughput = " << ULThroughput +  DLThroughput << endl ;
*/
	//Use tpt calculated from the FlowMon Class:
	cout << "ULThroughput = " << tptUl/1024/1024 << endl << "DLThroughput = " << tptDl/1024/1024 << endl
				<< "TotalThroughput = " << (tptUl+tptDl)/1024/1024 << "(Mbps)" << endl ;
	//int totalPhyDlDropped = 0;
	Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(bs->GetPhy());
	NS_LOG_DEBUG("Total Phy DL Drops (class)= " << totalPhyDlDropped << endl
			      << "Total Phy UL Drops (class)= " << myPhy->m_drop << endl
			      << "Drops using Trace:");

	PrintTotalDropCounts();

	Simulator::Destroy();


	NS_LOG_UNCOND ("Done.");

	return 0;
}
