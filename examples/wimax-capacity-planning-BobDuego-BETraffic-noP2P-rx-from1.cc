/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */

//
// Default network topology includes a base station (BS) and 2
// subscriber station (SS).

//      +-----+
//      | SS0 |
//      +-----+
//     10.1.1.1
//      -------
//        ((*))
//
//                  10.1.1.7
//               +------------+
//               |Base Station| ==((*))
//               +------------+
//
//        ((*))
//       -------
//      10.1.1.2
//       +-----+
//       | SS1 |
//       +-----+

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>
#include "ns3/CSV_Reading.h"
#include <string>     // std::string, std::to_string

#define MAX_APPS 50
NS_LOG_COMPONENT_DEFINE ("WimaxCapacityExample");

using namespace std;
using namespace ns3;

//TCP Traffic
//Uplink
int setup_tcp(int nbSS, NodeContainer ssNodes, Ipv4InterfaceContainer SSinterfaces,
		                 NodeContainer bsNode, Ipv4InterfaceContainer BSinterface,
		                 double duration, std::string on_time, std::string off_time,
		                 uint32_t fid, int startPort)
{
	  PacketSinkHelper *tcpServer[MAX_APPS];// = new PacketSinkHelper[nbSS];
	  ApplicationContainer* sinkApps = new ApplicationContainer[nbSS];
	  OnOffHelper *onoff[MAX_APPS]; //("ns3:TcpSocketFactory", Ipv4Address::GetAny()) = new OnOffHelper[nbSS];
	  ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	  int port;
	  for (int i = 0; i < nbSS; i++)
	    {
		  port = startPort + (i * 10);
	      // set server port to 100+(i*10)
		  Address localAddress (InetSocketAddress(Ipv4Address::GetAny(),port));
	      tcpServer[i] = new PacketSinkHelper ("ns3::TcpSocketFactory",localAddress);
	      sinkApps[i] = tcpServer[i]->Install (bsNode.Get(0));
	      sinkApps[i].Start (Seconds (6));
	      sinkApps[i].Stop (Seconds (duration));

	      onoff[i] = new OnOffHelper ("ns3::TcpSocketFactory", Ipv4Address::GetAny());
	      AddressValue remoteAddr(InetSocketAddress(BSinterface.GetAddress(0),port));
	      onoff[i]->SetAttribute("Remote", remoteAddr);
	      onoff[i]->SetAttribute("OnTime", StringValue (on_time));
	      onoff[i]->SetAttribute("OffTime",  StringValue (off_time));
	      onoff[i]->SetAttribute("FlowId", UintegerValue (fid));
	      onoff[i]->SetAttribute("DataRate",DataRateValue (DataRate ("10kb/s")));
	  	  onoff[i]->SetAttribute ("PacketSize", UintegerValue (128));//pktSize
	  	  //clientHelper.SetConstantRate(DataRate (data_rate));
	  	  //clientHelper.SetAttribute("MaxBytes", UintegerValue (maxBytes));

//	      udpClient[i].SetAttribute ("Interval", TimeValue (Seconds (1)));
//	      udpClient[i].SetAttribute ("PacketSize", UintegerValue (70));

	      clientApps[i] = onoff[i]->Install (ssNodes.Get (i));
	      clientApps[i].Start (Seconds (6));
	      clientApps[i].Stop (Seconds (duration));
	    }
	  return port;
}


int main (int argc, char *argv[])
{
  int  schedType = 0, simulation_time = 10, dlIntegStTime = 2.0, duration = 10;

  //LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
  //LogComponentEnable ("BulkSendApplication", LOG_LEVEL_ALL); //TCP
  LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink
  //LogComponentEnable ("Cost231PropagationLossModel", LOG_LEVEL_ALL); //TCP Sink
  //LogComponentEnable ("Ipv4AddressHelper",LOG_LEVEL_ALL);
  //LogComponentEnable ("simpleOfdmWimaxChannel",LOG_LEVEL_ALL);
  //LogComponentEnable ("WimaxConnection",LOG_LEVEL_ALL);
  //LogComponentEnable ("SubscriberStationNetDevice",LOG_LEVEL_ALL);
  //LogComponentEnable ("BaseStationNetDevice",LOG_LEVEL_ALL);
  //LogComponentEnable ("SSLinkManager",LOG_LEVEL_ALL);
  WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
  WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;

  vector<Node_Data> BS_Info;
  vector<Node_Data> CPEs_Info;

  Xml_Read_Freq_Compatible("20140519PTMP LakeshoreWPCP.xml", BS_Info, CPEs_Info);
  NS_LOG_UNCOND("BS_Info.size()=" << BS_Info.size()<< "," << "CPEs_Info.size()=" << CPEs_Info.size());
 // NS_ASSERT(false);
  for (unsigned int i=0; i < CPEs_Info.size(); i++)
  {
	  NS_LOG_UNCOND( i<< "Name:" << CPEs_Info.at(i).location << "x,y,z" << CPEs_Info.at(i).latitude << "," << CPEs_Info.at(i).longtitude << "," << CPEs_Info.at(i).TxAntennaHeight);
  }

  CommandLine cmd;
  cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
  cmd.Parse (argc, argv);

  NS_LOG_UNCOND("Start...");
  switch (schedType)
    {
    case 0:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
      break;
    case 1:
      scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
      break;
    case 2:
      scheduler = WimaxHelper::SCHED_TYPE_RTPS;
      break;
    default:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
    }

  NodeContainer ssNodesAlcona[3];
  vector<Node_Data> CPEs_Alcona[3];
  WimaxHelper wimaxAlcona[3];
  InternetStackHelper stack;
  Ipv4AddressHelper address;
  NetDeviceContainer ssDevsAlcona[3];

  Ipv4InterfaceContainer SSinterface_Alcona[3];

  //NetDeviceContainer allNetDevs;


  address.SetBase ("10.1.1.0", "255.255.255.0");
  NS_LOG_UNCOND("Sorting CPEs based on their sectNum...");
  for (unsigned int i=0; i<CPEs_Info.size(); i++)
  {
	  CPEs_Alcona[CPEs_Info.at(i).sectNum-1].push_back(CPEs_Info.at(i));
  }

  //int end_nums[4];
  //end_nums[0]=0;

  //MobilityHelper mobility;

  NS_LOG_UNCOND("Assigning CPEs");
  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  unsigned int num = CPEs_Alcona[sectNum].size();
	  ssNodesAlcona[sectNum].Create (num);
//	  cout << "end_nums[" << sectNum+1 << "]=" << end_nums[sectNum+1] <<endl;
	  vector <double> ssAntennaHeightVector;
	  vector <string> ssNodeNameVector;
	  vector <double> ssTxFreqVector;
	  Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject <ListPositionAllocator>();
	 for (unsigned int j=0;j< num; j++)
	  {
	  	  ssPositionAlloc ->Add(Vector(CPEs_Alcona[sectNum].at(j).latitude,
	  			  CPEs_Alcona[sectNum].at(j).longtitude,
	  			  (CPEs_Alcona[sectNum].at(j).elevation)/1000
	  	  	  	  ));
	  	  ssAntennaHeightVector.push_back(CPEs_Alcona[sectNum].at(j).TxAntennaHeight);
	  	  ssNodeNameVector.push_back(CPEs_Alcona[sectNum].at(j).location);
	  	  ssTxFreqVector.push_back(CPEs_Alcona[sectNum].at(j).TxFreq);
	  }

	  MobilityHelper ssMobility;
	  ssMobility.SetPositionAllocator(ssPositionAlloc);
	  ssMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  ssMobility.m_antennaHeightVector = ssAntennaHeightVector;
	  ssMobility.m_nodeNameVector = ssNodeNameVector;
	  ssMobility.m_txFreqVector = ssTxFreqVector;
	  ssMobility.Install(ssNodesAlcona[sectNum]);


	 // mobility.Install (ssNodesAlcona[sectNum]);

	  //print
	  /*for (unsigned int k=0;k<num;k++)
	  {
		  Ptr<MobilityModel> position = ssNodesAlcona[sectNum].Get(k)->GetObject<MobilityModel> ();
		  NS_ASSERT (position != 0);
		  Vector pos = position->GetPosition ();
		  NS_LOG_UNCOND( "CPE#" << k << " at location " << CPEs_Alcona[sectNum].at(k).location << " TX Freq " << CPEs_Alcona[sectNum].at(k).TxFreq << " at Sector#" << CPEs_Alcona[sectNum].at(k).sectNum << ":, x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << ", ant=" << position->m_antennaHeight );
	  } //for k*/

	  //Building SS devices
	  //channel = CreateObject<SimpleOfdmWimaxChannel> ();
	  //channel->SetPropagationModel (SimpleOfdmWimaxChannel::COST231_PROPAGATION);
	  ssDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install(	ssNodesAlcona[sectNum],
			  	  	  	  	  	  	  	  	  	  	  	  	WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
			  	  	  	  	  	  	  	  	  	  	  	    WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
			  	  	  	  	  	  	  	  	  	  	  	    scheduler, &CPEs_Alcona[sectNum], true);
	  //allNetDevs.Add(ssDevsAlcona[sectNum]);
	  /*ObjectFactory queueFactory;
	  vector<Ptr<Queue> > queue = queueFactory.Create<Queue> ();
	  allNetDevs.Get(0)->SetQueue (queue);*/

	 // end_nums[sectNum+1] = allNetDevs.GetN();

	  //set modulation and activate loss
	  for (unsigned int j=0;j< num; j++)
	  {
		  ssDevsAlcona[sectNum].Get(j)->GetObject<SubscriberStationNetDevice> ()->SetModulationType (modulation);
		  Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(ssDevsAlcona[sectNum].Get(0)->GetObject<SubscriberStationNetDevice>()->GetPhy());
		//  ssPhy->ActivateLoss(true);
	  }
	  //install IP stack on all ss nodes in this sector.
	  stack.Install (ssNodesAlcona[sectNum]);
	  //address assignment for ss nodes: (added Jul. 25, 2014)
	  SSinterface_Alcona[sectNum] = address.Assign (ssDevsAlcona[sectNum]);
  } // for sectNum

  /*
  std::vector<NodeContainer> AdjacencyList[3];
  for (int sectNum =0; sectNum < 3; sectNum++)
	  AdjacencyList[sectNum] = std::vector<NodeContainer> (ssNodesAlcona[sectNum].GetN());
  */
  NS_LOG_UNCOND("Done assigning SSNodesAlcona, starting BS assignments...");

  vector<Node_Data> BS_Alcona[3];
  Ipv4InterfaceContainer BSinterface_Alcona[3];
  for (unsigned int i=0; i<3; i++)
  {
	  NS_LOG_UNCOND("i="<<i);
	  BS_Alcona[i].push_back(BS_Info[i]);
  }
  NodeContainer bsNodesAlcona[3];
  NetDeviceContainer bsDevsAlcona[3];

  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  NS_LOG_UNCOND("sectNum="<<sectNum);
	  bsNodesAlcona[sectNum].Create (1);
	  vector <double> bsAntennaHeightVector;
	  vector <string> bsNodeNameVector;
	  vector <double> bsTxFreqVector;
	  Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject <ListPositionAllocator>();
	  bsPositionAlloc ->Add(Vector(BS_Info.at(sectNum).latitude, BS_Info.at(sectNum).longtitude, (BS_Info.at(sectNum).elevation)/1000));
	  NS_LOG_UNCOND("here 1");
	  bsAntennaHeightVector.push_back(BS_Info.at(sectNum).TxAntennaHeight);
	  bsNodeNameVector.push_back(BS_Info.at(sectNum).location);
	  bsTxFreqVector.push_back(BS_Info.at(sectNum).TxFreq);
	  MobilityHelper bsMobility;
	  bsMobility.SetPositionAllocator(bsPositionAlloc);
	  bsMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  bsMobility.m_antennaHeightVector = bsAntennaHeightVector;
	  bsMobility.m_nodeNameVector = bsNodeNameVector;
	  bsMobility.m_txFreqVector = bsTxFreqVector;
	  bsMobility.Install(bsNodesAlcona[sectNum]);
	 // mobility.Install (bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("here 2");
	  bsDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install (bsNodesAlcona[sectNum].Get(0), WimaxHelper::DEVICE_TYPE_BASE_STATION,
	 		  	  	  	  	  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
	 		  	  	  	  	  scheduler, &BS_Alcona[sectNum], true);

	  //allNetDevs.Add(bsDevsAlcona[sectNum]);

	  NS_LOG_UNCOND("here 3");
	  /*bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->
			  SetDuplex(BS_Alcona[sectNum].at(0).TxFreq,BS_Alcona[sectNum].at(0).RxFreq);
	  bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->SetChannelBandwidth(10e6);*/

	  Ptr<SimpleOfdmWimaxPhy> bsPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy());
	 // bsPhy->ActivateLoss(true);

	  NS_LOG_UNCOND("Assigning IP to bs#"<<sectNum);
	  stack.Install (bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("Building the BS Interface");
	  BSinterface_Alcona[sectNum] = address.Assign (bsDevsAlcona[sectNum]);
  } // for base station Alcona sectNum


  NS_LOG_UNCOND("Done assigning BSNodesAlcona.");

  //### Uplink Traffic-TCP--integrity poll response
  //###
  duration = simulation_time;
  std::string on_time = "ns3::ConstantRandomVariable[Constant=30.0]";
  std::string off_time = "ns3::ConstantRandomVariable[Constant=0.0]";
  uint32_t fid = 0;
  //ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_UGS;

  double unit_data_rate_per_rtu = 0.129; // kb/s
  std::string data_rate; // = "0.129kb/s";// Data rate here needs to be updated according to the number of devices attached to one device
  //uint32_t pktSize = 242; //256;  //242 is for integrity polling, 256 is fro CPE traffic
  double integStTime = dlIntegStTime + 0.008 - CPEs_Info.size()*0.033;
  //uint32_t maxBytes = 0;
  //std::map <const char*,ServiceFlow::SchedulingType> serviceProfile = CSV_Reading ("20140625 XML_SCADA_MatchSiteNames3Col.csv",CPEs_Info);
  std::map <const char*,double> numRTUs;
 /* std::map <const char*,ServiceFlow::SchedulingType> serviceProfile =
		  CSV_Reading("20140625 XML_SCADA_MatchSiteNamesNumRTUs.csv",CPEs_Info,numRTUs);
  for (uint j=0; j<CPEs_Info.size(); j++)
  {
	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "has service flow of type " << serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()]
	       << ", and owns " <<numRTUs[(const char*)CPEs_Info.at(j).location.c_str()] << " RTUs!" );
	  serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_RTPS;
  }*/


  for (int sectNum =0; sectNum < 1; sectNum++)//3
  {
		integStTime = integStTime + 0.033;
		int nbSS = ssNodesAlcona[sectNum].GetN();
		stringstream s;
		s << unit_data_rate_per_rtu /* numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] */<< "kb/s";
		data_rate = s.str();
	  int startPort = 100;
	  setup_tcp(nbSS, ssNodesAlcona[sectNum], SSinterface_Alcona[sectNum],
								bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum],
								duration, on_time, off_time,
								fid, startPort);
	  for (int i = 0; i < nbSS; i++)
    {
			/*  IpcsClassifierRecord DlClassifierBe (Ipv4Address ("0.0.0.0"),
																						 Ipv4Mask ("0.0.0.0"),
																						 SSinterfaces.GetAddress (i),
																						 Ipv4Mask ("255.255.255.255"),
																						 0,
																						 65000,
																						 startPort + (i * 10),
																						 startPort + (i * 10),
																						 17,
																						 1);
				ServiceFlow DlServiceFlowBe = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN,
																															 ServiceFlow::SF_TYPE_BE,
																															 DlClassifierBe);
				ss[i]->AddServiceFlow (DlServiceFlowBe);*/
				IpcsClassifierRecord ulClassifierBe (SSinterface_Alcona[sectNum].GetAddress (i),
																						 Ipv4Mask ("255.255.255.255"),
																						 Ipv4Address ("0.0.0.0"),
																						 Ipv4Mask ("0.0.0.0"),
																						 0,
																						 65000,
																						 startPort + (i * 10),
																						 startPort + (i * 10),
																						 6,
																						 1);
				ServiceFlow ulServiceFlowBe = wimaxAlcona[sectNum].CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP,
																															 ServiceFlow::SF_TYPE_BE,
																															 ulClassifierBe);
				ssDevsAlcona[sectNum].Get(i)->GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (ulServiceFlowBe);


    }
  }

  cout << "number of nodes in sector1=" << ssNodesAlcona[1].GetN() << endl;


  NS_LOG_UNCOND (":-)");// << stTime);
  NS_LOG_UNCOND ("Starting simulation.....");
  //NS_ASSERT(false);
  Simulator::Stop(Seconds(50));
  Simulator::Run ();


  //bs = 0;
  NS_LOG_INFO ("Done.");

  //Print Statistics:
  int reliable_received_all_nodes[FLOW_NUM];
  //ns3::Time latency_all_nodes[FLOW_NUM];
  Time Delays_all_nodes[FLOW_NUM];
  Time ReliableDelays_all_nodes[FLOW_NUM];
  int Recv_all_nodes[FLOW_NUM];
  //int sent_all_nodes[FLOW_NUM];  m_Sent
  for (int flowid=0; flowid<FLOW_NUM; flowid++)
  {
	  reliable_received_all_nodes[flowid] 	= 0;
	  Delays_all_nodes[flowid] 				= Time(0.0);
	  ReliableDelays_all_nodes[flowid] 		= Time(0.0);
	  Recv_all_nodes[flowid] 				= 0;
  }

  //for 0<=flowid<5 (UL) we need to sum over bsNodes:
  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  Ptr<Node> myNode = bsNodesAlcona[sectNum].Get(0);
	  for (int flowid=0; flowid<5; flowid++)
	  {
		  //NS_LOG_UNCOND(sectNum << 	":" << flowid  );
		  reliable_received_all_nodes[flowid] 	+= myNode->reliable_received[flowid];
		  //NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
		  Delays_all_nodes[flowid] 				+= myNode->Delays[flowid];
		  //NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
		  ReliableDelays_all_nodes[flowid] 		+= myNode->ReliableDelays[flowid];
		  //NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
		  Recv_all_nodes[flowid] 				+= myNode->Recv[flowid];
		  //NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
	  }
  }

  //for flowid=5 (DL) we need to sum over all ssNodes:
  int flowid=5;
  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < ssNodesAlcona[sectNum].GetN();cpe_num++)
	  {
		  Ptr<Node> myNode = ssNodesAlcona[sectNum].Get(cpe_num);
		  //NS_LOG_UNCOND(sectNum << ":" << cpe_num << ":" << flowid  );
		  reliable_received_all_nodes[flowid] 	+= myNode->reliable_received[flowid];
		  //NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
		  Delays_all_nodes[flowid] 				+= myNode->Delays[flowid];
		  //NS_LOG_UNCOND("delays: " << myNode->Delays[flowid].ToDouble(Time::MS) << "," << Delays_all_nodes[flowid]) ;
		  ReliableDelays_all_nodes[flowid] 		+= myNode->ReliableDelays[flowid];
		  //NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid].ToDouble(Time::MS) << "," << ReliableDelays_all_nodes[flowid] );
		  Recv_all_nodes[flowid] 				+= myNode->Recv[flowid];
		  //NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
	 }
  }

  cout << "flow ID\t\treliable rcvd\treliable delay\tall rcvd\tall delay" << endl;
  //cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
  for (int flowid=0; flowid<6; flowid++)
  {
	  cout << flowid << "\t\t" << reliable_received_all_nodes[flowid] << "\t\t" <<
			  ReliableDelays_all_nodes[flowid].ToDouble(Time::MS) / (double) reliable_received_all_nodes[flowid] << "\t\t" <<
			  Recv_all_nodes[flowid] << "\t\t" <<
	  	  	  Delays_all_nodes[flowid].ToDouble(Time::MS) / (double) Recv_all_nodes[flowid] <<  endl;
  }

  int Throughput[3];

    for (int sectNum =0; sectNum < 3; sectNum++)
    {
    	Throughput[sectNum] = 0;
    	for (int flowid=0; flowid<5 ; flowid++)
    	{
    		Throughput[sectNum] += bsNodesAlcona[sectNum].Get(0)->receivedBytes[flowid];
    	}
	  for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
	  {
		  Throughput[sectNum] += ssNodesAlcona[sectNum].Get(i)->receivedBytes[5];
	  }
	  cout <<	 "Throughput" << sectNum << " = " <<   Throughput[sectNum]/130.0 << endl;
    }

    int SentSect[3], RecvSect[3];

      for (int sectNum =0; sectNum < 3; sectNum++)
      {
		SentSect[sectNum] = 0;
		RecvSect[sectNum] = 0;
		//UL Sent
		for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
		{
			for (int flowid=0; flowid<5 ; flowid++)
			{
				SentSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->m_Sent[flowid];
			}
		}
		//UL recv
		for (int flowid=0; flowid<5 ; flowid++)
		{
			RecvSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->Recv[flowid];
		}

		//DL sent
		SentSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->m_Sent[5];
		//DL recv
		for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
		{
		  	RecvSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->Recv[5];
		}
		cout <<	 "SentNum" << sectNum << " = " <<  SentSect[sectNum]  << "\t RecvNum" << RecvSect[sectNum]  << endl;
		cout << "Loss Ratio" << RecvSect[sectNum]/float(SentSect[sectNum]) << endl;
      }














  Simulator::Destroy ();

  return 0;
}
