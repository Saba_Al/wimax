/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */

//
// Default network topology includes a base station (BS) and 2
// subscriber station (SS).

//      +-----+
//      | SS0 |
//      +-----+
//     10.1.1.1
//      -------
//        ((*))
//
//                  10.1.1.7
//               +------------+
//               |Base Station| ==((*))
//               +------------+
//
//        ((*))
//       -------
//      10.1.1.2
//       +-----+
//       | SS1 |
//       +-----+

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/point-to-point-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "assert.h"
#include "ns3/service-flow.h"
#include <iostream>
#include <sstream>

#include "../model/bandwidth-manager.h"

#define MAX_USERS 3005
NS_LOG_COMPONENT_DEFINE ("WimaxSimpleExample");


using namespace ns3;

//uplink udp:
//UdpServerHelper udpServerUL[MAX_USERS];
ApplicationContainer serverAppsUL[MAX_USERS];
//UdpClientHelper udpClientUL[MAX_USERS];
//ApplicationContainer clientAppsUL[MAX_USERS];
IpcsClassifierRecord UlClassifier[MAX_USERS];
ServiceFlow UlServiceFlow[MAX_USERS];

//downlink udp:
//UdpServerHelper udpServerDL[MAX_USERS];
ApplicationContainer serverAppsDL[MAX_USERS];
UdpClientHelper udpClientDL[MAX_USERS];
ApplicationContainer clientAppsDL[MAX_USERS];
IpcsClassifierRecord DlClassifier[MAX_USERS];
ServiceFlow DlServiceFlow[MAX_USERS];

//keep track of appnum per type
int appnumAfterWASADL=-1;
int appnumAfterWASAUL=-1;
int appnumAfterMonDL=-1;
int appnumAfterMonUL=-1;
int appnumAfterCtrlDL=-1;
int appnumAfterCtrlUL=-1;
int appnumAfterPtctDL=-1;
int appnumAfterPtctUL=-1;
int appnumAfterSMDL=-1;
int appnumAfterSMUL=-1;
int appnumBeforeWASADL=-1;
int appnumBeforeWASAUL=-1;
int appnumBeforeMonDL=-1;
int appnumBeforeMonUL=-1;
int appnumBeforeCtrlDL=-1;
int appnumBeforeCtrlUL=-1;
int appnumBeforePtctDL=-1;
int appnumBeforePtctUL=-1;
int appnumBeforeSMDL=-1;
int appnumBeforeSMUL=-1;

int setup_udp_onoff(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size, std::string data_rate,
		int startPort, WimaxHelper wimaxAlcona, WimaxPhy::ModulationType modulation,
		ServiceFlow::SchedulingType sfType, int req_latency=0) {
		//,PacketSinkHelper* udpServer

	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
/*	double startTime = 2;
	double stopTime = 7;*/
	for (int i = 0; i < nbSS; i++) {
		cout << "nbSS" << nbSS << endl;
		cout << "setup_udp_onoff,i="<< i << endl;
		port = startPort + ((i+1) * 10);
		// set server port to 100+(i*10)
		Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory", sinkLocalAddress);;
		serverApps[i] = udpServer[i]->Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(110));//duration


		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory",Address());
		AddressValue remoteAddress (InetSocketAddress (BSinterface.GetAddress(0), port));
		udpClient[i]->SetAttribute ("Remote", remoteAddress);
		//udpClient[i]->SetAttribute("MaxPackets", UintegerValue(1200));
		//udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		cout << "bb" << endl;
		udpClient[i]->SetConstantRate (DataRate (data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId",UintegerValue(fid));
		udpClient[i]->SetAttribute	("OnTime", StringValue (on_time));
		udpClient[i]->SetAttribute	("OffTime", StringValue (off_time));

		cout << "cc" << endl;
		cout << "size"  << ssNodes.GetN() << endl;
		clientApps[i] = udpClient[i]->Install(ssNodes.Get(i));

		clientApps[i].Start(Seconds(2));
		clientApps[i].Stop(Seconds(100));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	int priority = 1;
	if (fid == 1)
		priority = 2;
	else if (fid == 3)
		priority = 1;
	for (int i = 0; i < nbSS; i++) {
		cout << "setup_udp_onoff,iiii="<< i << endl;
				IpcsClassifierRecord ulClassifierBe(
				SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.0.0"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + ((i+1) * 10),
				startPort + ((i+1)* 10), 17, priority);
				cout << "dd" << endl;
		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, sfType,
				ulClassifierBe);
		cout << "ee" << endl;
		ulServiceFlowBe.SetModulation(modulation);
	//	ulServiceFlowBe.SetMaximumLatency(req_latency);
/*		if (sfType == ServiceFlow::SF_TYPE_UGS && fid == 1)
		{
			ulServiceFlowBe.SetMinReservedTrafficRate(std::atoi(data_rate.c_str()));
		}*/
		ss[i]->AddServiceFlow(ulServiceFlowBe);
	}
	return port;
}

void NodeConfigurationCommonSetupUDP(WimaxHelper &wimax, int numUsers, int &created_nodes, int areaRadius, InternetStackHelper &stack, Ipv4AddressHelper &address, WimaxHelper::SchedulerType scheduler, WimaxPhy::ModulationType modulation,/*inputs*/
		NodeContainer &ssNodes, Ptr<SubscriberStationNetDevice> *ss,
		Ipv4InterfaceContainer &SSinterfaces /*outputs*/)
{
	/*------------------------------*/
	//For each application, we need to create common variables here (common part between UL and DL of each application):
	//ssNodes = new NodeContainer;
	NS_LOG_UNCOND ("Assigning IP Adresss for UDP nodes " << created_nodes << " to " << created_nodes+numUsers-1 << "...");

	ssNodes.Create (numUsers);
	NetDeviceContainer ssDevs;
	ssDevs = wimax.Install (ssNodes,
						  WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
						  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
						  scheduler);
//	for (int i=0;i<ssDevs.GetN();i++)
//		ssDevs.Get(i)->GetObject<SubscriberStationNetDevice>()->GetBa
	stack.Install (ssNodes);
	SSinterfaces = address.Assign (ssDevs);

	//node positions
	MobilityHelper mobility;
	std::ostringstream convert;
	convert << areaRadius;
	mobility.SetPositionAllocator ("ns3::UniformDiscPositionAllocator",
				 "X", StringValue ("0.0"),
				 "Y", StringValue ("0.0"),
				 "rho", StringValue (convert.str())); //rho = 5000, My_Ro
	/*vector<double> ssAntennaHeightVector;
	vector<double> ssTxFreqVector;
	vector<string> ssNodeNameVector;
	for (int j = 0; j < numUsers; j++) {
				ssAntennaHeightVector.push_back(9);
				ssTxFreqVector.push_back(1827);
				ostringstream name;
				name << "Node_" << j;
				ssNodeNameVector.push_back(name.str());
	}
	mobility.m_antennaHeightVector = ssAntennaHeightVector;
	mobility.m_txFreqVector = ssTxFreqVector;*/
	//mobility.Install(ssNodes);
	//mobility.m_antennaHeightVector = ssAntennaHeightVector;
	//mobility.m_nodeNameVector = ssNodeNameVector;
	//mobility.m_txFreqVector = ssTxFreqVector;

	//mobility.m_txFreqVector = 1812;
	for (int i = 0; i < numUsers; i++)
	{
		ss[i] = ssDevs.Get (i)->GetObject<SubscriberStationNetDevice> ();
		ss[i]->SetModulationType (modulation);
	}
	created_nodes = created_nodes + numUsers;
	//NS_LOG_UNCOND("SSinterfaces 0:" << SSinterfaces.GetAddress(0));
}

int Uplink_UDP_Traffic(WimaxHelper &wimax, int port,int numUsers,int duration,
		Ipv4InterfaceContainer BSinterface, Ipv4InterfaceContainer SSinterfaces,
		NodeContainer bsNodes, NodeContainer ssNodes , Ptr<SubscriberStationNetDevice> ss[],
		double interval, int fid, uint32_t pktSize, UdpClientHelper* udpClient,
		UdpServerHelper* udpServer, ApplicationContainer* clientApps, ServiceFlow::SchedulingType sfType)
{
	int port1 = port;
	for (int i = 0; i < numUsers; i++)
	{
		  port = port+10;
		  udpServer[i] = UdpServerHelper (port);//udpServerUL
		  serverAppsUL[i] = udpServer[i].Install (bsNodes.Get (0));
		  serverAppsUL[i].Start (Seconds (0));
		  serverAppsUL[i].Stop (Seconds (duration));
		  udpClient[i] = UdpClientHelper (BSinterface.GetAddress (0), port);
		  udpClient[i].SetAttribute ("MaxPackets", UintegerValue (1200));
		  udpClient[i].SetAttribute ("Interval", TimeValue (Seconds (interval)));//0.01
		  udpClient[i].SetAttribute("FlowId", UintegerValue(fid));
		  udpClient[i].SetAttribute ("PacketSize", UintegerValue (pktSize));
		  clientApps[i] = udpClient[i].Install (ssNodes.Get(i));
		  clientApps[i].Start (Seconds (1.5));
		  clientApps[i].Stop (Seconds (duration));
	}

	//classifier
	for (int i = 0; i < numUsers; i++)
	{
		  UlClassifier[i] = IpcsClassifierRecord (SSinterfaces.GetAddress (i),
												Ipv4Mask ("255.255.0.0"),
												Ipv4Address ("0.0.0.0"),
												Ipv4Mask ("0.0.0.0"),
												0,
												65000,
												port1+10*(i+1),
												port1+10*(i+1),
												17,
												1);
		  UlServiceFlow[i] = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP,
		  		sfType/*ServiceFlow::SF_TYPE_RTPS*/,
																  UlClassifier[i]);
		  uint32_t data_rate = pktSize/interval;
		  UlServiceFlow[i].SetMinReservedTrafficRate(data_rate);
		  ss[i]->AddServiceFlow (UlServiceFlow[i]);
	}

	return port;
}

int Downlink_UDP_Traffic(WimaxHelper &wimax, int port,int numUsers,int duration, Ipv4InterfaceContainer SSinterfaces,
		NodeContainer bsNodes, NodeContainer ssNodes , Ptr<SubscriberStationNetDevice> ss[], int fid, uint32_t pktSize,
		double interval, UdpServerHelper *udpServerDL, ServiceFlow::SchedulingType sfType, int req_latency=0)
{
	int port1 = port;
	for (int i = 0; i < numUsers; i++)
	{
		port=port+10;
		//create applications whose servers are on ssNodes
		udpServerDL[i] = UdpServerHelper (port);
		serverAppsDL[i] = udpServerDL[i].Install(ssNodes.Get(i));
		serverAppsDL[i].Start (Seconds (0.0));
		serverAppsDL[i].Stop (Seconds (10));
		//create clients on BS for each application
		udpClientDL[i] = UdpClientHelper (SSinterfaces.GetAddress (i), port);
		udpClientDL[i].SetAttribute ("MaxPackets", UintegerValue (120000));
		udpClientDL[i].SetAttribute ("Interval", TimeValue (Seconds (interval)));//0.01
		udpClientDL[i].SetAttribute("FlowId", UintegerValue(fid));
		udpClientDL[i].SetAttribute ("PacketSize", UintegerValue (pktSize));
		clientAppsDL[i] = udpClientDL[i].Install (bsNodes.Get(0));
		Ptr<UniformRandomVariable> uv = CreateObject<UniformRandomVariable> ();
		//NS_LOG_UNCOND("rrrrrrrrrrrrrrrrrr:" << uv->GetValue());
		clientAppsDL[i].Start (Seconds (1.5));//+3*uv->GetValue()
		clientAppsDL[i].Stop (Seconds (4.5));
	}

	//classifier
	for (int i = 0; i < numUsers; i++)
	{
		DlClassifier[i] = IpcsClassifierRecord  (Ipv4Address ("0.0.0.0"),
										  Ipv4Mask ("0.0.0.0"),
										  SSinterfaces.GetAddress (i),
										  Ipv4Mask ("255.255.0.0"),
										  0,
										  65000,
										  port1+10*(i+1),
										  port1+10*(i+1),
										  17,
										  1);
		DlServiceFlow[i] = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN,
															sfType/*ServiceFlow::SF_TYPE_UGS*/,
															DlClassifier[i]);
	//	uint32_t data_rate = pktSize/interval;
//		if (sfType == ServiceFlow::SF_TYPE_UGS)
//				{
//					DlServiceFlow[i].SetMinReservedTrafficRate(data_rate);
//					DlServiceFlow[i].SetMaximumLatency(req_latency);
//				}
		ss[i]->AddServiceFlow (DlServiceFlow[i]);
	}
	return port;
}

int Downlink_onoff_Traffic(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[],
		NodeContainer bsNode,	Ipv4InterfaceContainer BSinterface, double duration,
		std::string on_time, std::string off_time, int pkt_size, std::string data_rate,
		int startPort, WimaxHelper wimax, WimaxPhy::ModulationType modulation,
		ServiceFlow::SchedulingType sfType, int req_latency=0)
{
	//int port1 = port;
	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
/*	double startTime = 2;
	double stopTime = 7;*/
	for (int i = 0; i < nbSS; i++) {
		cout << "nbSS" << nbSS << endl;
		cout << "setup_udp_onoff,i="<< i << endl;
		port = startPort + ((i+1) * 10);
		// set server port to 100+(i*10)
		Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory", sinkLocalAddress);;
		serverApps[i] = udpServer[i]->Install(ssNodes.Get(i));
		serverApps[i].Start(Seconds(4.8));
		serverApps[i].Stop(Seconds(110));//duration


		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory",Address());
	  AddressValue remoteAddress (InetSocketAddress (SSinterfaces.GetAddress (i), port));
		udpClient[i]->SetAttribute ("Remote", remoteAddress);
		//udpClient[i]->SetAttribute("MaxPackets", UintegerValue(1200));
		//udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		cout << "bb" << endl;
		udpClient[i]->SetConstantRate (DataRate (data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId",UintegerValue(fid));
		udpClient[i]->SetAttribute	("OnTime", StringValue (on_time));
		udpClient[i]->SetAttribute	("OffTime", StringValue (off_time));

		cout << "cc" << endl;
		cout << "size"  << ssNodes.GetN() << endl;
		clientApps[i] = udpClient[i]->Install(bsNode.Get(0));

		clientApps[i].Start(Seconds(5.0));
		clientApps[i].Stop(Seconds(100));
	}

	//classifier
	for (int i = 0; i < nbSS; i++)
	{
		DlClassifier[i] = IpcsClassifierRecord  (Ipv4Address ("0.0.0.0"),
										  Ipv4Mask ("0.0.0.0"),
										  SSinterfaces.GetAddress (i),
										  Ipv4Mask ("255.255.0.0"),
										  0,
										  65000,
										  startPort+10*(i+1),
										  startPort+10*(i+1),
										  17,
										  1);
		DlServiceFlow[i] = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN,
															sfType/*ServiceFlow::SF_TYPE_RTPS*/,
															DlClassifier[i]);
//		DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str()));
//		if (sfType == ServiceFlow::SF_TYPE_UGS)
//						{
//							DlServiceFlow[i].SetMinReservedTrafficRate(std::atoi(data_rate.c_str()));
//							DlServiceFlow[i].SetMaximumLatency(req_latency);
//						}
		//m_minReservedTrafficRate = (data_rate);

		ss[i]->AddServiceFlow (DlServiceFlow[i]);
	}
	return port;
} //Downlink_onoff_Traffic



int main (int argc, char *argv[])
{
	bool verbose = false, enableUplink = false, enableDownlink=false,
		enableRural = false, enableSuburban = false, enableUrban = false;
	int duration =5, schedType = 0, port=100, numAggregators=0;
	int numWASAUsers=0,numPtctUsers=0,numMonUsers=0,numCtrlUsers=0,numSMUsers=0;
	WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM64_23;//  64_23; ;
	double compressionRatio=1.0;
	double areaRadius = 0.56434; //0.56434; //kilometer
	int fidWASAUL= 0, fidMonUL = 1,  fidCtrlUL = 2, fidPtctUL = 3, fidSMUL = 4, fidAgg = 4;
	int START_DL_FID = 5;
	int fidWASADL= 5, fidMonDL = 6,  fidCtrlDL = 7, fidPtctDL = 8;
	int END_DL_FID = 9;
//int monDLInterval = 0.2; //with packet size 256//0.1 with packetsize 128,

	WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;

	CommandLine cmd;
	cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
	cmd.AddValue ("duration", "duration of the simulation in seconds", duration);

	cmd.AddValue ("verbose", "turn on all WimaxNetDevice log components", verbose);
	cmd.AddValue ("NW", "Number of nodes which have WASA data.", numWASAUsers);
	cmd.AddValue ("NP", "Number of nodes which have PTCT data.", numPtctUsers);
	cmd.AddValue ("NM", "Number of nodes which have MONT data.", numMonUsers);
	cmd.AddValue ("NC", "Number of nodes which have CTRL data.", numCtrlUsers);
	cmd.AddValue ("NS", "Number of nodes which have SMRT data.", numSMUsers);
	cmd.AddValue ("DL", "Enable DL transmission?",enableDownlink);
	cmd.AddValue ("UL", "Enable UL transmission?",enableUplink);
	cmd.AddValue ("NAG", "Number of Aggregators.",numAggregators);
	cmd.AddValue ("comp", "Aggregation Compression Ratio.",compressionRatio);
	cmd.AddValue ("rad", "Cell Radius (km).",areaRadius);
	cmd.AddValue ("RL", "Enable Rural area transmission?",enableRural);
	cmd.AddValue ("SU", "Enable Suburban area transmission?",enableSuburban);
	cmd.AddValue ("U", "Enable Urban area transmission?",enableUrban);
	cmd.Parse (argc, argv);

	//LogComponentEnable ("UdpClient", LOG_LEVEL_ALL);
	//LogComponentEnable ("UdpServer", LOG_LEVEL_ALL);
	//LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
	//LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink
 // LogComponentEnable ("WimaxHelper",LOG_LEVEL_ALL);
//	LogComponentEnable("WimaxNetDevice",LOG_LEVEL_ALL);
//	LogComponentEnable("Socket",LOG_LEVEL_ALL);
	//LogComponentEnable ("MobilityHelper",LOG_LEVEL_ALL); //Mobility Model
	//LogComponentEnable ("WimaxMacQueue", LOG_LEVEL_ALL);
//	LogComponentEnable("SimpleOfdmWimaxPhy", LOG_LEVEL_ALL);
//	  LogComponentEnable("WimaxMacQueue",LOG_LEVEL_ALL);
	switch (schedType)
	{
	case 0:
	  scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
	  break;
	case 1:
	  scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
	  break;
	case 2:
	  scheduler = WimaxHelper::SCHED_TYPE_RTPS;
	  break;
	default:
	  scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
	  break;
	}

	//environment and number of nodes
	int density;
	double onTimeSMUL= 0.1;//0.1
	double offTimeSMUL;
	double dataRateKBSMUL = 1.0;  //UL data rate for each smart meter (kbps)
	if (enableRural == true)
	{
		assert(enableUrban==false && enableSuburban==false);
		numMonUsers = 5; //5;//5
		numWASAUsers = 3; //3;//3
		numCtrlUsers = 3; //3;//3
		numPtctUsers = 1; //1;//1
		density = 10; //10;
		numSMUsers = int(3.14/3.0 * areaRadius * areaRadius * density); //20
		//numSMUsers = 20;
		assert(MAX_USERS>=numSMUsers);
	}
	if (enableSuburban == true)
	{
		assert(enableUrban==false && enableRural==false);
		numMonUsers = 11;
		numCtrlUsers = 8;
		numWASAUsers = 8;
		numPtctUsers = 4;
		density = 800;
		numSMUsers = 10; //int(3.14/3.0 * areaRadius * areaRadius * density);
		assert(MAX_USERS>=numSMUsers);
	}

	if (enableUrban == true)
	{
		assert(enableRural==false && enableSuburban==false);
		numMonUsers = 27;
		numCtrlUsers = 11;
		numWASAUsers = 11;
		numPtctUsers = 8;
		density = 9000; //1200; //2000;
		numSMUsers = int(3.14/3.0 * areaRadius * areaRadius * density);
		assert(MAX_USERS>=numSMUsers);
	}
	if (numAggregators > 0)
	{
		onTimeSMUL *= int(numSMUsers/numAggregators);
		assert(compressionRatio>=0.0 && compressionRatio<=1.0);
		dataRateKBSMUL *= int(compressionRatio*numSMUsers/numAggregators);
		numSMUsers = numAggregators;
	}
	else //added on 23 Nov. 2014 to test the TD configuration (20 aggregators, 266 SMs)
	{
		onTimeSMUL *= 13;
		dataRateKBSMUL *= int(compressionRatio*13);
		//numSMUsers = 20;
	}
	offTimeSMUL = onTimeSMUL>=4.1?0.0:4.1-onTimeSMUL;

	WimaxHelper wimax;

	if (verbose)
	{
	  wimax.EnableLogComponents ();  // Turn on all wimax logging
	}


	/*This is the field for defining common variables (the same for all applications), e.g. BS*/
	NodeContainer bsNodes;
	bsNodes.Create (1);
	int created_nodes=1;


	NetDeviceContainer bsDevs;
	bsDevs = wimax.Install (bsNodes, WimaxHelper::DEVICE_TYPE_BASE_STATION, WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler);
	Ptr<BaseStationNetDevice> bs;
	bs = bsDevs.Get (0)->GetObject<BaseStationNetDevice> ();
	//Added by Fariba on 15 Oct. 2014, 8:16pm
	bs->GetBandwidthManager()->SetSubframeRatio();
	//BS Position
	Ptr<ConstantPositionMobilityModel> BSPosition;
	BSPosition = CreateObject<ConstantPositionMobilityModel> ();

	BSPosition->SetPosition (Vector (0, 0, 0));
	bsNodes.Get (0)->AggregateObject (BSPosition);
  //BSPosition.m_txFreqVector = 1827;

	cout << "-aa-" << endl;

	InternetStackHelper stack;
	stack.Install (bsNodes);
	Ipv4AddressHelper address;
	address.SetBase ("10.1.0.0", "255.255.0.0"); //"10.1.1.0","255.255.255.0"
	Ipv4InterfaceContainer BSinterface = address.Assign (bsDevs);

	/*COMMON part for UDP and TCP per application*/
	NS_LOG_UNCOND("Common UDP Protection Setup.");
	NodeContainer ssNodesPtct;
	Ptr<SubscriberStationNetDevice> ssPtct[numPtctUsers];
	Ipv4InterfaceContainer SSinterfacesPtct;
//	UdpServerHelper* udpServerPtctUL = new UdpServerHelper[numPtctUsers];
	//UdpClientHelper* udpClientPtctUL = new UdpClientHelper[numPtctUsers];
//	ApplicationContainer *clientAppsPtctUL = new ApplicationContainer[numPtctUsers];
	if (numPtctUsers > 0)
	{
		NodeConfigurationCommonSetupUDP(wimax,numPtctUsers,created_nodes,areaRadius,stack,address,scheduler,modulation, ssNodesPtct,ssPtct,SSinterfacesPtct);
	}

	NS_LOG_UNCOND("Common UDP WASA Setup.");
	NodeContainer ssNodesWASA;
	Ptr<SubscriberStationNetDevice> ssWASA[numWASAUsers];
	Ipv4InterfaceContainer SSinterfacesWASA;
	if (numWASAUsers > 0)
	{
		NodeConfigurationCommonSetupUDP(wimax,numWASAUsers,created_nodes,areaRadius,stack,address,scheduler, modulation, ssNodesWASA,ssWASA,SSinterfacesWASA);
	}

	NS_LOG_UNCOND("Common UDP Monitoring (UL) Setup.");
	NodeContainer ssNodesMon;
	Ptr<SubscriberStationNetDevice> ssMon[numMonUsers];
	Ipv4InterfaceContainer SSinterfacesMon;
//	UdpServerHelper* udpServerMontUL = new UdpServerHelper[numMonUsers];
//	UdpClientHelper* udpClientMontUL = new UdpClientHelper[numMonUsers];
	UdpServerHelper* udpServerMonDL = new UdpServerHelper[numMonUsers];
//	ApplicationContainer *clientAppsMontUL = new ApplicationContainer[numMonUsers];
	if (numMonUsers > 0)
	{
		NodeConfigurationCommonSetupUDP(wimax,numMonUsers,created_nodes,areaRadius,stack,address,scheduler,modulation, ssNodesMon,ssMon,SSinterfacesMon);
	}

/***********DL to be added, UL and DL nodes should not be seperated.
//	NS_LOG_UNCOND("Common TCP Monitoring (DL) Setup.");
	NodeContainer ssNodesMonDL;
	Ptr<SubscriberStationNetDevice> ssMonDL[numMonUsers];
	std::vector<Ipv4InterfaceContainer> MonDLinterfaceAdjacencyList (numMonUsers);
	if (numMonUsers > 0)
	{
		NodeConfigurationCommonSetupTCP(wimax,numMonUsers,created_nodes,areaRadius,stack,address,scheduler,bsDevs,bsNodes,ssNodesMonDL,ssMonDL,MonDLinterfaceAdjacencyList);
	}
*/

	NS_LOG_UNCOND("Common UDP Control Setup.");
	NodeContainer ssNodesCtrl;
	Ptr<SubscriberStationNetDevice> ssCtrl[numCtrlUsers];
	Ipv4InterfaceContainer SSinterfacesCtrl;
	//std::vector<Ipv4InterfaceContainer> CtrlinterfaceAdjacencyList (numCtrlUsers);
	if (numCtrlUsers > 0)
	{
		NodeConfigurationCommonSetupUDP(wimax,numCtrlUsers,created_nodes,areaRadius,stack,address,scheduler, modulation, ssNodesCtrl,ssCtrl,SSinterfacesCtrl);
	}

	NodeContainer ssNodesSM;
	Ptr<SubscriberStationNetDevice> ssSM[numSMUsers];
	Ipv4InterfaceContainer SSinterfacesSM;
//	std::vector<Ipv4InterfaceContainer> SMinterfaceAdjacencyList (numSMUsers);
  NodeContainer ssNodesSMUL;
	Ptr<SubscriberStationNetDevice> ssSMUL[numSMUsers];
	Ipv4InterfaceContainer SSinterfacesSMUL;
	UdpServerHelper* udpServerSMUL = new UdpServerHelper[numSMUsers];
	UdpClientHelper* udpClientSMUL = new UdpClientHelper[numSMUsers];
	ApplicationContainer *clientAppsSMUL = new ApplicationContainer[numSMUsers];

	//UdpServerHelper udpServerPtctDL[numPtctUsers];
			/*if (numAggregators ==0)
	{*/
		//NS_LOG_UNCOND("Common TCP Smart Meters Setup.");
		if (numSMUsers > 0)
		{
			//areaRadius = 0.1;
			NodeConfigurationCommonSetupUDP(wimax,numSMUsers,created_nodes,areaRadius,stack,address,scheduler, modulation, ssNodesSM,ssSM,SSinterfacesSM);
		}
	//}
	/*else
	{
		NS_LOG_UNCOND("Common UDP Smart Meters (UL Agg.) Setup.");
		if (numMonUsers > 0)
		{
			NodeConfigurationCommonSetupUDP(wimax,numSMUsers,created_nodes,areaRadius,stack,address,scheduler,ssNodesSMUL,ssSMUL,SSinterfacesSMUL);
		}
	}*/
	/* END COMMON*/

	/*-------------Traffic Generation (UL and then DL)-----------------*/
	if (enableUplink)
	{
		//NodeConfigurationCommonSetup(wimax,5,stack,address,scheduler,ssNodes,ss,SSinterfaces);

		//Control is TCP:
		appnumBeforeWASAUL = bsNodes.Get(0)->GetNApplications();
		if (numWASAUsers > 0)
		{
			NS_LOG_UNCOND("Setting up UL UDP for WASA.");
			port = setup_udp_onoff(numWASAUsers, fidWASAUL, ssNodesWASA, SSinterfacesWASA, ssWASA,
					bsNodes, BSinterface, duration, "ns3::ConstantRandomVariable[Constant=1]",
					"ns3::ConstantRandomVariable[Constant=5]",
					256,"5kb/s", port,wimax, modulation,ServiceFlow::SF_TYPE_NRTPS);
		}
		appnumAfterWASAUL = bsNodes.Get(0)->GetNApplications();


		//Protection is UDP:
		appnumBeforePtctUL = bsNodes.Get(0)->GetNApplications();
		if (numPtctUsers > 0)
		{
			NS_LOG_UNCOND("Setting up UL UDP for Protection.");
			port = setup_udp_onoff(numPtctUsers, fidPtctUL, ssNodesPtct, SSinterfacesPtct, ssPtct, bsNodes, BSinterface, duration, "ns3::ExponentialRandomVariable[Mean=1]", "ns3::ExponentialRandomVariable[Mean=0.1]",
								128,"150kb/s", port,wimax, modulation, ServiceFlow::SF_TYPE_UGS, 20);
			//port = Uplink_UDP_Traffic(wimax, port, numPtctUsers, duration, BSinterface, SSinterfacesPtct, bsNodes, ssNodesPtct, ssPtct,2.0, fidPtctUL, 128, udpClientPtctUL, udpServerPtctUL, clientAppsPtctUL);
		}
		appnumAfterPtctUL = bsNodes.Get(0)->GetNApplications();

		//Monitoring UL is UDP:
		appnumBeforeMonUL = bsNodes.Get(0)->GetNApplications();
		if (numMonUsers > 0)
		{
			NS_LOG_UNCOND("Setting up UL UDP for Monitoring.");
			port = setup_udp_onoff(numMonUsers, fidMonUL, ssNodesMon, SSinterfacesMon,  ssMon, bsNodes, BSinterface, duration, "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=0]",
											256,"300kb/s",//300
											port,wimax, modulation, ServiceFlow::SF_TYPE_UGS, 100);
			//port = Uplink_UDP_Traffic(wimax, port, numMonUsers, duration, BSinterface, SSinterfacesMon, bsNodes, ssNodesMon, ssMon,0.0066, fidMonUL, 256, udpClientMontUL, udpServerMontUL, clientAppsMontUL, ServiceFlow::SF_TYPE_UGS);
		}
		appnumAfterMonUL = bsNodes.Get(0)->GetNApplications();

		//Control is TCP:
		appnumBeforeCtrlUL = bsNodes.Get(0)->GetNApplications();
		if (numCtrlUsers > 0)
		{
			NS_LOG_UNCOND("Setting up UL UDP for control.");
			//UdpServerHelper* udpServerMontUL = new UdpServerHelper[numMonUsers];
			port = setup_udp_onoff(numCtrlUsers, fidCtrlUL, ssNodesCtrl, SSinterfacesCtrl, ssCtrl, bsNodes, BSinterface, duration, "ns3::ExponentialRandomVariable[Mean=1]", "ns3::ExponentialRandomVariable[Mean=5]",
					128,"5kb/s", port,wimax, modulation, ServiceFlow::SF_TYPE_RTPS);

			/*fid, tmpNodes3 , tmpIfs3, sstmp3,
								bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
								on_time, off_time, pkt_size, data_rate, startPort, wimaxAlcona[sectNum],
								modulation);*/
			//(wimax, port, numCtrlUsers, duration, BSinterface, SSinterfacesCtrl,  bsNodes, ssNodesCtrl, ssCtrl,
			//"ns3::ExponentialRandomVariable[Mean=1]", "ns3::ExponentialRandomVariable[Mean=5]", 1, ServiceFlow::SF_TYPE_RTPS, "5kb/s", 128);
		  //"ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=5]", 1, ServiceFlow::SF_TYPE_RTPS, "5kb/s", 128);
		}
		appnumAfterCtrlUL = bsNodes.Get(0)->GetNApplications();

		//SmartMetering is TCP:
		appnumBeforeSMUL = bsNodes.Get(0)->GetNApplications();
		if (numSMUsers > 0)
		{
			if (numAggregators ==0)
			{
				std::ostringstream onTimeSMULStr,offTimeSMULStr,dataRateKBSMULStr;
				onTimeSMULStr << "ns3::ConstantRandomVariable[Constant=" << onTimeSMUL << "]";
				offTimeSMULStr << "ns3::ConstantRandomVariable[Constant=" << offTimeSMUL << "]";
				dataRateKBSMULStr << dataRateKBSMUL << "kb/s";
				NS_LOG_UNCOND("Setting up UL UDP for smart metering, onTime: " << onTimeSMULStr.str() <<
						", offTime: " << offTimeSMULStr.str() << ", dataRate: " << dataRateKBSMULStr.str());
				port = setup_udp_onoff(numSMUsers, fidSMUL, ssNodesSM, SSinterfacesSM, ssSM, bsNodes, BSinterface, duration, (std::string) onTimeSMULStr.str(), (std::string) offTimeSMULStr.str(),
									128,(std::string)dataRateKBSMULStr.str(), port,wimax, modulation, ServiceFlow::SF_TYPE_BE);
				/*port = Uplink_TCP_Traffic(wimax, port, numSMUsers, duration, BSinterface, SMinterfaceAdjacencyList, bsNodes, ssNodesSM, ssSM,
						onTimeSMULStr.str(), offTimeSMULStr.str(), 3, ServiceFlow::SF_TYPE_RTPS, dataRateKBSMULStr.str(), 128); //SF_TYPE_BE*/
			}
			else
			{
				NS_LOG_UNCOND("Setting up UL UDP for smart metering (aggregation), dataRate: " << dataRateKBSMUL);
				port = Uplink_UDP_Traffic(wimax, port, numSMUsers, duration, BSinterface, SSinterfacesSMUL, bsNodes, ssNodesSMUL, ssSMUL,128.0/dataRateKBSMUL, fidAgg, 128, udpClientSMUL, udpServerSMUL, clientAppsSMUL, ServiceFlow::SF_TYPE_BE);
			}
		}
		appnumAfterSMUL = bsNodes.Get(0)->GetNApplications();

	}

	if (enableDownlink)
	{
		//Control is TCP:
		appnumBeforeWASADL = bsNodes.Get(0)->GetNApplications();
		if (numWASAUsers > 0)
		{
			NS_LOG_UNCOND("Setting up DL UDP for WASA.");
			port = Downlink_onoff_Traffic(numWASAUsers, fidWASADL, ssNodesWASA, SSinterfacesWASA,
					                         ssWASA, bsNodes, BSinterface, duration,
					                         "ns3::ConstantRandomVariable[Constant=1]",
					                         "ns3::ConstantRandomVariable[Constant=5]",//5
												           256,"1kb/s", port,wimax, modulation, ServiceFlow::SF_TYPE_NRTPS);
		}
		appnumAfterWASADL = bsNodes.Get(0)->GetNApplications();

		appnumBeforePtctDL = bsNodes.Get(0)->GetNApplications();
		if (numPtctUsers > 0)
		{
			NS_LOG_UNCOND("Setting up DL UDP for Protection.");
			NS_LOG_UNCOND("Setting up DL TCP for monitoring.");
			port = Downlink_onoff_Traffic(numPtctUsers, fidPtctDL, ssNodesPtct, SSinterfacesPtct,
					ssPtct, bsNodes, BSinterface, duration,"ns3::ExponentialRandomVariable[Mean=1]", "ns3::ExponentialRandomVariable[Mean=0.1]",128,"150kb/s", port,wimax, modulation, ServiceFlow::SF_TYPE_UGS, 20);
		/*	port = Downlink_UDP_Traffic(wimax, port, numPtctUsers, duration, SSinterfacesPtct, bsNodes, ssNodesPtct,
					ssPtct, fidPtctDL, 128, udpServerPtctDL);*/
		}
		appnumAfterPtctDL = bsNodes.Get(0)->GetNApplications();

		//Monitoring is TCP:
		appnumBeforeMonDL = bsNodes.Get(0)->GetNApplications();
		double monDLInterval = 0.1;
		if (numMonUsers > 0)
		{
			NS_LOG_UNCOND("Setting up DL UDP for monitoring.");
			port = Downlink_UDP_Traffic(wimax, port, numMonUsers, duration, SSinterfacesMon, bsNodes, ssNodesMon,
								ssMon, fidMonDL, 128, monDLInterval, udpServerMonDL, ServiceFlow::SF_TYPE_BE, 100);
			/*port = Downlink_onoff_Traffic(numMonUsers, fidMonDL, ssNodesMon, SSinterfacesMon,
					                         ssMon, bsNodes, BSinterface, duration,
					                         "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=0]",
												           128,"10kb/s", port,wimax, modulation);*/
			/*port = Downlink_UDP_Traffic(wimax, port, numMonUsers, duration, SSinterfacesMont, bsNodes, ssNodesMon, ssMon,
					"ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=5]", 4, ServiceFlow::SF_TYPE_NRTPS, "10kb/s", 256);*/

		}
		appnumAfterMonDL = bsNodes.Get(0)->GetNApplications();

		//Control is TCP:
		appnumBeforeCtrlDL = bsNodes.Get(0)->GetNApplications();
		if (numCtrlUsers > 0)
		{
			NS_LOG_UNCOND("Setting up DL UDP for control.");
			port = Downlink_onoff_Traffic(numCtrlUsers, fidCtrlDL, ssNodesCtrl, SSinterfacesCtrl,
					                         ssCtrl, bsNodes, BSinterface, duration,
					                         "ns3::ConstantRandomVariable[Constant=1]", "ns3::ConstantRandomVariable[Constant=5]",//5
												           128,"1kb/s", port,wimax, modulation, ServiceFlow::SF_TYPE_RTPS);
		}
		appnumAfterCtrlDL = bsNodes.Get(0)->GetNApplications();

//		//SmartMetering is TCP:
//		appnumBeforeSMDL = bsNodes.Get(0)->GetNApplications();
//		if (numSMUsers > 0)
//		{
//			NS_LOG_UNCOND("Setting up DL TCP for smart metering.");
//			port = Downlink_TCP_Traffic(wimax, port, numSMUsers, duration, BSinterface, SMDLinterfaceAdjacencyList, bsNodes, ssNodesSMDL, ssSMDL,
//					"ns3::ConstantRandomVariable[Constant=0.1]", "ns3::ConstantRandomVariable[Constant=4]", 7, ServiceFlow::SF_TYPE_BE, "50b/s", 128);
//		}
//		appnumAfterSMDL = bsNodes.Get(0)->GetNApplications();
	}
  //run
  Simulator::Stop (Seconds (duration + 0.1));
  NS_LOG_UNCOND ("Starting simulation for " << numPtctUsers << " Protection Users, " << numMonUsers << " Monitoring Users, " <<
		  numCtrlUsers << " Control Users, and " << numSMUsers << " Smart Meters (SM UL only rate " << dataRateKBSMUL  << "kbps)...");
  NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink<<numAggregators);
  Simulator::Run ();
  NS_LOG_UNCOND ("Printing statistics for " << numPtctUsers << " Protection Users, " << numMonUsers << " Monitoring Users, " <<
		  numCtrlUsers << " Control Users, and " << numSMUsers << " Smart Meters (SM UL only rate " << dataRateKBSMUL  << "kbps)...");
  NS_LOG_UNCOND ("UL/DL/Agg = "<<enableUplink<<enableDownlink<<numAggregators);

  /******************************GET STATISTICS**********************************************************/
  cout << "hello" << endl;
  //
  //Statistics for protection
/*
  uint32_t losstPtct[numPtctUsers], sumLosstPtct=0;
	uint32_t senttPtct[numPtctUsers], sumSentPtct=0;
	uint32_t senttPtctFromApp[numPtctUsers], sumSenttPtctFromApp=0;
	uint32_t rcvddPtct[numPtctUsers], sumRcvdPtct=0;
*/

	if (enableUplink){
		/*for (int i=0; i < numPtctUsers; i++)
			{
				cout << i << endl;
				Ptr<UdpServer> myserver = udpServerPtctUL[i].GetServer();
				cout << "?" << endl;
				cout << myserver->GetLost() << endl;
				cout << losstPtct[i]  << endl;
				losstPtct[i] = myserver->GetLost();
				sumLosstPtct += losstPtct[i];
				cout << "/1/" << endl;

				rcvddPtct[i] = udpServerPtctUL[i].GetServer()->GetReceived();
				sumRcvdPtct += rcvddPtct[i];
				cout << "/2/" << endl;


	//			Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(ssDevsAlcona[sectNum].Get(0)->GetObject<SubscriberStationNetDevice>()->GetPhy());
				Ptr<UdpClient> cl = DynamicCast<UdpClient,Application>(clientAppsPtctUL[i].Get(0));
				//cl = Ptr<UdpClient>(DynamicCast<UdpClient *,Application *>()));
				senttPtctFromApp[i] = cl->m_sent;
				sumSenttPtctFromApp += senttPtctFromApp[i];
				cout << "/3/" << endl;

				Ptr<Node> myNode = ssNodesPtct.Get(i);
				senttPtct[i] = myNode->m_Sent[fidPtctUL];
				sumSentPtct += senttPtct[i];
			}*/

		/*	cout << "sumSentPtct =" << sumSentPtct << endl
					 << "sumSenttPtctFromApp=" << sumSenttPtctFromApp << endl
					 << "sumRcvdPtct=" << sumRcvdPtct << endl
					 << "sumLosstPtct=" << sumLosstPtct << endl;*/

			//Statistics for Monitoring Apps
			/*uint32_t losstMont[numMonUsers], sumLosstMont=0;
			uint32_t senttMont[numMonUsers], sumSentMont=0;
			uint32_t senttMontFromApp[numPtctUsers], sumSenttMontFromApp=0;
			uint32_t rcvddMont[numMonUsers], sumRcvdMont=0;

			for (int i=0; i < numMonUsers; i++)
				{
					cout << i << endl;
					Ptr<UdpServer> myserver = udpServerMontUL[i].GetServer();
					cout << "?" << endl;
			//		cout << myserver->GetLost() << endl;
			//		cout << losstMont[i]  << endl;
					losstMont[i] = myserver->GetLost();
					sumLosstMont += losstMont[i];
			//		cout << "1" << endl;

					rcvddMont[i] = udpServerMontUL[i].GetServer()->GetReceived();
					sumRcvdMont += rcvddMont[i];
			//		cout << "2" << endl;

					Ptr<UdpClient> cl = DynamicCast<UdpClient,Application>(clientAppsMontUL[i].Get(0));
					senttMontFromApp[i] = cl->m_sent;
					sumSenttMontFromApp += senttMontFromApp[i];

					Ptr<Node> myNode = ssNodesMon.Get(i);
					senttMont[i] = myNode->m_Sent[fidMonUL];
					sumSentMont += senttMont[i];
				}
				cout << "sumSentMont =" << sumSentMont << endl
						 << "sumSenttMontFromApp=" << sumSenttMontFromApp << endl
						 << "sumRcvdMont=" << sumRcvdMont << endl
						 <<	"sumLosstMont=" << sumLosstMont << endl;*/

					//Statistics for Smart Metering Apps
				uint32_t losstSM[numSMUsers], sumLosstSM=0;
				uint32_t senttSM[numSMUsers], sumSentSM=0;
				uint32_t rcvddSM[numSMUsers], sumRcvdSM=0;

				if (numAggregators != 0)
					{
						for (int i=0; i < numSMUsers; i++)
							{
								cout << i << endl;
								Ptr<UdpServer> myserver = udpServerSMUL[i].GetServer();
							//	cout << "?" << endl;
								cout << myserver->GetLost() << endl;
								cout << losstSM[i]  << endl;
								losstSM[i] = myserver->GetLost();
								sumLosstSM += losstSM[i];
							//	cout << "1" << endl;

								rcvddSM[i] = udpServerSMUL[i].GetServer()->GetReceived();
								sumRcvdSM += rcvddSM[i];
							//	cout << "2" << endl;

								Ptr<Node> myNode = ssNodesSMUL.Get(i);
								senttSM[i] = myNode->m_Sent[fidSMUL];
								sumSentSM += senttSM[i];
							}
								cout << "sumSentSM =" << sumSentSM << endl << "sumRcvdSM=" << sumRcvdSM << endl <<
												"sumLosstSM=" << sumLosstSM << endl;
					}
	}
	//Print Statistics:
		int reliable_received_all_nodes[FLOW_NUM];
		//ns3::Time latency_all_nodes[FLOW_NUM];
		Time Delays_all_nodes[FLOW_NUM];
		Time ReliableDelays_all_nodes[FLOW_NUM];
		int Recv_all_nodes[FLOW_NUM];
		double avg_Delays_all_nodes[FLOW_NUM];
		double avg_ReliableDelays_all_nodes[FLOW_NUM];

		for (int flowid = 0; flowid < FLOW_NUM; flowid++) {
				reliable_received_all_nodes[flowid] = 0;
				Delays_all_nodes[flowid] = Time(0.0);
				ReliableDelays_all_nodes[flowid] = Time(0.0);
				Recv_all_nodes[flowid] = 0;


				avg_Delays_all_nodes[flowid] = 0;
				avg_ReliableDelays_all_nodes[flowid] = 0;
				/*
				min_Delays_all_nodes[flowid] = 0;
				min_ReliableDelays_all_nodes[flowid] = 0;

				var_Delays_all_nodes[flowid] = 0;
				var_ReliableDelays_all_nodes[flowid] = 0;
		*/
			}
		int Sent[FLOW_NUM]; //, Recv[FLOW_NUM];
		for (int flowid = 0; flowid < FLOW_NUM; flowid++)
			Sent[flowid] = 0;
		if(enableUplink)
		{
				Ptr<Node> myNode = bsNodes.Get(0);
				for (int flowid = 0; flowid < START_DL_FID; flowid++) {
					//NS_LOG_UNCOND(sectNum << 	":" << flowid  );
					reliable_received_all_nodes[flowid] += myNode->reliable_received[flowid];
					//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
					Delays_all_nodes[flowid] += myNode->Delays[flowid];
					//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
					ReliableDelays_all_nodes[flowid] += myNode->ReliableDelays[flowid];
					//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
					Recv_all_nodes[flowid] += myNode->Recv[flowid];
					//NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
				}


					for (int flowid = 0; flowid < FLOW_NUM; flowid++) {
						Sent[flowid] = 0;
						//Recv[flowid] = 0;
					}
					cout << "be" << endl;
					//UL Sent: Ptct
					for (int i = 0; i < numPtctUsers; i++) {
						Sent[fidPtctUL] += ssNodesPtct.Get(i)->m_Sent/*sentBytes*/[fidPtctUL];//m_Sent[flowid]
					}
		 }

		//DL WASA
		for (int i = 0; i < numWASAUsers; i++) {
				reliable_received_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->reliable_received[fidWASADL];
				//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
				Delays_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->Delays[fidWASADL];
				//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
				ReliableDelays_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->ReliableDelays[fidWASADL];
				//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
				Recv_all_nodes[fidWASADL] += ssNodesWASA.Get(i)->Recv[fidWASADL];
		}

		//DL Ptct
			for (int i = 0; i < numPtctUsers; i++) {
					reliable_received_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->reliable_received[fidPtctDL];
					//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
					Delays_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->Delays[fidPtctDL];
					//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
					ReliableDelays_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->ReliableDelays[fidPtctDL];
					//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
					Recv_all_nodes[fidPtctDL] += ssNodesPtct.Get(i)->Recv[fidPtctDL];
			}
			//DL Mont
			for (int i = 0; i < numMonUsers; i++) {
					reliable_received_all_nodes[fidMonDL] += ssNodesMon.Get(i)->reliable_received[fidMonDL];
					//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
					Delays_all_nodes[fidMonDL] += ssNodesMon.Get(i)->Delays[fidMonDL];
					//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
					ReliableDelays_all_nodes[fidMonDL] += ssNodesMon.Get(i)->ReliableDelays[fidMonDL];
					//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
					Recv_all_nodes[fidMonDL] += ssNodesMon.Get(i)->Recv[fidMonDL];
			}
			//DL Ctrl
			for (int i = 0; i < numCtrlUsers; i++) {
					reliable_received_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->reliable_received[fidCtrlDL];
					//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
					Delays_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->Delays[fidCtrlDL];
					//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
					ReliableDelays_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->ReliableDelays[fidCtrlDL];
					//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
					Recv_all_nodes[fidCtrlDL] += ssNodesCtrl.Get(i)->Recv[fidCtrlDL];
			}

			cout << "khand" << endl;
			if(enableUplink)
			{
				//UL Sent: WASA
				for (int i = 0; i < numWASAUsers; i++) {
					Sent[fidWASAUL] += ssNodesWASA.Get(i)->m_Sent/*sentBytes*/[fidWASAUL];//m_Sent[flowid]
				}
				//UL Sent: Mont
				for (int i = 0; i < numMonUsers; i++) {
					Sent[fidMonUL] += ssNodesMon.Get(i)->m_Sent/*sentBytes*/[fidMonUL];//m_Sent[flowid]
				}
				//UL Sent: Ctrl
				for (int i = 0; i < numCtrlUsers; i++) {
					Sent[fidCtrlUL] += ssNodesCtrl.Get(i)->m_Sent/*sentBytes*/[fidCtrlUL];//m_Sent[flowid]
				}
				cout << "ruye" <<endl;
				//UL Sent: SM
				for (int i = 0; i < numSMUsers; i++) {
					Sent[fidSMUL] += ssNodesSM.Get(i)->m_Sent/*sentBytes*/[fidSMUL];//m_Sent[flowid]
				}
			}
			//cout << "donya" << endl;
			for (int flowid = START_DL_FID; flowid < END_DL_FID; flowid++) {
				Sent[flowid] += bsNodes.Get(0)->m_Sent/*sentBytes*/[flowid];
			}


			cout << "flow ID\tsent\t\treliable rcvd\treliable delay\tall rcvd\tall delay" << endl;
						//cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
			int cnt = -1;
			if(enableUplink)
				cnt = 0;
			else
				cnt = START_DL_FID;
			for (int flowid = cnt; flowid < END_DL_FID; flowid++) {
				avg_ReliableDelays_all_nodes[flowid] = ReliableDelays_all_nodes[flowid].ToDouble(Time::MS)
										/ (double) reliable_received_all_nodes[flowid];
				avg_Delays_all_nodes[flowid] = Delays_all_nodes[flowid].ToDouble(Time::MS)
										/ (double) Recv_all_nodes[flowid];
				cout << flowid << "\t" << Sent[flowid] << "\t\t" << reliable_received_all_nodes[flowid] << "\t\t"
						<< avg_ReliableDelays_all_nodes[flowid] << "\t\t"
						<< Recv_all_nodes[flowid] << "\t\t"
						<< avg_Delays_all_nodes[flowid] << endl;
			}

/*
  //UDP for Protection UL (sum)
  int totalSentPtctUL = 0;
  for (int j=0; j < numPtctUsers; j++)
  {
	  //NS_LOG_UNCOND("????????");
	  Ptr<UdpClient> myApp = Ptr<UdpClient> (dynamic_cast<UdpClient *> (PeekPointer(ssNodesPtct.Get(j)->GetApplication(0))));
	  totalSentPtctUL +=  myApp->m_Sent[2];
  }
  //NS_LOG_UNCOND("totalSentPtctUL (fid=2)=" << totalSentPtctUL);
  //UDP for Monitoring UL (sum)
  int totalSentMonUL = 0;
  for (int j=0; j < numMonUsers; j++)
  {
	  Ptr<UdpClient> myApp = Ptr<UdpClient> (dynamic_cast<UdpClient *> (PeekPointer(ssNodesMon.Get(j)->GetApplication(0))));
	  totalSentMonUL +=  myApp->m_Sent[0];
  }
  //NS_LOG_UNCOND("totalSentMonUL (fid=0)=" << totalSentMonUL);
  //on-off for Control UL (sum)
  int totalSentCtrlUL = 0;
  for (int j=0; j < numCtrlUsers; j++)
  {
	  Ptr<OnOffApplication> myApp = Ptr<OnOffApplication> (dynamic_cast<OnOffApplication *> (PeekPointer(ssNodesCtrl.Get(j)->GetApplication(0))));
	  totalSentCtrlUL += myApp->m_Sent[1];
  }
  //on-off for SM UL (sum)
  int totalSentSMUL = 0;
  for (int j=0; j < numSMUsers; j++)
  {
	  if (numAggregators==0)
	  {
		  Ptr<OnOffApplication> myApp = Ptr<OnOffApplication> (dynamic_cast<OnOffApplication *> (PeekPointer(ssNodesSM.Get(j)->GetApplication(0))));
		  totalSentSMUL += myApp->m_Sent[3];
	  }
	  else
	  {
		  Ptr<UdpClient> myApp = Ptr<UdpClient> (dynamic_cast<UdpClient *> (PeekPointer(ssNodesSMUL.Get(j)->GetApplication(0))));
		  totalSentSMUL += myApp->m_Sent[3];
	  }
  }

  NS_LOG_UNCOND("All UL flows:\nFlow Id\tSent\tRecv\tReliable Recv\tAvg. Delay\n");
  myNode = bsNodes.Get(0);
  for (int i=0; i< FLOW_NUM; i++)
  {
	  //NS_LOG_UNCOND("GGGGGGGGG"<<i<<myNode->Recv[i]);
	  int totalSent;
	  if (i==0)
		  totalSent = totalSentMonUL;
	  else if (i==2)
		  totalSent = totalSentPtctUL;
	  else if (i==1)
		  totalSent = totalSentCtrlUL;
	  else if (i==3)
		  totalSent= totalSentSMUL;
	  else
		  continue;
	  NS_LOG_UNCOND(i << "\t" << totalSent << "\t" << myNode->Recv[i] << "\t" <<  myNode->reliable_received[i] << "\t\t"
			  << myNode->Delays[i].ToDouble(Time::MS)/float(myNode->Recv[i]) << std::endl );
  }

  NS_LOG_UNCOND("TCP DL Mon:\nFlow Id\tSent\tRecv\tReliable Recv\tAvg. Delay\n");
  int totalRecvMonDL = 0, totalRelRecvMonDL = 0, totalSentMonDLAll=0;
  double totalDelaysMonDL = 0.0;
  for (int j=0; j<numMonUsers; j++)
  {
  //NS_LOG_UNCOND("Control Node#" << j << "--");
	  Ptr<Node> myNode = ssNodesMonDL.Get(j);
	  totalRecvMonDL += myNode->Recv[4];
	  totalRelRecvMonDL += myNode->reliable_received[4];
	  totalDelaysMonDL += myNode->Delays[4].ToDouble(Time::MS);
	  totalSentMonDLAll += totalSentMonDL[j];
  }
  //Ptr<OnOffApplication> myApp = Ptr<OnOffApplication> (dynamic_cast<OnOffApplication *> (PeekPointer(myNode->GetApplication(0))));
  NS_LOG_UNCOND(4 << "\t" << totalSentMonDLAll << "\t" << totalRecvMonDL << "\t" <<  totalRelRecvMonDL << "\t\t"
		  << totalDelaysMonDL/float(totalRecvMonDL) << std::endl );

  NS_LOG_UNCOND("TCP DL Ctrl:\nFlow Id\tSent\tRecv\tReliable Recv\tAvg. Delay\n");
  int totalRecvCtrlDL = 0, totalRelRecvCtrlDL = 0, totalSentCtrlDLAll=0;
  double totalDelaysCtrlDL = 0.0;
  for (int j=0; j<numCtrlUsers; j++)
  {
  //NS_LOG_UNCOND("Control Node#" << j << "--");
	  Ptr<Node> myNode = ssNodesCtrl.Get(j);
	  totalRecvCtrlDL += myNode->Recv[5];
	  totalRelRecvCtrlDL += myNode->reliable_received[5];
	  totalDelaysCtrlDL += myNode->Delays[5].ToDouble(Time::MS);
	  totalSentCtrlDLAll += totalSentCtrlDL[j];
  }
  //Ptr<OnOffApplication> myApp = Ptr<OnOffApplication> (dynamic_cast<OnOffApplication *> (PeekPointer(myNode->GetApplication(0))));
  NS_LOG_UNCOND(5 << "\t" << totalSentCtrlDLAll << "\t" << totalRecvCtrlDL << "\t" <<  totalRelRecvCtrlDL << "\t\t"
		  << totalDelaysCtrlDL/float(totalRecvCtrlDL) << std::endl );


  NS_LOG_UNCOND("UDP DL Ptct:\nFlow Id\tSent\tRecv\tReliable Recv\tAvg. Delay\n");
  int totalRecvPtctDL = 0, totalRelRecvPtctDL = 0, totalSentPtctDLAll=0;
  double totalDelaysPtctDL = 0.0;
  for (int j=0; j<numPtctUsers; j++)
  {
  //NS_LOG_UNCOND("Control Node#" << j << "--");
	  Ptr<Node> myNode = ssNodesPtct.Get(j);
	  totalRecvPtctDL += myNode->Recv[6];
	  totalRelRecvPtctDL += myNode->reliable_received[6];
	  totalDelaysPtctDL += myNode->Delays[6].ToDouble(Time::MS);
	  totalSentPtctDLAll+= totalSentPtctDL[j];
  }
  //Ptr<OnOffApplication> myApp = Ptr<OnOffApplication> (dynamic_cast<OnOffApplication *> (PeekPointer(myNode->GetApplication(0))));
  NS_LOG_UNCOND(6 << "\t" << totalSentPtctDLAll << "\t" << totalRecvPtctDL << "\t" <<  totalRelRecvPtctDL << "\t\t"
		  << totalDelaysPtctDL/float(totalRecvPtctDL) << std::endl );

//  NS_LOG_UNCOND("TCP DL SM:\nFlow Id\tSent\tRecv\tReliable Recv\tAvg. Delay\n");
//  int totalRecvSMDL = 0, totalRelRecvSMDL = 0, totalSentSMDLAll=0;
//  double totalDelaysSMDL = 0.0;
//  for (int j=0; j<numSMUsers; j++)
//  {
//  //NS_LOG_UNCOND("Control Node#" << j << "--");
//	  Ptr<Node> myNode = ssNodesSMDL.Get(j);
//	  totalRecvSMDL += myNode->Recv[7];
//	  totalRelRecvSMDL += myNode->reliable_received[7];
//	  totalDelaysSMDL += myNode->Delays[7].ToDouble(Time::MS);
//	  totalSentSMDLAll += totalSentSMDL[j];
//  }

//  NS_LOG_UNCOND(7 << "\t" << totalSentSMDLAll << "\t" << totalRecvSMDL << "\t" <<  totalRelRecvSMDL << "\t\t"
//		  << totalDelaysSMDL/float(totalRecvSMDL) << std::endl );

//  //cleanup
//  for (int i = 0; i < numUsers; i++)
//  {
//	  (*ssPtr)[i] = 0;
//  }*/
  Simulator::Destroy ();
  NS_LOG_UNCOND ("Done.");

  return 0;
}
