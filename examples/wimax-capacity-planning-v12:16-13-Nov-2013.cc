/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */

//
// Default network topology includes a base station (BS) and 2
// subscriber station (SS).

//      +-----+
//      | SS0 |
//      +-----+
//     10.1.1.1
//      -------
//        ((*))
//
//                  10.1.1.7
//               +------------+
//               |Base Station| ==((*))
//               +------------+
//
//        ((*))
//       -------
//      10.1.1.2
//       +-----+
//       | SS1 |
//       +-----+

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include <iostream>
#include <vector>

NS_LOG_COMPONENT_DEFINE ("WimaxCapacityExample");

using namespace ns3;

void ReadData (std::string filename, int &ssNum, int &bsNum, Ptr<ListPositionAllocator> ssPositionAlloc, Ptr<ListPositionAllocator> bsPositionAlloc,
		  Ptr<std::vector<double> > ssAntennaHeight,
		  Ptr<std::vector<double> > ssTxGain,
		  Ptr<std::vector<double> > ssRxGain,
		  Ptr<std::vector<double> > ssTxPower,
		  Ptr<std::vector<double> > bsAntennaHeight,
		  Ptr<std::vector<double> >bsTxGain,
		  Ptr<std::vector<double> >bsRxGain,
		  Ptr<std::vector<double> >bsTxPower,
		  Ptr<std::vector<unsigned int> > ctrFreq)
{
	NS_LOG_UNCOND("opening file");
	std::ifstream file ( "20130917sites.csv" ); // declare file stream: http://www.cplusplus.com/reference/iostream/ifstream/
	NS_LOG_UNCOND("opened file");
	std::string value;
	int lineNumber=0;

	while ( file.good() )
	{
		NS_LOG_UNCOND("file is good! Yes" );
		lineNumber++;
		double x,y,z;
		double Height = 0;
		for (int i=0;i<9;i++)
		{
			getline ( file, value, ',' ); // read a string until next comma: http://www.cplusplus.com/reference/string/getline/
			if (i==3)
				z=std::atof(value.c_str());
			if (i==4)
				Height = std::atof(value.c_str());
			if (i==1)
				x=(std::atof(value.c_str())-44)*111.2;
			if (i==2)
				y=(std::atof(value.c_str())+79)*79.5;

			if (i==5 && std::strcmp(value.c_str(),"YES")==1)
				{
					ssNum++;
					ssPositionAlloc ->Add(Vector(x, y, z));
					ssAntennaHeight ->push_back(Height);
				}
			if (i==6 && std::strcmp(value.c_str(),"YES")==1)
				{
					bsNum++;
					bsPositionAlloc ->Add(Vector(x, y, z));
					bsAntennaHeight ->push_back(Height);
				}

			NS_LOG_UNCOND(lineNumber << "," << i << ":" << value << std::endl);
		}


	    //cout << string( value, 1, value.length()-2 ); // display value removing the first and the last character from it
	}

	file.close();
	/*for(int i = 0; i < nodes.GetN(); i++)
	{
		nodes.Get(i)->
	}*/
}

int main (int argc, char *argv[])
{
  bool verbose = false;

  int duration = 10, schedType = 0, ssNum=1, bsNum=0, modType  = 1;
  WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
  WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;
  MobilityHelper ssMobility, bsMobility;

  CommandLine cmd;
  cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
  cmd.AddValue ("duration", "duration of the simulation in seconds", duration);
  cmd.AddValue ("verbose", "turn on all WimaxNetDevice log components", verbose);
  cmd.AddValue ("modType", "Type of modulation", modType);

  cmd.Parse (argc, argv);

  NS_LOG_UNCOND("Start...");
  switch (schedType)
    {
    case 0:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
      break;
    case 1:
      scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
      break;
    case 2:
      scheduler = WimaxHelper::SCHED_TYPE_RTPS;
      break;
    default:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
    }

  switch (modType)
    {
    case 0:
      modulation = WimaxPhy::MODULATION_TYPE_BPSK_12;
      break;
    case 1:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_12;
      break;
    case 2:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_34;
      break;
    case 3:
      modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;
      break;
    case 4:
      modulation = WimaxPhy::MODULATION_TYPE_QAM16_34;
      break;
    case 5:
      modulation = WimaxPhy::MODULATION_TYPE_QAM64_23;
      break;
    case 6:
      modulation = WimaxPhy::MODULATION_TYPE_QAM64_34;
      break;
      default:
      modulation = WimaxPhy::MODULATION_TYPE_QPSK_12;
    }

  NodeContainer ssNodes;
  NodeContainer bsNodes;
  Ptr<std::vector<double> > ssAntennaHeight= CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > ssTxGain = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > ssRxGain = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > ssTxPower = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > bsAntennaHeight = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > bsTxGain = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > bsRxGain = CreateObject <std::vector<double> > ();
  Ptr<std::vector<double> > bsTxPower = CreateObject <std::vector<double> > ();
  Ptr<std::vector<unsigned int> > ctrFreq = CreateObject <std::vector<unsigned int> > ();


  NS_LOG_UNCOND("Assigning...");
  std::string a="a";
  Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject <ListPositionAllocator>();
  Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject <ListPositionAllocator>();
  ReadData(a, ssNum, bsNum, ssPositionAlloc, bsPositionAlloc, ssAntennaHeight, ssTxGain, ssRxGain, ssTxPower, bsAntennaHeight, bsTxGain, bsRxGain, bsTxPower, ctrFreq);
  ssNodes.Create (ssNum);
  ssMobility.SetPositionAllocator(ssPositionAlloc);
  ssMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  ssMobility.Install(ssNodes);
  NS_LOG_UNCOND("Done assigning.");


  bsNodes.Create (bsNum);
  bsMobility.SetPositionAllocator(bsPositionAlloc);
  bsMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
  bsMobility.Install(bsNodes);
  NodeContainer firstBss(bsNodes.Get(0), bsNodes.Get(1)), secondBss (bsNodes.Get(1), bsNodes.Get(2)), thirdBss(bsNodes.Get(0), bsNodes.Get(2));

  PointToPointHelper ptop[3];
  for (int i = 0;i < 3; i++)
  {
	ptop[i].SetDeviceAttribute ("DataRate", StringValue ("52Mbps"));
	ptop[i].SetChannelAttribute ("Delay", StringValue ("2ms"));
  }
  NetDeviceContainer ptopDevices[3];
  ptopDevices[0] = ptop[0].Install(firstBss);
  ptopDevices[1] = ptop[1].Install(secondBss);
  ptopDevices[2] = ptop[2].Install(thirdBss);

  WimaxHelper wimax;
  //double AntennaHeight[1]={3};

  for (unsigned int i=0; i < ssNodes.GetN(); i++)
	  ssAntennaHeight.push_back(3);



  for (unsigned int i=0; i< bsNodes.GetN(); i++)
	  bsAntennaHeight.push_back(50);

  NetDeviceContainer ssDevs, bsDevs;

  ssDevs = wimax.Install (ssNodes,WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
		  	  	  	  	  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
                          scheduler,&ssAntennaHeight,&ssTxGain,&ssRxGain,&ssTxPower);

  bsDevs = wimax.Install (bsNodes, WimaxHelper::DEVICE_TYPE_BASE_STATION,
		  	  	  	  	  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
		  	  	  	  	  scheduler,&bsAntennaHeight, &ssTxGain,&ssRxGain,&ssTxPower, &ctrFreq);

  Ptr<SubscriberStationNetDevice> ss[ssNum];

  for (int i = 0; i < ssNum; i++)
    {
      ss[i] = ssDevs.Get (i)->GetObject<SubscriberStationNetDevice> ();
      ss[i]->SetModulationType (modulation);
    }

  Ptr<BaseStationNetDevice> bs;

  bs = bsDevs.Get (0)->GetObject<BaseStationNetDevice> ();

  InternetStackHelper stack;
  stack.Install (bsNodes);
  stack.Install (ssNodes);

  Ipv4AddressHelper address;
  address.SetBase ("10.1.1.0", "255.255.255.0");

  Ipv4InterfaceContainer SSinterfaces = address.Assign (ssDevs);
  Ipv4InterfaceContainer BSinterface = address.Assign (bsDevs);

  NS_LOG_INFO ("Starting simulation.....");
  Simulator::Run ();

  for (int i=0; i < ssNum; i++)
  {
	  ss[i] = 0;
  }

  bs = 0;

  Simulator::Destroy ();
  NS_LOG_INFO ("Done.");

  return 0;
}
