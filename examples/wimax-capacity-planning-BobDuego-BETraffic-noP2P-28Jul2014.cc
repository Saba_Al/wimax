/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */

//
// Default network topology includes a base station (BS) and 2
// subscriber station (SS).

//      +-----+
//      | SS0 |
//      +-----+
//     10.1.1.1
//      -------
//        ((*))
//
//                  10.1.1.7
//               +------------+
//               |Base Station| ==((*))
//               +------------+
//
//        ((*))
//       -------
//      10.1.1.2
//       +-----+
//       | SS1 |
//       +-----+

#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>
#include "ns3/CSV_Reading.h"
#include <string>     // std::string, std::to_string

NS_LOG_COMPONENT_DEFINE ("WimaxCapacityExample");

using namespace std;
using namespace ns3;

//TCP Traffic
//Uplink
int Uplink_TCP_Traffic(uint32_t fid, double stTime, WimaxHelper &wimax,
		int port, int duration,Ipv4InterfaceContainer bsIf, Ipv4InterfaceContainer ssIf,
		NodeContainer bsNodes, NodeContainer ssNodes , NetDeviceContainer ss,
		std::string on_time, std::string off_time,
		std::map <const char*,ServiceFlow::SchedulingType> serviceProfile,
		vector<Node_Data> CPE_Alcona, std::string data_rate,
		uint32_t pktSize, uint32_t maxBytes, int32_t cpe_num = -1) //int numUsers,std::vector<Ipv4InterfaceContainer>
	{
//	port+=10;
	port=50000;
	//Collect an adjacency list of nodes for the p2p topology
	// std::vector<NodeContainer> MonnodeAdjacencyList (numUsers);
	// for(uint32_t i=0; i<MonnodeAdjacencyList.size (); ++i)
	// {
	// MonnodeAdjacencyList[i] = NodeContainer (bsNodes, ssNodes.Get (i));
	// }
//	OnOffHelper clientHelper ("ns3::TcpSocketFactory", Address ());
//	NS_LOG_UNCOND("bsNum=" << bsNum);
//	NS_LOG_UNCOND("---" << allIfs.GetAddress(bsNum));
	OnOffHelper clientHelper ("ns3::TcpSocketFactory",Ipv4Address::GetAny());
			//InetSocketAddress (bsIf.GetAddress(0), port));
			//InetSocketAddress (allIfs.GetAddress(bsNum), port));
	clientHelper.SetAttribute("OnTime", StringValue (on_time));
	clientHelper.SetAttribute("OffTime",  StringValue (off_time));
	clientHelper.SetAttribute("FlowId", UintegerValue (fid));
	clientHelper.SetAttribute("MaxBytes", UintegerValue (maxBytes));
	//clientHelper.SetConstantRate(DataRate (data_rate));
	clientHelper.SetAttribute("DataRate",DataRateValue(10000));
	clientHelper.SetAttribute ("PacketSize", UintegerValue (128));//pktSize

	Address sinkLocalAddress (InetSocketAddress (ssIf.GetAddress(0), port));
	PacketSinkHelper sinkHelperUL("ns3::TcpSocketFactory", sinkLocalAddress);



	/*
    BulkSendHelper ulClientHelper ("ns3::TcpSocketFactory",
                                        InetSocketAddress (remoteHostAddr, ulPort));
	ulClientHelper.SetAttribute ("MaxBytes", UintegerValue (0));
	clientApps.Add (ulClientHelper.Install (ue));
	PacketSinkHelper ulPacketSinkHelper ("ns3::TcpSocketFactory",
									  InetSocketAddress (Ipv4Address::GetAny (), ulPort));
	serverApps.Add (ulPacketSinkHelper.Install (remoteHost));
	*/


	ApplicationContainer clientApps; // = new ApplicationContainer[numUsers];

	for(uint32_t i=0; i< ssNodes.GetN (); ++i)
	{

		if (cpe_num == -1 || cpe_num == (int32_t)i)
		{
			ApplicationContainer sinkApp = sinkHelperUL.Install (ssNodes.Get(i));
			sinkApp.Start (Seconds (0.0));//stTime
			sinkApp.Stop (Seconds (duration-2.0));//stTime+

//			AddressValue remoteAddress (InetSocketAddress (interfaceAdjacencyList[i].GetAddress(0), port));//.GetAddress (0)
			//NS_LOG_UNCOND("bsIf.GetAddress(0)="<<bsIf.GetAddress(0));
			AddressValue remoteAddress (InetSocketAddress (ssIf.GetAddress(i), port));
			clientHelper.SetAttribute ("Remote", remoteAddress);
			cout << "killu" << endl;
			// clientHelper.SetAttribute ("FlowId", UintegerValue (i));
/*			Ipv4StaticRoutingHelper ipv4RoutingHelper;
			Ptr<Ipv4StaticRouting> sr =
			          	ipv4RoutingHelper.GetStaticRouting (ssNodes.Get (i)->GetObject<Ipv4> ());
			sr->SetDefaultRoute (allIfs.GetAddress(bsNum),0);
			sr = ipv4RoutingHelper.GetStaticRouting (bsNodes.Get(0)->GetObject<Ipv4> ());
			NS_LOG_UNCOND("establishing static route:" << allIfs.GetAddress(firstSSNum+i)<<"-->"<<allIfs.GetAddress(bsNum));
			sr->AddNetworkRouteTo (allIfs.GetAddress(firstSSNum+i),
					Ipv4Mask ("255.255.255.0"), 0);
			//NS_ASSERT(false);
*/
			clientApps.Add(clientHelper.Install (bsNodes.Get (0)));// MonclientApps.Add
			IpcsClassifierRecord UlClassifier = IpcsClassifierRecord(Ipv4Address ("0.0.0.0"),
					Ipv4Mask ("0.0.0.0"),
				ssIf.GetAddress(i),
				Ipv4Mask ("255.255.255.0"),

				0,
				65000,
				port,
				port,
				6,
				1
			);

			ServiceFlow ul_sf = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN,
					ServiceFlow::SF_TYPE_BE ,UlClassifier);//ServiceFlow::SF_DIRECTION_UP,/*serviceProfile[(const char*)CPE_Alcona.at(i).location.c_str()]*/
			ss.Get(i)-> GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (ul_sf);

			//UniformVariable t;
		}
	}//for i
	clientApps.Start (Seconds (0.0)); //5+stTime)); //t.GetValue()));
	clientApps.Stop (Seconds (duration-3.0));//+stTime
	return port;
} //end Uplink_TCP_Traffic

/*int Uplink_TCP_Traffic_Polling(uint32_t fid, double stTime, WimaxHelper &wimax, int port,int duration, std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList,
NodeContainer bsNodes, NodeContainer ssNodes , NetDeviceContainer ss, std::string on_time, std::string off_time,
std::map <const char*,ServiceFlow::SchedulingType> serviceProfile, vector<Node_Data> CPE_Alcona,
uint32_t pktSize, int32_t cpe_num=-1) //int numUsers,std::vector<Ipv4InterfaceContainer>
{
	port+=10;
	//Collect an adjacency list of nodes for the p2p topology
	// std::vector<NodeContainer> MonnodeAdjacencyList (numUsers);
	// for(uint32_t i=0; i<MonnodeAdjacencyList.size (); ++i)
	// {
	// MonnodeAdjacencyList[i] = NodeContainer (bsNodes, ssNodes.Get (i));
	// }
	OnOffHelper clientHelper ("ns3::TcpSocketFactory", Address ());
	clientHelper.SetAttribute
	("OnTime", StringValue (on_time));
	clientHelper.SetAttribute
	("OffTime", StringValue (off_time));//off_time

	clientHelper.SetAttribute("FlowId", UintegerValue (fid));
	//clientHelper.SetAttribute("MaxBytes", UintegerValue (pktSize));
	//clientHelper.SetConstantRate (DataRate (data_rate));
	clientHelper.SetAttribute ("PacketSize", UintegerValue (pktSize));

	Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
	PacketSinkHelper sinkHelperUL("ns3::TcpSocketFactory", sinkLocalAddress);
	ApplicationContainer sinkApp = sinkHelperUL.Install (bsNodes);

//	sinkApp.Start (Seconds (1.0));
//	sinkApp.Stop (Seconds (duration));
	sinkApp.Start (Seconds (stTime));
	sinkApp.Stop (Seconds (stTime+duration));


	//normally wouldn't need a loop here but the server IP address is different
	//on each p2p subnet
	// ApplicationContainer MonclientApps;
	ApplicationContainer clientApps; // = new ApplicationContainer[numUsers];
	for(uint32_t i=0; i< ssNodes.GetN (); ++i)
	{
		if (cpe_num == -1 || cpe_num == (int32_t)i)
		{

			AddressValue remoteAddress (InetSocketAddress (interfaceAdjacencyList[i].GetAddress(0), port));//.GetAddress (0)
			clientHelper.SetAttribute ("Remote", remoteAddress);
			// clientHelper.SetAttribute ("FlowId", UintegerValue (i));
			clientApps.Add(clientHelper.Install (ssNodes.Get (i)));// MonclientApps.Add
			IpcsClassifierRecord UlClassifier = IpcsClassifierRecord(interfaceAdjacencyList[i].GetAddress(1),//.GetAddress (1)
			Ipv4Mask ("255.255.255.252"),
			Ipv4Address ("0.0.0.0"), //BSinterface.GetAddress (0),
			Ipv4Mask ("0.0.0.0"),
			0,
			65000,
			port,
			port,
			17,
			1);

			ServiceFlow UlServiceFlowUgs = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP, serviceProfile[(const char*)CPE_Alcona.at(i).location.c_str()] ,UlClassifier);
			ss.Get(i)-> GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (UlServiceFlowUgs);
			//UniformVariable t;
		}
	}//for i
	clientApps.Start (Seconds (stTime)); //t.GetValue()));
	clientApps.Stop (Seconds (stTime+duration));
	return port;
} //end Uplink_TCP_Traffic_Polling
*/

//Uplink Bulk:
/*
int Uplink_TCP_Bulk_Traffic(uint32_t fid, double stTime, WimaxHelper &wimax, int port,int duration, std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList,
NodeContainer bsNodes, NodeContainer ssNodes , NetDeviceContainer ss, std::map <const char*,ServiceFlow::SchedulingType> serviceProfile, vector<Node_Data> CPE_Alcona,
uint32_t send_size, uint32_t maxBytes, int32_t cpe_num=-1) //int numUsers,std::vector<Ipv4InterfaceContainer>
{
	port+=10;
	//Collect an adjacency list of nodes for the p2p topology
	// std::vector<NodeContainer> MonnodeAdjacencyList (numUsers);
	// for(uint32_t i=0; i<MonnodeAdjacencyList.size (); ++i)
	// {
	// MonnodeAdjacencyList[i] = NodeContainer (bsNodes, ssNodes.Get (i));
	// }


	BulkSendHelper clientHelper ("ns3::TcpSocketFactory", Address());
	// Set the amount of data to send in bytes. Zero is unlimited.
	clientHelper.SetAttribute("FlowId", UintegerValue (fid));
	clientHelper.SetAttribute ("MaxBytes", UintegerValue (maxBytes));
	//clientHelper.SetAttribute ("SendSize", UintegerValue (send_size));

	Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
	PacketSinkHelper sinkHelperUL("ns3::TcpSocketFactory", sinkLocalAddress);
	ApplicationContainer sinkApp = sinkHelperUL.Install (bsNodes);

//	sinkApp.Start (Seconds (1.0));
//	sinkApp.Stop (Seconds (duration));
	sinkApp.Start (Seconds (stTime));
	sinkApp.Stop (Seconds (stTime+duration));


	//normally wouldn't need a loop here but the server IP address is different
	//on each p2p subnet
	// ApplicationContainer MonclientApps;
	ApplicationContainer clientApps; // = new ApplicationContainer[numUsers];
	for(uint32_t i=0; i< ssNodes.GetN (); ++i)
	{
		if (cpe_num == -1 || cpe_num == (int32_t)i)
		{

			AddressValue remoteAddress (InetSocketAddress (interfaceAdjacencyList[i].GetAddress(0), port));//.GetAddress (0)
			clientHelper.SetAttribute ("Remote", remoteAddress);
			// clientHelper.SetAttribute ("FlowId", UintegerValue (i));
			clientApps.Add(clientHelper.Install (ssNodes.Get (i)));// MonclientApps.Add
			IpcsClassifierRecord UlClassifier = IpcsClassifierRecord(interfaceAdjacencyList[i].GetAddress(1),//.GetAddress (1)
			Ipv4Mask ("255.255.255.252"),
			Ipv4Address ("0.0.0.0"), //BSinterface.GetAddress (0),
			Ipv4Mask ("0.0.0.0"),
			0,
			65000,
			port,
			port,
			17,
			1);

			ServiceFlow UlServiceFlowUgs = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP, serviceProfile[(const char*)CPE_Alcona.at(i).location.c_str()] ,UlClassifier);
			ss.Get(i)-> GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (UlServiceFlowUgs);
			//UniformVariable t;
		}
	}//for i
	clientApps.Start (Seconds (stTime)); //t.GetValue()));
	clientApps.Stop (Seconds (stTime+duration));
	return port;
} //end Uplink_TCP_Bulk_Traffic
*/


//Downlink:
/*
int Downlink_TCP_Traffic(uint32_t fid, double stTime, WimaxHelper &wimax, int port,int duration, std::vector<Ipv4InterfaceContainer> interfaceAdjacencyList,
NodeContainer bsNodes, NodeContainer ssNodes , NetDeviceContainer ss, std::string on_time, std::string off_time,
std::map <const char*,ServiceFlow::SchedulingType> serviceProfile, vector<Node_Data> CPE_Alcona,
std::string data_rate, uint32_t pktSize, int32_t cpe_num) //int numUsers,std::vector<Ipv4InterfaceContainer>
{
	port+=10;
	//Collect an adjacency list of nodes for the p2p topology
	// std::vector<NodeContainer> MonnodeAdjacencyList (numUsers);
	// for(uint32_t i=0; i<MonnodeAdjacencyList.size (); ++i)
	// {
	// MonnodeAdjacencyList[i] = NodeContainer (bsNodes, ssNodes.Get (i));
	// }
	OnOffHelper clientHelper ("ns3::TcpSocketFactory", Address ());
	clientHelper.SetAttribute
	("OnTime", StringValue (on_time));
	clientHelper.SetAttribute
	("OffTime", StringValue (off_time));//off_time
	clientHelper.SetAttribute("FlowId", UintegerValue (fid));
	//clientHelper.SetAttribute("MaxBytes", UintegerValue (pktSize));
	clientHelper.SetConstantRate (DataRate (data_rate));
	clientHelper.SetAttribute ("PacketSize", UintegerValue (pktSize));

	Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
	PacketSinkHelper sinkHelperUL("ns3::TcpSocketFactory", sinkLocalAddress);
	ApplicationContainer sinkApp = sinkHelperUL.Install (ssNodes);

//	sinkApp.Start (Seconds (1.0));
//	sinkApp.Stop (Seconds (duration));
	sinkApp.Start (Seconds (stTime));
	sinkApp.Stop (Seconds (stTime+duration));


	//normally wouldn't need a loop here but the server IP address is different
	//on each p2p subnet
	// ApplicationContainer MonclientApps;
	ApplicationContainer clientApps; // = new ApplicationContainer[numUsers];
	for(uint32_t i=0; i< ssNodes.GetN (); ++i)
	{
		if (cpe_num == -1 || cpe_num == (int32_t)i)
		{
			AddressValue remoteAddress (InetSocketAddress (interfaceAdjacencyList[i].GetAddress(1), port));//.GetAddress (0)
			clientHelper.SetAttribute ("Remote", remoteAddress);
			// clientHelper.SetAttribute ("FlowId", UintegerValue (i));
			clientApps.Add(clientHelper.Install (bsNodes));// MonclientApps.Add
			IpcsClassifierRecord DlClassifier = IpcsClassifierRecord(interfaceAdjacencyList[i].GetAddress(0),//.GetAddress (1)
			Ipv4Mask ("255.255.255.252"),
			Ipv4Address ("0.0.0.0"), //BSinterface.GetAddress (0),
			Ipv4Mask ("0.0.0.0"),
			0,
			65000,
			port,
			port,
			17,
			1);

			ServiceFlow DlServiceFlowUgs = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN, serviceProfile[(const char*)CPE_Alcona.at(i).location.c_str()] ,DlClassifier);
			ss.Get(i)-> GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (DlServiceFlowUgs);
			//UniformVariable t;
		}
	}//for i
	clientApps.Start (Seconds (stTime)); //t.GetValue()));
	clientApps.Stop (Seconds (stTime+duration));
	return port;
}//end Downlink_TCP_Traffic
*/
//void ReadData (std::string filename, int &ssNum, int &bsNum, Ptr<ListPositionAllocator> ssPositionAlloc, Ptr<ListPositionAllocator> bsPositionAlloc,


int main (int argc, char *argv[])
{
  int port = 100;
  int  schedType = 0, simulation_time = 10, dlIntegStTime = 2.0, duration = 10;

  LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
  //LogComponentEnable ("BulkSendApplication", LOG_LEVEL_ALL); //TCP
  LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink
  //LogComponentEnable ("Cost231PropagationLossModel", LOG_LEVEL_ALL); //TCP Sink
  //LogComponentEnable ("Ipv4AddressHelper",LOG_LEVEL_ALL);
  //LogComponentEnable ("simpleOfdmWimaxChannel",LOG_LEVEL_ALL);
  //LogComponentEnable ("WimaxConnection",LOG_LEVEL_ALL);
  //LogComponentEnable ("SubscriberStationNetDevice",LOG_LEVEL_ALL);
  //LogComponentEnable ("BaseStationNetDevice",LOG_LEVEL_ALL);
  //LogComponentEnable ("SSLinkManager",LOG_LEVEL_ALL);
  WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
  WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;

  vector<Node_Data> BS_Info;
  vector<Node_Data> CPEs_Info;

  Xml_Read_Freq_Compatible("20140519PTMP LakeshoreWPCP.xml", BS_Info, CPEs_Info);
  NS_LOG_UNCOND("BS_Info.size()=" << BS_Info.size()<< "," << "CPEs_Info.size()=" << CPEs_Info.size());
 // NS_ASSERT(false);
  for (unsigned int i=0; i < CPEs_Info.size(); i++)
  {
	  NS_LOG_UNCOND( i<< "Name:" << CPEs_Info.at(i).location << "x,y,z" << CPEs_Info.at(i).latitude << "," << CPEs_Info.at(i).longtitude << "," << CPEs_Info.at(i).TxAntennaHeight);
  }

  CommandLine cmd;
  cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
  cmd.Parse (argc, argv);

  NS_LOG_UNCOND("Start...");
  switch (schedType)
    {
    case 0:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
      break;
    case 1:
      scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
      break;
    case 2:
      scheduler = WimaxHelper::SCHED_TYPE_RTPS;
      break;
    default:
      scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
    }

  NodeContainer ssNodesAlcona[3];
  vector<Node_Data> CPEs_Alcona[3];
  WimaxHelper wimaxAlcona[3];
  InternetStackHelper stack;
  Ipv4AddressHelper address;
  NetDeviceContainer ssDevsAlcona[3];

  Ipv4InterfaceContainer SSinterface_Alcona[3];

  //NetDeviceContainer allNetDevs;


  address.SetBase ("10.1.1.0", "255.255.255.0");
  NS_LOG_UNCOND("Sorting CPEs based on their sectNum...");
  for (unsigned int i=0; i<CPEs_Info.size(); i++)
  {
	  CPEs_Alcona[CPEs_Info.at(i).sectNum-1].push_back(CPEs_Info.at(i));
  }

  //int end_nums[4];
  //end_nums[0]=0;

  NS_LOG_UNCOND("Assigning CPEs");
  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  unsigned int num = CPEs_Alcona[sectNum].size();
	  ssNodesAlcona[sectNum].Create (num);
//	  cout << "end_nums[" << sectNum+1 << "]=" << end_nums[sectNum+1] <<endl;
	  vector <double> ssAntennaHeightVector;
	  vector <string> ssNodeNameVector;
	  vector <double> ssTxFreqVector;
	  Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject <ListPositionAllocator>();
	  for (unsigned int j=0;j< num; j++)
	  {
	  	  ssPositionAlloc ->Add(Vector(CPEs_Alcona[sectNum].at(j).latitude,
	  			  CPEs_Alcona[sectNum].at(j).longtitude,
	  			  (CPEs_Alcona[sectNum].at(j).elevation)/1000
	  	  	  	  ));
	  	  ssAntennaHeightVector.push_back(CPEs_Alcona[sectNum].at(j).TxAntennaHeight);
	  	  ssNodeNameVector.push_back(CPEs_Alcona[sectNum].at(j).location);
	  	  ssTxFreqVector.push_back(CPEs_Alcona[sectNum].at(j).TxFreq);
	  }

	  MobilityHelper ssMobility;
	  ssMobility.SetPositionAllocator(ssPositionAlloc);
	  ssMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  ssMobility.m_antennaHeightVector = ssAntennaHeightVector;
	  ssMobility.m_nodeNameVector = ssNodeNameVector;
	  ssMobility.m_txFreqVector = ssTxFreqVector;
	  ssMobility.Install(ssNodesAlcona[sectNum]);

	  //print
	  for (unsigned int k=0;k<num;k++)
	  {
		  Ptr<MobilityModel> position = ssNodesAlcona[sectNum].Get(k)->GetObject<MobilityModel> ();
		  NS_ASSERT (position != 0);
		  Vector pos = position->GetPosition ();
		  NS_LOG_UNCOND( "CPE#" << k << " at location " << CPEs_Alcona[sectNum].at(k).location << " TX Freq " << CPEs_Alcona[sectNum].at(k).TxFreq << " at Sector#" << CPEs_Alcona[sectNum].at(k).sectNum << ":, x=" << pos.x << ", y=" << pos.y << ", z=" << pos.z << ", ant=" << position->m_antennaHeight );
	  } //for k

	  //Building SS devices
	  //channel = CreateObject<SimpleOfdmWimaxChannel> ();
	  //channel->SetPropagationModel (SimpleOfdmWimaxChannel::COST231_PROPAGATION);
	  ssDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install(	ssNodesAlcona[sectNum],
			  	  	  	  	  	  	  	  	  	  	  	  	WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
			  	  	  	  	  	  	  	  	  	  	  	    WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
			  	  	  	  	  	  	  	  	  	  	  	    scheduler, &CPEs_Alcona[sectNum], true);
	  //allNetDevs.Add(ssDevsAlcona[sectNum]);
	  /*ObjectFactory queueFactory;
	  vector<Ptr<Queue> > queue = queueFactory.Create<Queue> ();
	  allNetDevs.Get(0)->SetQueue (queue);*/

	 // end_nums[sectNum+1] = allNetDevs.GetN();

	  //set modulation and activate loss
	  for (unsigned int j=0;j< num; j++)
	  {
		  ssDevsAlcona[sectNum].Get(j)->GetObject<SubscriberStationNetDevice> ()->SetModulationType (modulation);
		  Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(ssDevsAlcona[sectNum].Get(0)->GetObject<SubscriberStationNetDevice>()->GetPhy());
		  ssPhy->ActivateLoss(true);
	  }
	  //install IP stack on all ss nodes in this sector.
	  stack.Install (ssNodesAlcona[sectNum]);
	  //address assignment for ss nodes: (added Jul. 25, 2014)
	  SSinterface_Alcona[sectNum] = address.Assign (ssDevsAlcona[sectNum]);
  } // for sectNum

  /*
  std::vector<NodeContainer> AdjacencyList[3];
  for (int sectNum =0; sectNum < 3; sectNum++)
	  AdjacencyList[sectNum] = std::vector<NodeContainer> (ssNodesAlcona[sectNum].GetN());
  */
  NS_LOG_UNCOND("Done assigning SSNodesAlcona, starting BS assignments...");

  vector<Node_Data> BS_Alcona[3];
  Ipv4InterfaceContainer BSinterface_Alcona[3];
  for (unsigned int i=0; i<3; i++)
  {
	  NS_LOG_UNCOND("i="<<i);
	  BS_Alcona[i].push_back(BS_Info[i]);
  }
  NodeContainer bsNodesAlcona[3];
  NetDeviceContainer bsDevsAlcona[3];

  for (unsigned int sectNum=0; sectNum<3; sectNum++)
  {
	  NS_LOG_UNCOND("sectNum="<<sectNum);
	  bsNodesAlcona[sectNum].Create (1);
	  vector <double> bsAntennaHeightVector;
	  vector <string> bsNodeNameVector;
	  vector <double> bsTxFreqVector;
	  Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject <ListPositionAllocator>();
	  bsPositionAlloc ->Add(Vector(BS_Info.at(sectNum).latitude, BS_Info.at(sectNum).longtitude, (BS_Info.at(sectNum).elevation)/1000));
	  NS_LOG_UNCOND("here 1");
	  bsAntennaHeightVector.push_back(BS_Info.at(sectNum).TxAntennaHeight);
	  bsNodeNameVector.push_back(BS_Info.at(sectNum).location);
	  bsTxFreqVector.push_back(BS_Info.at(sectNum).TxFreq);
	  MobilityHelper bsMobility;
	  bsMobility.SetPositionAllocator(bsPositionAlloc);
	  bsMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  bsMobility.m_antennaHeightVector = bsAntennaHeightVector;
	  bsMobility.m_nodeNameVector = bsNodeNameVector;
	  bsMobility.m_txFreqVector = bsTxFreqVector;
	  bsMobility.Install(bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("here 2");
	  bsDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install (bsNodesAlcona[sectNum].Get(0), WimaxHelper::DEVICE_TYPE_BASE_STATION,
	 		  	  	  	  	  WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
	 		  	  	  	  	  scheduler, &BS_Alcona[sectNum], true);

	  //allNetDevs.Add(bsDevsAlcona[sectNum]);

	  NS_LOG_UNCOND("here 3");
	  bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->
			  SetDuplex(BS_Alcona[sectNum].at(0).TxFreq,BS_Alcona[sectNum].at(0).RxFreq);
	  bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->SetChannelBandwidth(10e2);

	  Ptr<SimpleOfdmWimaxPhy> bsPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy());
	  bsPhy->ActivateLoss(true);

	  NS_LOG_UNCOND("Assigning IP to bs#"<<sectNum);
	  stack.Install (bsNodesAlcona[sectNum]);
	  NS_LOG_UNCOND("Building the BS Interface");
	  BSinterface_Alcona[sectNum] = address.Assign (bsDevsAlcona[sectNum]);
  } // for base station Alcona sectNum

  NS_LOG_UNCOND("Done assigning BSNodesAlcona.");


  //Ipv4InterfaceContainer allIfs= address.Assign(allNetDevs);
  //Turn on global static routing
   //Ipv4GlobalRoutingHelper::PopulateRoutingTables ();
  //Making Adjacency Lists
/*
	for (int sectNum =0; sectNum < 3; sectNum++)
	{
		for(uint32_t i=0; i<AdjacencyList[sectNum].size (); ++i)
			{
				AdjacencyList[sectNum][i] = NodeContainer (bsNodesAlcona[sectNum], ssNodesAlcona[sectNum].Get (i));
			}
	}


	PointToPointHelper p2p;
	p2p.SetDeviceAttribute ("DataRate", StringValue ("0.12Mbps"));
	std::vector<NetDeviceContainer> deviceAdjacencyList[3];
	for (int sectNum=0; sectNum<3; sectNum++)
		deviceAdjacencyList[sectNum] = std::vector<NetDeviceContainer> (ssNodesAlcona[sectNum].GetN());
	for (int sectNum=0; sectNum<3; sectNum++)
	{
		for(uint32_t i=0; i<AdjacencyList[sectNum].size (); ++i)
		{
			deviceAdjacencyList[sectNum][i] = p2p.Install (AdjacencyList[sectNum][i]);
		}
	}
	*/
/*
	int created_nodes = 1;
	std::vector<Ipv4InterfaceContainer>  interfaceAdjacencyList [3];
	for (int sectNum=0; sectNum<3; sectNum++)
		interfaceAdjacencyList[sectNum] = std::vector<Ipv4InterfaceContainer> (ssNodesAlcona[sectNum].GetN());

	Ipv4AddressHelper ipv4;

	for (int sectNum=0; sectNum<3; sectNum++)
	{
		for( uint32_t i=0; i<interfaceAdjacencyList[sectNum].size (); ++i)
		{
			NS_LOG_UNCOND("C#"<<i);
			std::ostringstream subnet;
			int cc,dd;
			dd = ((i+1+created_nodes)%62) +1; //first 6 digit of dd
			cc = (i+1+created_nodes-dd+1)/62 + 1; // created_nodes
			dd *= 4;
			subnet << "10.1."<< cc << "." << dd ;
			NS_LOG_UNCOND("cn: " << created_nodes << ", i: " << i << ", cc: " << cc << ", dd:" << dd);
			ipv4.SetBase (subnet.str ().c_str (), "255.255.255.252");
			interfaceAdjacencyList[sectNum][i] = ipv4.Assign (deviceAdjacencyList[sectNum][i]);
		}
		created_nodes  = created_nodes  +   interfaceAdjacencyList[sectNum].size ();
	}
	*/

  //### Traffic Generation

  //### Uplink Traffic-TCP--integrity poll response
  //###
  duration = simulation_time;
  std::string on_time = "ns3::ConstantRandomVariable[Constant=30.0]";
  std::string off_time = "ns3::ConstantRandomVariable[Constant=0.0]";
  uint32_t fid = 0;
  //ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_UGS;

  double unit_data_rate_per_rtu = 0.129; // kb/s
  std::string data_rate; // = "0.129kb/s";// Data rate here needs to be updated according to the number of devices attached to one device
  uint32_t pktSize = 242; //256;  //242 is for integrity polling, 256 is fro CPE traffic
  double integStTime = dlIntegStTime + 0.008 - CPEs_Info.size()*0.033;
  uint32_t maxBytes = 0;
  //std::map <const char*,ServiceFlow::SchedulingType> serviceProfile = CSV_Reading ("20140625 XML_SCADA_MatchSiteNames3Col.csv",CPEs_Info);
  std::map <const char*,double> numRTUs;
  std::map <const char*,ServiceFlow::SchedulingType> serviceProfile =
		  CSV_Reading("20140625 XML_SCADA_MatchSiteNamesNumRTUs.csv",CPEs_Info,numRTUs);
  for (uint j=0; j<CPEs_Info.size(); j++)
  {
	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "has service flow of type " << serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()]
	       << ", and owns " <<numRTUs[(const char*)CPEs_Info.at(j).location.c_str()] << " RTUs!" );
	  serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_RTPS;
  }

  for (int sectNum =0; sectNum < 1; sectNum++)//3
  {
	  	  integStTime = integStTime + 0.033;
	  	  for (uint32_t cpe_num=0;cpe_num < 1;cpe_num++)//ssNodesAlcona[sectNum].GetN()
	  	  {
			  stringstream s;
			  s << unit_data_rate_per_rtu * numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
			  data_rate = s.str();

			  //NS_LOG_UNCOND( "fid integrity poll=" << fid );


			  port = Uplink_TCP_Traffic(fid, integStTime, wimaxAlcona[sectNum], port,
					  duration,
					  BSinterface_Alcona[sectNum],SSinterface_Alcona[sectNum],
					  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] ,
					  ssDevsAlcona[sectNum], on_time, off_time,
					  serviceProfile,CPEs_Alcona[sectNum],
					  data_rate, pktSize, maxBytes, cpe_num
					);
	  	  }
  }

  //### Uplink Traffic-TCP--BE Traffic
   //###
  /*
   duration = simulation_time;
   on_time = "ns3::ConstantRandomVariable[Constant=1]";
   off_time = "ns3::ConstantRandomVariable[Constant=0]";
   fid = 1;
   //ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_UGS;
   pktSize = 256;
   unit_data_rate_per_rtu = 0.129; // kb/s
   integStTime = dlIntegStTime + 0.008 - CPEs_Info.size()*0.033;
   maxBytes = 0;
   //std::map <const char*,ServiceFlow::SchedulingType> serviceProfile = CSV_Reading ("20140625 XML_SCADA_MatchSiteNames3Col.csv",CPEs_Info);
   for (uint j=0; j<CPEs_Info.size(); j++)
   {
 	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "has service flow of type " << serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()]
 	       << ", and owns" <<numRTUs[(const char*)CPEs_Info.at(j).location.c_str()] << "RTUs!" );
 	  serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_RTPS;
   }

   for (int sectNum =0; sectNum < 3; sectNum++)
   {
 	  	  integStTime = integStTime + 0.033;
 	  	  for (uint32_t cpe_num=0;cpe_num < 1;cpe_num++) // ssNodesAlcona[sectNum].GetN()
 	  	  {
 			//  stringstream s;
 			//  s << unit_data_rate_per_rtu * numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
 			  data_rate = "300kb/s"; //s.str();

 			  //NS_LOG_UNCOND( "fid integrity poll=" << fid );

 			  port = Uplink_TCP_Traffic(fid, integStTime, wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
 			  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time,
 			  serviceProfile,CPEs_Alcona[sectNum], data_rate, pktSize, maxBytes, cpe_num);
 	  	  }
   }
*/
/*
  //Configuration file
  double mean = 22.0;
  double min = 2.0;
  double max = 45.0;
  fid = 4;

  on_time = "ns3::ConstantRandomVariable[Constant=1]";
  off_time = "ns3::TriangularRandomVariable[Min=200,Mean=300,Max=400]"; //ns3::TriangularRandomVariable[Mean=2,Min=1,Max=3]
  Ptr<TriangularRandomVariable> x = CreateObject<TriangularRandomVariable> ();
  pktSize = 300;
  maxBytes = 170000;
  double value=0;
  int stTime = 0;
  for (uint j=0; j<CPEs_Info.size(); j++)
  	    {
  	  	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "	" << serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()]);
  	  	  serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_BE; //all are RTPS
  	    }
  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < 2;cpe_num++) //ssNodesAlcona[sectNum].GetN()
	  	  	  {
				  x->SetAttribute ("Mean", DoubleValue (mean));
				  x->SetAttribute ("Min", DoubleValue (min));
				  x->SetAttribute ("Max", DoubleValue (max));
				  // The expected value for the mean of the values returned by a
				  // triangularly distributed random variable is equal to mean.
				  value = x->GetValue ();
				  stTime = 5.0;
				  std::stringstream s;
				  s << value* numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
				  duration = 15;
				  stTime =stTime + 0.033;

				  //NS_LOG_UNCOND( "fid conf file=" << fid );

				  port = Uplink_TCP_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
				  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time,
				  serviceProfile,CPEs_Alcona[sectNum], s.str(),  pktSize, maxBytes, cpe_num);
	  	  	  }
  }

	//Configuration File-Second Triangle
	mean = 10.0;
	min = 2.0;
	max = 20.0;
	//uint32_t fid = 2;
	x->SetAttribute ("Mean", DoubleValue (mean));
	x->SetAttribute ("Min", DoubleValue (min));
	x->SetAttribute ("Max", DoubleValue (max));
	// The expected value for the mean of the values returned by a
	// triangularly distributed random variable is equal to mean.
	duration = 3;
	for (int sectNum =0; sectNum < 3; sectNum++)
	{
		for (uint32_t cpe_num=0;cpe_num < ssNodesAlcona[sectNum].GetN();cpe_num++)
		{
			value = x->GetValue ();
			std::stringstream DR;
			DR << value * numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
			stTime =stTime + 0.033;

			//NS_LOG_UNCOND( "fid conf file 2=" << fid);

			port = Uplink_TCP_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
			bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time,
			serviceProfile,CPEs_Alcona[sectNum], DR.str(),  pktSize, maxBytes, cpe_num);
		}
	}

	//Configuration File-Third Triangle
	duration = 3;
	mean = 15.0;
	min = 2.0;
	max = 27.0;
	//fid =3;
	x->SetAttribute ("Mean", DoubleValue (mean));
	x->SetAttribute ("Min", DoubleValue (min));
	x->SetAttribute ("Max", DoubleValue (max));
	for (int sectNum =0; sectNum < 3; sectNum++)
	{
		for (uint32_t cpe_num=0;cpe_num < ssNodesAlcona[sectNum].GetN();cpe_num++)
		{
			// The expected value for the mean of the values returned by a
			// triangularly distributed random variable is equal to mean.
			value = x->GetValue ();
			std::stringstream D_R;
			D_R << value * numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
			stTime =stTime + 0.033;
			//NS_LOG_UNCOND( "fid conf file 3=" << fid);

			port = Uplink_TCP_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
			bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time,
			serviceProfile,CPEs_Alcona[sectNum], D_R.str(),  pktSize, maxBytes, cpe_num);
		}
	}

//----------------------------------------------------------
	//Engineering Access-Type 3-Event Record Upload
	  mean = 24.0;
	  min = 0.0;
	  max = 48.0;
	  fid = 3;
	  pktSize = 1460;
	  maxBytes = 60000;
	  on_time = "ns3::ConstantRandomVariable[Constant=1]";
	  off_time = "ns3::ConstantRandomVariable[Constant=0]";
	  x->SetAttribute ("Mean", DoubleValue (mean));
	  x->SetAttribute ("Min", DoubleValue (min));
	  x->SetAttribute ("Max", DoubleValue (max));
	  // The expected value for the mean of the values returned by a
	  // triangularly distributed random variable is equal to mean.
	  duration = 3;
	  for (uint j=0; j<CPEs_Info.size(); j++)
	    {
	  	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "	" << serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()]);
	  	  serviceProfile[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_BE; //all are RTPS
	    }


	  for (int sectNum =0; sectNum < 3; sectNum++)
	  {
		  for (uint32_t cpe_num=0;cpe_num < 1;cpe_num++)//ssNodesAlcona[sectNum].GetN()
			{
				  std::stringstream s;
				  s.str(std::string());
				  s.clear();
				  value = x->GetValue ();
				  s << value * numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "kb/s";
				  stTime =stTime + 0.1;
				  //NS_LOG_UNCOND( "fid EA1=" << fid );

				  port = Uplink_TCP_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration, interfaceAdjacencyList[sectNum],
				  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time, off_time,
				  serviceProfile,CPEs_Alcona[sectNum], s.str(),  pktSize, maxBytes, cpe_num);
			}
	  }
*/
  //### Downlink Traffic-TCP--Integrity polling DNP3 Master sends 72 bytes to RTU
  //###
  /*
  double duration_dl = simulation_time;
  fid = 5;
  double stTime = 5.0;
  std::string on_time_dl = "ns3::ConstantRandomVariable[Constant=1]";
  std::string off_time_dl = "ns3::ConstantRandomVariable[Constant=0]";
  //ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_UGS;
  double unit_data_rate_dl_per_rtu = 38; //"b/s"
  std::string data_rate_dl; // = "38b/s"; //needs to be changed according to the number of RTUs connected to one CPE, for now it has been considered that 4 devices generate each 9.6bps
  uint32_t pktSizeDL = 72;

  std::map <const char*,ServiceFlow::SchedulingType> serviceProfileDL = CSV_Reading ("20140625 XML_SCADA_MatchSiteNamesNumRTUs.csv",CPEs_Info,numRTUs);
  for (uint j=0; j<CPEs_Info.size(); j++)
  {
	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "	" << serviceProfileDL[(const char*)CPEs_Info.at(j).location.c_str()]);
	  serviceProfileDL[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_RTPS; //all are RTPS
  }

  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < ssNodesAlcona[sectNum].GetN();cpe_num++)
		{
			  dlIntegStTime = dlIntegStTime + 0.033;
			  stringstream s;
			  s << unit_data_rate_dl_per_rtu *
					  numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] << "b/s";
			  data_rate_dl = s.str();
			  //NS_LOG_UNCOND( "fid DL=" << fid );

			  port = Downlink_TCP_Traffic(fid, dlIntegStTime, wimaxAlcona[sectNum], port,duration_dl, interfaceAdjacencyList[sectNum],
			  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] , ssDevsAlcona[sectNum], on_time_dl, off_time_dl,
			  serviceProfileDL,CPEs_Alcona[sectNum], data_rate_dl,  pktSizeDL, cpe_num);
		}
  }
  */
/*
  //Uplink Bulk-Engineering Access Type1
  double duration_bulk = simulation_time;
  maxBytes = 390000;
  uint32_t send_size = 536;
  fid = 1;
  //stTime =2.0;
  std::map <const char*,ServiceFlow::SchedulingType> serviceProfileBulk = CSV_Reading ("20140625 XML_SCADA_MatchSiteNamesNumRTUs.csv",CPEs_Info,numRTUs);
  for (uint j=0; j<CPEs_Info.size(); j++)
  {
	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "	" << serviceProfileBulk[(const char*)CPEs_Info.at(j).location.c_str()] );
	  serviceProfileBulk[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_BE; //all are RTPS
  }

  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < 1;cpe_num++) //ssNodesAlcona[sectNum].GetN()
	  {
		  stTime = stTime + 0.1;
		  //NS_LOG_UNCOND( "fid Bulk1=" << fid);

		  port = Uplink_TCP_Bulk_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration_bulk,
				  interfaceAdjacencyList[sectNum],  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] ,
				  ssDevsAlcona[sectNum], serviceProfileBulk,CPEs_Alcona[sectNum], send_size,
				  maxBytes, cpe_num);//numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()]
	  }
  }

  //Uplink Bulk-Engineering Access Type2
  duration_bulk = simulation_time;
  maxBytes = 460000;
  send_size = 536;
  fid = 2;
  //std::map <const char*,ServiceFlow::SchedulingType> serviceProfileBulk = CSV_Reading ("20140625 XML_SCADA_MatchSiteNames3Col.csv",CPEs_Info);
  for (uint j=0; j<CPEs_Info.size(); j++)
  {
	  NS_LOG_UNCOND( CPEs_Info.at(j).location.c_str() << "	" << serviceProfileBulk[(const char*)CPEs_Info.at(j).location.c_str()]);
	  serviceProfileBulk[(const char*)CPEs_Info.at(j).location.c_str()] = ServiceFlow::SF_TYPE_BE; //all are RTPS
  }

  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < 1;cpe_num++) //ssNodesAlcona[sectNum].GetN()
	  {
		  stTime = stTime + 0.1;
		  //NS_LOG_UNCOND( "fid Bulk2=" << fid);

		  port = Uplink_TCP_Bulk_Traffic(fid, stTime, wimaxAlcona[sectNum], port,duration_bulk,
				  interfaceAdjacencyList[sectNum],  bsNodesAlcona[sectNum], ssNodesAlcona[sectNum] ,
				  ssDevsAlcona[sectNum],  serviceProfileBulk,CPEs_Alcona[sectNum], send_size,
		  maxBytes*numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()], cpe_num);
	  }
  }

*/


  //TODO: Make different helpers for each of the BS and its corresponding SSs.
  //NS_LOG_UNCOND( "size" << ssNodesAlcona[sectNum].GetN());
  NS_LOG_UNCOND ("here 1 stTime=");// << stTime);
  NS_LOG_UNCOND ("Starting simulation.....");
  cout << "number of nodes in sector1" << ssNodesAlcona[1].GetN() << endl;
  //NS_ASSERT(false);
  Simulator::Stop(Seconds(150));
  Simulator::Run ();

//  for (unsigned int i=0; i < CPEs_Info.size(); i++)
//  {
//	  ss[i] = 0;
//  }

  //bs = 0;
  NS_LOG_INFO ("Done.");

  //Print Statistics:
  int reliable_received_all_nodes[FLOW_NUM];
  //ns3::Time latency_all_nodes[FLOW_NUM];
  Time Delays_all_nodes[FLOW_NUM];
  Time ReliableDelays_all_nodes[FLOW_NUM];
  int Recv_all_nodes[FLOW_NUM];
  //int sent_all_nodes[FLOW_NUM];  m_Sent
  for (int flowid=0; flowid<FLOW_NUM; flowid++)
  {
	  reliable_received_all_nodes[flowid] 	= 0;
	  Delays_all_nodes[flowid] 				= Time(0.0);
	  ReliableDelays_all_nodes[flowid] 		= Time(0.0);
	  Recv_all_nodes[flowid] 				= 0;
  }

  //for 0<=flowid<5 (UL) we need to sum over bsNodes:
  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  Ptr<Node> myNode = bsNodesAlcona[sectNum].Get(0);
	  for (int flowid=0; flowid<5; flowid++)
	  {
		  //NS_LOG_UNCOND(sectNum << 	":" << flowid  );
		  reliable_received_all_nodes[flowid] 	+= myNode->reliable_received[flowid];
		  //NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
		  Delays_all_nodes[flowid] 				+= myNode->Delays[flowid];
		  //NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
		  ReliableDelays_all_nodes[flowid] 		+= myNode->ReliableDelays[flowid];
		  //NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
		  Recv_all_nodes[flowid] 				+= myNode->Recv[flowid];
		  //NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
	  }
  }

  //for flowid=5 (DL) we need to sum over all ssNodes:
  int flowid=5;
  for (int sectNum =0; sectNum < 3; sectNum++)
  {
	  for (uint32_t cpe_num=0;cpe_num < ssNodesAlcona[sectNum].GetN();cpe_num++)
	  {
		  Ptr<Node> myNode = ssNodesAlcona[sectNum].Get(cpe_num);
		  //NS_LOG_UNCOND(sectNum << ":" << cpe_num << ":" << flowid  );
		  reliable_received_all_nodes[flowid] 	+= myNode->reliable_received[flowid];
		  //NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
		  Delays_all_nodes[flowid] 				+= myNode->Delays[flowid];
		  //NS_LOG_UNCOND("delays: " << myNode->Delays[flowid].ToDouble(Time::MS) << "," << Delays_all_nodes[flowid]) ;
		  ReliableDelays_all_nodes[flowid] 		+= myNode->ReliableDelays[flowid];
		  //NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid].ToDouble(Time::MS) << "," << ReliableDelays_all_nodes[flowid] );
		  Recv_all_nodes[flowid] 				+= myNode->Recv[flowid];
		  //NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
	 }
  }

  cout << "flow ID\t\treliable rcvd\treliable delay\tall rcvd\tall delay" << endl;
  //cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
  for (int flowid=0; flowid<6; flowid++)
  {
	  cout << flowid << "\t\t" << reliable_received_all_nodes[flowid] << "\t\t" <<
			  ReliableDelays_all_nodes[flowid].ToDouble(Time::MS) / (double) reliable_received_all_nodes[flowid] << "\t\t" <<
			  Recv_all_nodes[flowid] << "\t\t" <<
	  	  	  Delays_all_nodes[flowid].ToDouble(Time::MS) / (double) Recv_all_nodes[flowid] <<  endl;
  }

  int Throughput[3];

    for (int sectNum =0; sectNum < 3; sectNum++)
    {
    	Throughput[sectNum] = 0;
    	for (int flowid=0; flowid<5 ; flowid++)
    	{
    		Throughput[sectNum] += bsNodesAlcona[sectNum].Get(0)->receivedBytes[flowid];
    	}
	  for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
	  {
		  Throughput[sectNum] += ssNodesAlcona[sectNum].Get(i)->receivedBytes[5];
	  }
	  cout <<	 "Throughput" << sectNum << " = " <<   Throughput[sectNum]/130.0 << endl;
    }

    int SentSect[3], RecvSect[3];

      for (int sectNum =0; sectNum < 3; sectNum++)
      {
		SentSect[sectNum] = 0;
		RecvSect[sectNum] = 0;
		//UL Sent
		for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
		{
			for (int flowid=0; flowid<5 ; flowid++)
			{
				SentSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->m_Sent[flowid];
			}
		}
		//UL recv
		for (int flowid=0; flowid<5 ; flowid++)
		{
			RecvSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->Recv[flowid];
		}

		//DL sent
		SentSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->m_Sent[5];
		//DL recv
		for (uint32_t i =0; i < ssNodesAlcona[sectNum].GetN(); i++)
		{
		  	RecvSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->Recv[5];
		}
		cout <<	 "SentNum" << sectNum << " = " <<  SentSect[sectNum]  << "\t RecvNum" << RecvSect[sectNum]  << endl;
		cout << "Loss Ratio" << RecvSect[sectNum]/float(SentSect[sectNum]) << endl;
      }














  Simulator::Destroy ();

  return 0;
}
