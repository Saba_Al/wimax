/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>
#include "ns3/CSV_Reading.h"
#include <string>     // std::string, std::to_string

#define MAX_APPS 50
#define FLOW_NUM 10
NS_LOG_COMPONENT_DEFINE ("WimaxCapacityExample");

using namespace std;
using namespace ns3;

//TCP Traffic
//Uplink
int setup_tcp(int nbSS, NodeContainer ssNodes, Ipv4InterfaceContainer SSinterfaces,
		NodeContainer bsNode, Ipv4InterfaceContainer BSinterface,
				                 double duration, std::string on_time, std::string off_time,
				                 int startPort)
		{
			  PacketSinkHelper *tcpServer[MAX_APPS];
			  ApplicationContainer sinkApps[MAX_APPS];
			  OnOffHelper *onoff[MAX_APPS];
			  ApplicationContainer clientApps[MAX_APPS];
			  int port;
			  int startTime = 6;
			  for (int i = 0; i < nbSS; i++)
			    {
				    port = startPort + (i * 10);
				    port = 50000;
			      // set server port to 100+(i*10)
				    Address localAddress (InetSocketAddress(Ipv4Address::GetAny(),port));
			      tcpServer[i] = new PacketSinkHelper ("ns3::TcpSocketFactory",localAddress);
			      sinkApps[i] = tcpServer[0]->Install (bsNode.Get(0));


			      onoff[i] = new OnOffHelper ("ns3::TcpSocketFactory", Ipv4Address::GetAny());
			      AddressValue remoteAddr(InetSocketAddress(BSinterface.GetAddress(0),port));
			      onoff[i]->SetAttribute("Remote", remoteAddr);
			      onoff[i]->SetAttribute("OnTime", StringValue ("ns3::ConstantRandomVariable[Constant=2.0]"));//on_time
			      onoff[i]->SetAttribute("OffTime",  StringValue ("ns3::ConstantRandomVariable[Constant=0.0]"));//off_time
			      onoff[i]->SetAttribute("DataRate",DataRateValue (DataRate ("1000kb/s")));
			  	  onoff[i]->SetAttribute ("PacketSize", UintegerValue (128));//pktSize
			      clientApps[i] = onoff[i]->Install (ssNodes.Get (i));
			      if (i == 2)
						{
									sinkApps[i].Start(Seconds(0));
									sinkApps[i].Stop (Seconds (320));
									clientApps[i].Start (Seconds (5));
									clientApps[i].Stop (Seconds (50));
						}
						else
						{
									sinkApps[i].Start (Seconds (0));
						      sinkApps[i].Stop (Seconds (320));
						      clientApps[i].Start (Seconds (5));
						      clientApps[i].Stop (Seconds (300));
						}
						startTime += 6;
			    }
			  return port;
		}





int main (int argc, char *argv[])
{
  int  schedType = 0, simulation_time = 340,  duration = 60;

  //LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
  LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink

  WimaxHelper::SchedulerType scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
  WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM16_12;

  CommandLine cmd;
  cmd.AddValue ("scheduler", "type of scheduler to use with the network devices", schedType);
  cmd.Parse (argc, argv);

  NS_LOG_UNCOND("Start...");
  switch (schedType)
	{
	case 0:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
		break;
	case 1:
		scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
		break;
	case 2:
		scheduler = WimaxHelper::SCHED_TYPE_RTPS;
		break;
	default:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
	}

  NodeContainer ssNodes;
  WimaxHelper wimax;
  InternetStackHelper stack;
  Ipv4AddressHelper address;
  NetDeviceContainer ssDevs;
  Ptr<SubscriberStationNetDevice>* ss;

  Ipv4InterfaceContainer SSinterface;

  MobilityHelper mobility;

  NS_LOG_UNCOND("Assigning CPEs");

  	int nbSS=8;
	  ssNodes.Create (nbSS);
	  ssDevs = wimax.Install(	ssNodes,	WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION, WimaxHelper::SIMPLE_PHY_TYPE_OFDM,scheduler);

  	ss = new Ptr<SubscriberStationNetDevice>[nbSS];

	  for (int i = 0; i < nbSS; i++)
	  {
		  ss[i] = ssDevs.Get (i)->GetObject<SubscriberStationNetDevice> ();
		  ss[i]->SetModulationType (modulation);
	  }

	  vector <double> ssAntennaHeightVector;
	  vector <string> ssNodeNameVector;
	  vector <double> ssTxFreqVector;
	  Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject <ListPositionAllocator>();

	  mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
	  		"X",StringValue("0.0"),"Y",StringValue("0.0"),"rho",StringValue("0.1"));

	  mobility.Install (ssNodes);

    stack.Install (ssNodes);
	  SSinterface = address.Assign (ssDevs);

		NS_LOG_UNCOND("Done assigning ssNodes, starting BS assignments...");

		Ipv4InterfaceContainer BSinterface;
		NodeContainer bsNodes;
		NetDeviceContainer bsDevs;

		bsNodes.Create (1);
		mobility.Install (bsNodes);
		bsDevs = wimax.Install (bsNodes.Get(0), WimaxHelper::DEVICE_TYPE_BASE_STATION,
												WimaxHelper::SIMPLE_PHY_TYPE_OFDM,
												scheduler);

		bsDevs.Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->SetChannelBandwidth(5e6);

		stack.Install (bsNodes);
		BSinterface = address.Assign (bsDevs);

		//### Uplink Traffic-TCP--integrity poll response
		//###
		duration = simulation_time;

		double unit_data_rate = 0.129; // kb/s
		std::string data_rate;
		stringstream s;
		s << unit_data_rate << "kb/s";
		data_rate = s.str();
		int startPort=100;
		std::string on_time = "ns3::ConstantRandomVariable[Constant=5.0]";
		std::string off_time = "ns3::ConstantRandomVariable[Constant=0.0]";
		setup_tcp(nbSS, ssNodes, SSinterface,
										bsNodes, BSinterface,
										duration, on_time, off_time, startPort);

		for (int i = 0; i < nbSS; i++)
		{
			IpcsClassifierRecord ulClassifierBe (SSinterface.GetAddress (i),
																					 Ipv4Mask ("255.255.255.255"),
																					 Ipv4Address ("0.0.0.0"),
																					 Ipv4Mask ("0.0.0.0"),
																					 0,
																					 65000,
																					 50000,
																					 50000,
																					 17,
																					 1);
			ServiceFlow ulServiceFlow = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP,
																														 ServiceFlow::SF_TYPE_UGS,
																														 ulClassifierBe);
			ss[i]->AddServiceFlow (ulServiceFlow);

		}
		wimax.EnableAscii ("bs-devices", bsDevs);
		wimax.EnableAscii ("ss-devices", ssDevs);
		wimax.EnablePcapAll ("wimax-program-pcap",  false);

		NS_LOG_UNCOND ("Starting simulation.....");
		Simulator::Stop(Seconds(simulation_time));


		Simulator::Run ();


		NS_LOG_INFO ("Done.");

		Simulator::Destroy ();

		return 0;
}
