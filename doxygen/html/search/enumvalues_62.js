var searchData=
[
  ['basic',['BASIC',['../classns3_1_1Cid.html#a10b8f92080ca5790e65a0bfa2f557e0aa68b82b5b38abe3f2b40e5e3d950ac746',1,'ns3::Cid']]],
  ['broadcast',['BROADCAST',['../classns3_1_1Cid.html#a10b8f92080ca5790e65a0bfa2f557e0aa1ced0953e087f23571111ae36699415a',1,'ns3::Cid']]],
  ['bs_5fstate_5fdl_5fsub_5fframe',['BS_STATE_DL_SUB_FRAME',['../classns3_1_1BaseStationNetDevice.html#a70b3d78837bcc09df385146fb7f51498a452d6ef98b926c1b244b01730ce44391',1,'ns3::BaseStationNetDevice']]],
  ['bs_5fstate_5frtg',['BS_STATE_RTG',['../classns3_1_1BaseStationNetDevice.html#a70b3d78837bcc09df385146fb7f51498a49e8db30dcf37738c5149f670c282fdc',1,'ns3::BaseStationNetDevice']]],
  ['bs_5fstate_5fttg',['BS_STATE_TTG',['../classns3_1_1BaseStationNetDevice.html#a70b3d78837bcc09df385146fb7f51498aeccaf118148a1c72ddfa5bec02b30ef6',1,'ns3::BaseStationNetDevice']]],
  ['bs_5fstate_5ful_5fsub_5fframe',['BS_STATE_UL_SUB_FRAME',['../classns3_1_1BaseStationNetDevice.html#a70b3d78837bcc09df385146fb7f51498aea64791fa58c182a50bf32da2b0a6f52',1,'ns3::BaseStationNetDevice']]]
];
