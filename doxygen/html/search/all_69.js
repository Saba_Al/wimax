var searchData=
[
  ['increasebacklogged',['IncreaseBacklogged',['../classns3_1_1ServiceFlowRecord.html#a5a51321879ed7cfab5820eb9d02f5459',1,'ns3::ServiceFlowRecord']]],
  ['increasebackloggedtemp',['IncreaseBackloggedTemp',['../classns3_1_1ServiceFlowRecord.html#a9162d74489b730e4249f4fc5f15e4a2e',1,'ns3::ServiceFlowRecord']]],
  ['incrementdsarspretries',['IncrementDsaRspRetries',['../classns3_1_1SSRecord.html#a76ff41d0ed227b65896d7924c3d911d7',1,'ns3::SSRecord']]],
  ['incrementinvitedrangingretries',['IncrementInvitedRangingRetries',['../classns3_1_1SSRecord.html#aaaa1931f5cc66c626a8bdb14affa3010',1,'ns3::SSRecord']]],
  ['incrementnrinvitedpollsrecvd',['IncrementNrInvitedPollsRecvd',['../classns3_1_1SSLinkManager.html#a5021f5083b69a2c8cd606501b26d5409',1,'ns3::SSLinkManager']]],
  ['incrementrangingcorrectionretries',['IncrementRangingCorrectionRetries',['../classns3_1_1SSRecord.html#acb8188736cb593974760cdd0f2a19e8d',1,'ns3::SSRecord']]],
  ['index',['Index',['../classns3_1_1ClassificationRuleVectorTlvValue.html#a9945c44c631de44d3b9c8dc9560cb820a411a276461e9e48f9bc54fecce932f54',1,'ns3::ClassificationRuleVectorTlvValue']]],
  ['initbasestationnetdevice',['InitBaseStationNetDevice',['../classns3_1_1BaseStationNetDevice.html#a26fd7b4b847fabe35297df4a70d4858a',1,'ns3::BaseStationNetDevice']]],
  ['initial_5franging',['INITIAL_RANGING',['../classns3_1_1Cid.html#a10b8f92080ca5790e65a0bfa2f557e0aac5353b87e0d0f5ce62535441b7554436',1,'ns3::Cid']]],
  ['initialranging',['InitialRanging',['../classns3_1_1Cid.html#a5c204ab0df783c7594c764bae1611436',1,'ns3::Cid']]],
  ['initiateserviceflows',['InitiateServiceFlows',['../classns3_1_1SsServiceFlowManager.html#a8cc3878dd2f5b51c1d25624117072257',1,'ns3::SsServiceFlowManager']]],
  ['initonce',['InitOnce',['../classns3_1_1UplinkSchedulerMBQoS.html#aa0ce923b356285a9e28d90b698d17557',1,'ns3::UplinkSchedulerMBQoS::InitOnce()'],['../classns3_1_1UplinkSchedulerRtps.html#ac712136e3b5f7e856daba25d7a946747',1,'ns3::UplinkSchedulerRtps::InitOnce()'],['../classns3_1_1UplinkSchedulerSimple.html#a075911b46c3528c4adc05ef7236fdad6',1,'ns3::UplinkSchedulerSimple::InitOnce()'],['../classns3_1_1UplinkScheduler.html#ae427e6144919f70a361c047ddcfd5c4c',1,'ns3::UplinkScheduler::InitOnce()']]],
  ['initsubscriberstationnetdevice',['InitSubscriberStationNetDevice',['../classns3_1_1SubscriberStationNetDevice.html#a4279217c5fd1bb26fb762adbb88e4861',1,'ns3::SubscriberStationNetDevice']]],
  ['initvalues',['InitValues',['../classns3_1_1ServiceFlow.html#ab0033129ef33dbf04faa2bc98c179f4f',1,'ns3::ServiceFlow']]],
  ['install',['Install',['../classns3_1_1WimaxHelper.html#a4eb96ce5b91ec7583131b1066845b3a2',1,'ns3::WimaxHelper::Install(NodeContainer c, NetDeviceType type, PhyType phyType, SchedulerType schedulerType, std::vector&lt; Node_Data &gt; *nodes_Info=NULL, bool activateLoss=false)'],['../classns3_1_1WimaxHelper.html#afa527d7cf5d240ee6ba0986c331475cb',1,'ns3::WimaxHelper::Install(NodeContainer c, NetDeviceType deviceType, PhyType phyType, Ptr&lt; WimaxChannel &gt; channel, SchedulerType schedulerType)'],['../classns3_1_1WimaxHelper.html#aca8acdb7cf193a3ba451a080cf57d4d4',1,'ns3::WimaxHelper::Install(NodeContainer c, NetDeviceType deviceType, PhyType phyType, SchedulerType schedulerType, double frameDuration, std::vector&lt; Node_Data &gt; *nodes_Info=NULL, bool activateLoss=false)'],['../classns3_1_1WimaxHelper.html#acf963b04d7d91b05e268d73837fd1823',1,'ns3::WimaxHelper::Install(Ptr&lt; Node &gt; node, NetDeviceType deviceType, PhyType phyType, Ptr&lt; WimaxChannel &gt; channel, SchedulerType schedulerType)']]],
  ['intermediate',['INTERMEDIATE',['../classns3_1_1UlJob.html#a6ae1d8e2e490a32ee1bc8aae661f4983a3619db7d63201ac367a111b38e443eea',1,'ns3::UlJob']]],
  ['ip_5fdst',['IP_dst',['../classns3_1_1ClassificationRuleVectorTlvValue.html#a9945c44c631de44d3b9c8dc9560cb820ab9d2ecb509415d656b9fecb6470a11d5',1,'ns3::ClassificationRuleVectorTlvValue']]],
  ['ip_5fsrc',['IP_src',['../classns3_1_1ClassificationRuleVectorTlvValue.html#a9945c44c631de44d3b9c8dc9560cb820a25dcc2e936d897953dd8fced923caa83',1,'ns3::ClassificationRuleVectorTlvValue']]],
  ['ipcs_2dclassifier_2drecord_2ecc',['ipcs-classifier-record.cc',['../ipcs-classifier-record_8cc.html',1,'']]],
  ['ipcs_2dclassifier_2drecord_2eh',['ipcs-classifier-record.h',['../ipcs-classifier-record_8h.html',1,'']]],
  ['ipcs_2dclassifier_2ecc',['ipcs-classifier.cc',['../ipcs-classifier_8cc.html',1,'']]],
  ['ipcs_2dclassifier_2eh',['ipcs-classifier.h',['../ipcs-classifier_8h.html',1,'']]],
  ['ipcsclassifier',['IpcsClassifier',['../classns3_1_1IpcsClassifier.html',1,'ns3']]],
  ['ipcsclassifier',['IpcsClassifier',['../classns3_1_1IpcsClassifier.html#ab35fb60fa2b310acbed40b2f82190cc7',1,'ns3::IpcsClassifier']]],
  ['ipcsclassifierrecord',['IpcsClassifierRecord',['../classns3_1_1IpcsClassifierRecord.html',1,'ns3']]],
  ['ipcsclassifierrecord',['IpcsClassifierRecord',['../classns3_1_1IpcsClassifierRecord.html#aa82334e8306a9a585f66ef81429887b3',1,'ns3::IpcsClassifierRecord::IpcsClassifierRecord()'],['../classns3_1_1IpcsClassifierRecord.html#a42ad4b285a8e1d4ce754e35dba41567a',1,'ns3::IpcsClassifierRecord::IpcsClassifierRecord(Ipv4Address srcAddress, Ipv4Mask srcMask, Ipv4Address dstAddress, Ipv4Mask dstMask, uint16_t srcPortLow, uint16_t srcPortHigh, uint16_t dstPortLow, uint16_t dstPortHigh, uint8_t protocol, uint8_t priority)'],['../classns3_1_1IpcsClassifierRecord.html#a451204bc3818c6a41e575a35ab405791',1,'ns3::IpcsClassifierRecord::IpcsClassifierRecord(Tlv tlv)']]],
  ['ipv4',['IPV4',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952a37e72e9b5d203d04c21d77f9cab6a94f',1,'ns3::ServiceFlow']]],
  ['ipv4_5fcs_5fparameters',['IPV4_CS_Parameters',['../classns3_1_1SfVectorTlvValue.html#aa23ab5c7acfce609dbfe28024c6d2ef7a32d52193813904b0a4a64e90be836dcf',1,'ns3::SfVectorTlvValue']]],
  ['ipv4_5fover_5fethernet',['IPV4_OVER_ETHERNET',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952a154501260038cd509819bd40ed5cc541',1,'ns3::ServiceFlow']]],
  ['ipv4_5fover_5fvlan',['IPV4_OVER_VLAN',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952a8f6135416e0043e495255e0832c9a2c1',1,'ns3::ServiceFlow']]],
  ['ipv4addr',['ipv4Addr',['../structns3_1_1Ipv4AddressTlvValue_1_1ipv4Addr.html',1,'ns3::Ipv4AddressTlvValue']]],
  ['ipv4addresstlvvalue',['Ipv4AddressTlvValue',['../classns3_1_1Ipv4AddressTlvValue.html#a886f71acc8111d82109920287c33c688',1,'ns3::Ipv4AddressTlvValue']]],
  ['ipv4addresstlvvalue',['Ipv4AddressTlvValue',['../classns3_1_1Ipv4AddressTlvValue.html',1,'ns3']]],
  ['ipv6',['IPV6',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952a327fc51c98215eaf4534a48656a8a45a',1,'ns3::ServiceFlow']]],
  ['ipv6_5fover_5fethernet',['IPV6_OVER_ETHERNET',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952a03f39694036b733efc4439b81ecede17',1,'ns3::ServiceFlow']]],
  ['ipv6_5fover_5fvlan',['IPV6_OVER_VLAN',['../classns3_1_1ServiceFlow.html#ad87f7547b7c053db4472543e17d21952ac3e53926df7a2d66131c459326e3f480',1,'ns3::ServiceFlow']]],
  ['isbasic',['IsBasic',['../classns3_1_1CidFactory.html#a7d70ebd7630a8414ce3eefb7580bdc57',1,'ns3::CidFactory']]],
  ['isbridge',['IsBridge',['../classns3_1_1WimaxNetDevice.html#aa6c47d753b4171fe54454fb8e5d81eba',1,'ns3::WimaxNetDevice']]],
  ['isbroadcast',['IsBroadcast',['../classns3_1_1Cid.html#af02254c3891a00a8032359fa14206d55',1,'ns3::Cid::IsBroadcast()'],['../classns3_1_1WimaxNetDevice.html#a2719e45018c84b51577e6b17e57d46ad',1,'ns3::WimaxNetDevice::IsBroadcast()']]],
  ['isduplex',['IsDuplex',['../classns3_1_1WimaxPhy.html#a14feaf88ff5560e1c15c978bcc8147e4',1,'ns3::WimaxPhy']]],
  ['isempty',['IsEmpty',['../classns3_1_1WimaxMacQueue.html#a45fc4e6c31ac1789b8fdedf4a8714e12',1,'ns3::WimaxMacQueue::IsEmpty(void) const '],['../classns3_1_1WimaxMacQueue.html#ac5441bd719302dd5e546601c19b0b23c',1,'ns3::WimaxMacQueue::IsEmpty(MacHeaderType::HeaderType packetType) const ']]],
  ['isinitialranging',['IsInitialRanging',['../classns3_1_1Cid.html#a731e5439f22cd7843b9c5958f03ea825',1,'ns3::Cid']]],
  ['isinrecord',['IsInRecord',['../classns3_1_1SSManager.html#a545c577246d52d9b1a16aad729198764',1,'ns3::SSManager']]],
  ['islinkup',['IsLinkUp',['../classns3_1_1WimaxNetDevice.html#a7d162542e86e757bd41593b82fe189cd',1,'ns3::WimaxNetDevice']]],
  ['ismulticast',['IsMulticast',['../classns3_1_1Cid.html#a2bd52ee9cef710e97c3e9034b680ac38',1,'ns3::Cid::IsMulticast()'],['../classns3_1_1WimaxNetDevice.html#a5d9dcdbdf2a35d2d6b6f2ac0026f483f',1,'ns3::WimaxNetDevice::IsMulticast()']]],
  ['ispadding',['IsPadding',['../classns3_1_1Cid.html#ac029211ea9958cf85f8b83ed17413d4b',1,'ns3::Cid']]],
  ['ispointtopoint',['IsPointToPoint',['../classns3_1_1WimaxNetDevice.html#a919a1d701ab5ef589152bcdef403871b',1,'ns3::WimaxNetDevice']]],
  ['isprimary',['IsPrimary',['../classns3_1_1CidFactory.html#a9ce2f9de01ae4bf9d782e043eef025db',1,'ns3::CidFactory']]],
  ['ispromisc',['IsPromisc',['../classns3_1_1WimaxNetDevice.html#a723428fe404a97984ad511200ac124cb',1,'ns3::WimaxNetDevice']]],
  ['isregistered',['IsRegistered',['../classns3_1_1SSManager.html#a486b9d022058a8e867d8a4bdf07e65d9',1,'ns3::SSManager::IsRegistered()'],['../classns3_1_1SubscriberStationNetDevice.html#ad88fedaf8b13d09d0c0d9aa0e52dc1b0',1,'ns3::SubscriberStationNetDevice::IsRegistered()']]],
  ['istransport',['IsTransport',['../classns3_1_1CidFactory.html#a0963e7b59774db4fb70956414f1a08f4',1,'ns3::CidFactory']]],
  ['isulchannelusable',['IsUlChannelUsable',['../classns3_1_1SSLinkManager.html#a95ea2618a3e9fffa842a86fe55d0175d',1,'ns3::SSLinkManager']]],
  ['iterator',['Iterator',['../classns3_1_1VectorTlvValue.html#a7b80a0076eb50f48943f779bb952a1e9',1,'ns3::VectorTlvValue::Iterator()'],['../classns3_1_1PortRangeTlvValue.html#a04bc61dc4b03a8e91d6ec855c8c88a69',1,'ns3::PortRangeTlvValue::Iterator()'],['../classns3_1_1ProtocolTlvValue.html#a83a87352f3acebc8d47c963d4f1936f0',1,'ns3::ProtocolTlvValue::Iterator()'],['../classns3_1_1Ipv4AddressTlvValue.html#a459ee680950c4dc60b4962224f6c7992',1,'ns3::Ipv4AddressTlvValue::Iterator()']]]
];
