//============================================================================
// Name        : XML_Read.cpp
// Author      : Fariba Aalamifar
// Version     :	1.0
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include "ns3/XML_Read.h"
#include "ns3/ptr.h"
#include "ns3/core-module.h"


using namespace std;

namespace ns3 {
double degree_to_Kmeter(double ddmmss, bool latitude)
{
	double ref,scale;
	if (latitude)
	{
		ref = 440000;
		scale=111.2; //km
	}
	else
	{
		ref = 790000;
		scale = 79.5; //km
	}
	int mmss = ddmmss-ref;
	int ss = mmss %100;
	int mm = (mmss-ss)/100;
	return scale*(mm*60+ss)/3600.0; //kmeters
};

//double degree_to_decimal(double ddmmss)
//{
//	double degrees = floor(ddmmss);
//	double rem = (ddmmss-degrees) * 60;
//	double minutes = floor(rem);
//	double seconds = (rem-minutes) * 60;
//	return degrees + minutes/60 + seconds/3600;
////	int ss = mmss %100;
////	int mm = (mmss-ss)/100;
////	return (mm*60+ss)/3600.0;
//};

/*
double degree_to_decimal(double ddmmss, bool latitude)
{
	double ref,scale;
	if (latitude)
	{
		ref = 440000;
		scale=111.2;
	}
	else
	{
		ref = 790000;
		scale = 79.5;
	}
	int mmss = ddmmss-ref;
	int ss = mmss %100;
	int mm = (mmss-ss)/100;
	return (mm*60+ss)/3600.0+ref;
};
*/

//double degree_to_distance(double bsLat, double bsLon, double cpeLat, double cpeLon)
//{
//	double R = 6371; //km
//	double dLat= (cpeLat - bsLat).toRad();
//	double
//}

Node_Data::Node_Data(void){
	RxFreq = 0;
	TxFreq = 0;
	bs = false;
};

Node_Data::~Node_Data(void){};

//BS_Data::BS_Data(void){
////	location = {};
////	longtitude = 0;
////	latitude = 0;
////	elevation = 0;
////	RxFreq = 0;
////	TxFreq = 0;
////	RxGain = 0;
////	TxGain = 0;
////	TxPower = 0;
////	TxAntennaHeight = 0;
////	RxAntennaHeight = 0;
//}
//
//BS_Data::~BS_Data(void){
//
//}

void Node_Data::print()
	{
		cout    << "[ location="<<location<<endl
				<< "longtitude="<<longtitude<<endl
				<< "latitude="<<latitude<<endl
				<< "elevation="<<elevation<<endl
				<< "RxFreq="<<RxFreq<<endl
				<< "TxFreq="<<TxFreq<<endl
				<< "RxGain="<<RxGain<<endl
				<< "TxGain="<<TxGain<<endl
				<< "TxAntennaHeight="<<TxAntennaHeight<<endl
				<< "RxAntennaHeight="<<RxAntennaHeight<<endl
				<< "TxPower="<<TxPower<<" ]"<<endl;
	}

//CPE_Data::CPE_Data(void){
////	location = {};
////	longtitude = 0;
////	latitude = 0;
////	elevation = 0;
////	RxFreq = 0;
////	TxFreq = 0;
////	RxGain = 0;
////	TxGain = 0;
////	TxPower = 0;
////	TxAntennaHeight = 0;
////	RxAntennaHeight = 0;
////	BS_Name = {};
////	//BS_Data *myBS;
////	sectNum = 0;
//}

//CPE_Data::~CPE_Data(void){};
//
//void CPE_Data::print()
//	{
//		cout    << "[ location="<<location<<endl
//				<< "longtitude="<<longtitude<<endl
//				<< "latitude="<<latitude<<endl
//				<< "elevation="<<elevation<<endl
//				<< "RxFreq="<<RxFreq<<endl
//				<< "TxFreq="<<TxFreq<<endl
//				<< "RxGain="<<RxGain<<endl
//				<< "TxGain="<<TxGain<<endl
//				<< "TxAntennaHeight="<<TxAntennaHeight<<endl
//				<< "RxAntennaHeight="<<RxAntennaHeight<<endl
//				<< "TxPower="<<TxPower<<endl
//				//<< "Linked to"<<myBS->location<<endl
//				<< "Linked to " << BS_Name <<endl
//				<< "Sector number "<<sectNum<<" "<<"]"<<endl;
//	}


void strip(const string &line,string &tmp) // strip whitespaces from the beginning
{
	for (unsigned int i = 0; i < line.size(); i++)
	{
		if ((line[i] == ' ' || line[i]=='\t') && tmp.size() == 0)
		{
		}
		else
		{
			tmp += line[i];
		}
	}
};

void Xml_Read(const char* FileName, vector<Node_Data> &BS_Info, vector<Node_Data> &CPEs_Info){
	string line;
	//ifstream inpfile(FileName);
	//bool new_station;
	ifstream inpfile ( FileName );
	string new_station_tag_begin = string("<new_station num=\"");
	string new_station_tag_end = string("</new_station>");
	string new_frequency_tag_begin = string("<new_frequency num=\"");
	string new_frequency_tag_end = string("</new_frequency");
	string link_station_tag_begin= string("<link_station num=\"");
	string link_station_tag_end= string("</link_station>");
	string link_station_location_tag_begin= string("<link_station_location>");
	string link_station_location_tag_end= string("</link_station_location>");
	string txFrequency_tag_begin = string("<tx_frequency>");
	string txFrequency_tag_end=string("</tx_frequency>");
	string rxFrequency_tag_begin = string("<rx_frequency>");
	string rxFrequency_tag_end=string("</rx_frequency>");
	string txAntennaHeight_tag_begin = string("<tx_ant_height>");
	string txAntennaHeight_tag_end=string("</tx_ant_height>");
	string rxAntennaHeight_tag_begin = string("<rx_ant_height>");
	string rxAntennaHeight_tag_end=string("</rx_ant_height>");
	string txAntennaGain_tag_begin = string("<tx_ant_gain>");
	string txAntennaGain_tag_end=string("</tx_ant_gain>");
	string rxAntennaGain_tag_begin = string("<rx_ant_gain>");
	string rxAntennaGain_tag_end=string("</rx_ant_gain>");
	string txPower_tag_begin = string("<rf_output_power>");
	string txPower_tag_end = string("</rf_output_power>");
	string location_tag_begin = string("<location>");
	string location_tag_end = string("</location>");
	string latitude_tag_begin = string("<latitude>");
	string latitude_tag_end = string("</latitude>");
	string longtitude_tag_begin = string("<longitude>");
	string longtitude_tag_end = string("</longitude>");
	string elevation_tag_begin = string("<site_elevation>");
	string elevation_tag_end = string("</site_elevation>");
	unsigned int BS_Cnt=0, CPE_Cnt=0;

	while (getline(inpfile,line))
	{
			//cout << "in file" << endl;
			std::string tmp;
			strip(line,tmp);
			//cout << "tmp=" << tmp << endl;
			if(BS_Cnt < 3)
			{
				if (tmp.compare(0,new_station_tag_begin.size(),new_station_tag_begin)==0)
				{
					cout << "BS yes" << endl;

					std::string tmp2("");
					string BS_Location;
					double BS_Latitude,BS_Longtitude, BS_Elevation;
					while (tmp2.compare(0,new_station_tag_end.size(),new_station_tag_end)!=0)
					{
						//cout << "tmp2=" << tmp2 << endl;
						tmp2="";
						getline(inpfile,line);
						strip(line,tmp2);

						if (tmp2.compare(0,location_tag_begin.size(),location_tag_begin)==0)
						{
							BS_Location = tmp2.substr(location_tag_begin.size(),
									tmp2.size()-location_tag_begin.size()-location_tag_end.size());
							cout << "BS_Location:" << BS_Location <<endl;
						}
						else if (tmp2.compare(0,latitude_tag_begin.size(),latitude_tag_begin)==0)
						{
							BS_Latitude = atof(tmp2.substr(latitude_tag_begin.size(),
									tmp2.size()-latitude_tag_begin.size()-latitude_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,longtitude_tag_begin.size(),longtitude_tag_begin)==0)
						{
							BS_Longtitude = atof(tmp2.substr(longtitude_tag_begin.size(),
									tmp2.size()-longtitude_tag_begin.size()-longtitude_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,elevation_tag_begin.size(),elevation_tag_begin)==0)
						{
							BS_Elevation = atof(tmp2.substr(elevation_tag_begin.size(),
									tmp2.size()-elevation_tag_begin.size()-elevation_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,new_frequency_tag_begin.size(),new_frequency_tag_begin)==0)
						{
							//Start of the new sector for BS.
							//BS_Data BS=BS_Data();
							Node_Data BS = Node_Data ();

							BS.location = BS_Location;
							BS.latitude = degree_to_Kmeter(BS_Latitude,true);
							BS.longtitude = degree_to_Kmeter(BS_Longtitude,false);
							BS.elevation = BS_Elevation;
							BS.bs = true;

							std::string tmp3("");
							while (tmp3.compare(0,new_frequency_tag_end.size(),new_frequency_tag_end)!=0)
							{
								tmp3="";
								getline(inpfile,line);
								strip(line,tmp3);

								if ( tmp3.compare(0,txFrequency_tag_begin.size(),txFrequency_tag_begin)==0)
								{
									BS.TxFreq = atof(tmp3.substr(txFrequency_tag_begin.size(),tmp3.size()-txFrequency_tag_begin.size()-txFrequency_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,rxFrequency_tag_begin.size(),rxFrequency_tag_begin)==0)
								{
									BS.RxFreq = atof(tmp3.substr(rxFrequency_tag_begin.size(),tmp3.size()-rxFrequency_tag_begin.size()-rxFrequency_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,txAntennaHeight_tag_begin.size(),txAntennaHeight_tag_begin)==0)
								{
									BS.TxAntennaHeight =atof(tmp3.substr(txAntennaHeight_tag_begin.size(),tmp3.size()-txAntennaHeight_tag_begin.size()-txAntennaHeight_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,rxAntennaHeight_tag_begin.size(),rxAntennaHeight_tag_begin)==0)
								{
									BS.RxAntennaHeight =atof(tmp3.substr(rxAntennaHeight_tag_begin.size(),tmp3.size()-rxAntennaHeight_tag_begin.size()-rxAntennaHeight_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,txAntennaGain_tag_begin.size(),txAntennaGain_tag_begin)==0)
								{
									BS.TxGain =atof(tmp3.substr(txAntennaGain_tag_begin.size(),tmp3.size()-txAntennaGain_tag_begin.size()-txAntennaGain_tag_end.size()).c_str());
									//BS.TxGain = 10 * std::log (BS.TxGain) / 2.302 + 30;
								}
								else if (tmp3.compare(0,rxAntennaGain_tag_begin.size(),rxAntennaGain_tag_begin)==0)
								{
									BS.RxGain =atof(tmp3.substr(rxAntennaGain_tag_begin.size(),tmp3.size()-rxAntennaGain_tag_begin.size()-rxAntennaGain_tag_end.size()).c_str());
									//BS.RxGain = 10 * std::log (BS.RxGain) / 2.302 + 30;
								}
								else if (tmp3.compare(0,txPower_tag_begin.size(),txPower_tag_begin)==0)
								{
									BS.TxPower =atof(tmp3.substr(txPower_tag_begin.size(),tmp3.size()-txPower_tag_begin.size()-txPower_tag_end.size()).c_str());
									BS.TxPower = 10 * std::log (BS.TxPower) / 2.302 + 30;
								}
								else if (tmp3.compare(0,link_station_tag_begin.size(),link_station_tag_begin)==0)
								{
									string tmp4=string("");
									while (tmp4.compare(0,link_station_tag_end.size(),link_station_tag_end)!=0)
									{
										tmp4="";
										getline(inpfile,line);
										strip(line,tmp4);
										//cout << "tmp4:" << tmp4 << endl;
										if ( tmp4.compare(0,link_station_location_tag_begin.size(),link_station_location_tag_begin)==0)
										{
											string CPE_Location = tmp4.substr(link_station_location_tag_begin.size(),
																				tmp4.size()-link_station_location_tag_begin.size()-link_station_location_tag_end.size());
											//CPE_Data CPE = CPE_Data();
											Node_Data CPE = Node_Data ();
											CPE.location = CPE_Location;
											CPE.sectNum = BS_Cnt+1;
											CPE.bs = false;
											CPEs_Info.push_back(CPE);
											//cout /*<<  "from BS Sector#" << BS_Cnt+1 */<<"; Link Station Location is " << CPE_Location << endl;
										}
									}
								}
							}
							BS_Info.push_back(BS);
							//cout << "BS_Info size=" << BS_Info.size() << endl;
							BS_Cnt++;
						}
					}
				}//if new station tag

			}//if bs_count
			else //CPE_Read_Data
			{
				if (tmp.compare(0,new_station_tag_begin.size(),new_station_tag_begin)==0)
				{
					cout << "CPE#" << CPE_Cnt << " ";
					//new_station = true;
					//CPE_Data CPE; //=CPE_Data();
					unsigned int CPE_Data_Loc;//location of CPE_Data in CPEs_Info
					std::string tmp2(""); // strip whitespaces from the beginning
					while (tmp2.compare(0,new_station_tag_end.size(),new_station_tag_end)!=0)
					{
						//cout << "tmp2=" << tmp2 << endl;
						tmp2="";
						getline(inpfile,line);
						strip(line,tmp2);
						if (tmp2.compare(0,location_tag_begin.size(),location_tag_begin)==0)
						{
							string CPE_Location = tmp2.substr(location_tag_begin.size(),
									tmp2.size()-location_tag_begin.size()-location_tag_end.size()
									);

							cout << CPEs_Info.at(CPE_Cnt).location << " & " << CPE_Location << endl << endl;

							CPE_Data_Loc = CPE_Cnt;
							CPEs_Info.at(CPE_Data_Loc).location = CPE_Location;

//							CPEs_Info.at(CPE_Data_Loc).Node_Name = BS_Info.at(CPEs_Info.at(CPE_Data_Loc).sectNum-1).location;
						}
						else if (tmp2.compare(0,latitude_tag_begin.size(),latitude_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).latitude = degree_to_Kmeter(atof(tmp2.substr(latitude_tag_begin.size(),
									tmp2.size()-latitude_tag_begin.size()-latitude_tag_end.size()).c_str()),true);
						}
						else if (tmp2.compare(0,longtitude_tag_begin.size(),longtitude_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).longtitude = degree_to_Kmeter(atof(tmp2.substr(longtitude_tag_begin.size(),
									tmp2.size()-longtitude_tag_begin.size()-longtitude_tag_end.size()).c_str()),false);
						}
						else if (tmp2.compare(0,elevation_tag_begin.size(),elevation_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).elevation = atof(tmp2.substr(elevation_tag_begin.size(),
									tmp2.size()-elevation_tag_begin.size()-elevation_tag_end.size()).c_str());
						}
						else if ( tmp2.compare(0,txFrequency_tag_begin.size(),txFrequency_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxFreq = atof(tmp2.substr(txFrequency_tag_begin.size(),
									tmp2.size()-txFrequency_tag_begin.size()-txFrequency_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,rxFrequency_tag_begin.size(),rxFrequency_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxFreq = atof(tmp2.substr(rxFrequency_tag_begin.size(),
									tmp2.size()-rxFrequency_tag_begin.size()-rxFrequency_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,txAntennaHeight_tag_begin.size(),txAntennaHeight_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxAntennaHeight =atof(tmp2.substr(txAntennaHeight_tag_begin.size(),
									tmp2.size()-txAntennaHeight_tag_begin.size()-txAntennaHeight_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,rxAntennaHeight_tag_begin.size(),rxAntennaHeight_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxAntennaHeight =atof(tmp2.substr(rxAntennaHeight_tag_begin.size(),
									tmp2.size()-rxAntennaHeight_tag_begin.size()-rxAntennaHeight_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,txAntennaGain_tag_begin.size(),txAntennaGain_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxGain =atof(tmp2.substr(txAntennaGain_tag_begin.size(),tmp2.size()-txAntennaGain_tag_begin.size()-txAntennaGain_tag_end.size()).c_str());
						//	CPEs_Info.at(CPE_Data_Loc).TxGain = 10 * std::log (CPEs_Info.at(CPE_Data_Loc).TxGain) / 2.302 + 30;
						}
						else if (tmp2.compare(0,rxAntennaGain_tag_begin.size(),rxAntennaGain_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxGain =atof(tmp2.substr(rxAntennaGain_tag_begin.size(),tmp2.size()-rxAntennaGain_tag_begin.size()-rxAntennaGain_tag_end.size()).c_str());
						//	CPEs_Info.at(CPE_Data_Loc).RxGain = 10 * std::log (CPEs_Info.at(CPE_Data_Loc).RxGain) / 2.302 + 30;
						}
						else if (tmp2.compare(0,txPower_tag_begin.size(),txPower_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxPower =atof(tmp2.substr(txPower_tag_begin.size(),tmp2.size()-txPower_tag_begin.size()-txPower_tag_end.size()).c_str());
							CPEs_Info.at(CPE_Data_Loc).TxPower = 10 * std::log (CPEs_Info.at(CPE_Data_Loc).TxPower) / 2.302 + 30;
						}
					}
					CPE_Cnt ++;
					//cout << "CPE_Info size=" << CPE_Info.size() << endl;
				}
			}
	}
}//XML_Read

void Xml_Read_Freq_Compatible(const char* FileName, vector<Node_Data> &BS_Info, vector<Node_Data> &CPEs_Info){
	string line;
	//ifstream inpfile(FileName);
	//bool new_station;
	ifstream inpfile ( FileName );
	string new_station_tag_begin = string("<new_station num=\"");
	string new_station_tag_end = string("</new_station>");
	string new_frequency_tag_begin = string("<new_frequency num=\"");
	string new_frequency_tag_end = string("</new_frequency");
	string link_station_tag_begin= string("<link_station num=\"");
	string link_station_tag_end= string("</link_station>");
	string link_station_location_tag_begin= string("<link_station_location>");
	string link_station_location_tag_end= string("</link_station_location>");
	string txFrequency_tag_begin = string("<tx_frequency>");
	string txFrequency_tag_end=string("</tx_frequency>");
	string rxFrequency_tag_begin = string("<rx_frequency>");
	string rxFrequency_tag_end=string("</rx_frequency>");
	string txAntennaHeight_tag_begin = string("<tx_ant_height>");
	string txAntennaHeight_tag_end=string("</tx_ant_height>");
	string rxAntennaHeight_tag_begin = string("<rx_ant_height>");
	string rxAntennaHeight_tag_end=string("</rx_ant_height>");
	string txAntennaGain_tag_begin = string("<tx_ant_gain>");
	string txAntennaGain_tag_end=string("</tx_ant_gain>");
	string rxAntennaGain_tag_begin = string("<rx_ant_gain>");
	string rxAntennaGain_tag_end=string("</rx_ant_gain>");
	string txPower_tag_begin = string("<rf_output_power>");
	string txPower_tag_end = string("</rf_output_power>");
	string location_tag_begin = string("<location>");
	string location_tag_end = string("</location>");
	string latitude_tag_begin = string("<latitude>");
	string latitude_tag_end = string("</latitude>");
	string longtitude_tag_begin = string("<longitude>");
	string longtitude_tag_end = string("</longitude>");
	string elevation_tag_begin = string("<site_elevation>");
	string elevation_tag_end = string("</site_elevation>");
	unsigned int BS_Cnt=0, CPE_Cnt=0;
	double freq_sect_array[3];
	while (getline(inpfile,line))
	{
			//cout << "in file" << endl;
			std::string tmp;
			strip(line,tmp);
			//cout << "tmp=" << tmp << endl;
			if(BS_Cnt < 3)
			{
				if (tmp.compare(0,new_station_tag_begin.size(),new_station_tag_begin)==0)
				{
					cout << "BS yes" << endl;

					std::string tmp2("");
					string BS_Location;
					double BS_Latitude,BS_Longtitude, BS_Elevation;
					while (tmp2.compare(0,new_station_tag_end.size(),new_station_tag_end)!=0)
					{
						//cout << "tmp2=" << tmp2 << endl;
						tmp2="";
						getline(inpfile,line);
						strip(line,tmp2);

						if (tmp2.compare(0,location_tag_begin.size(),location_tag_begin)==0)
						{
							BS_Location = tmp2.substr(location_tag_begin.size(),
									tmp2.size()-location_tag_begin.size()-location_tag_end.size());
							cout << "BS_Location:" << BS_Location <<endl;
						}
						else if (tmp2.compare(0,latitude_tag_begin.size(),latitude_tag_begin)==0)
						{
							BS_Latitude = atof(tmp2.substr(latitude_tag_begin.size(),
									tmp2.size()-latitude_tag_begin.size()-latitude_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,longtitude_tag_begin.size(),longtitude_tag_begin)==0)
						{
							BS_Longtitude = atof(tmp2.substr(longtitude_tag_begin.size(),
									tmp2.size()-longtitude_tag_begin.size()-longtitude_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,elevation_tag_begin.size(),elevation_tag_begin)==0)
						{
							BS_Elevation = atof(tmp2.substr(elevation_tag_begin.size(),
									tmp2.size()-elevation_tag_begin.size()-elevation_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,new_frequency_tag_begin.size(),new_frequency_tag_begin)==0)
						{
							//Start of the new sector for BS.
							//BS_Data BS=BS_Data();
							Node_Data BS = Node_Data ();

							BS.location = BS_Location;
							BS.latitude = degree_to_Kmeter(BS_Latitude,true);
							BS.longtitude = degree_to_Kmeter(BS_Longtitude,false);
							BS.elevation = BS_Elevation;
							BS.bs = true;

							std::string tmp3("");
							while (tmp3.compare(0,new_frequency_tag_end.size(),new_frequency_tag_end)!=0)
							{
								tmp3="";
								getline(inpfile,line);
								strip(line,tmp3);

								if ( tmp3.compare(0,txFrequency_tag_begin.size(),txFrequency_tag_begin)==0)
								{
									BS.TxFreq = atof(tmp3.substr(txFrequency_tag_begin.size(),tmp3.size()-txFrequency_tag_begin.size()-txFrequency_tag_end.size()).c_str());
									freq_sect_array[BS_Cnt] = BS.TxFreq;
								}
								else if (tmp3.compare(0,rxFrequency_tag_begin.size(),rxFrequency_tag_begin)==0)
								{
									BS.RxFreq = atof(tmp3.substr(rxFrequency_tag_begin.size(),tmp3.size()-rxFrequency_tag_begin.size()-rxFrequency_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,txAntennaHeight_tag_begin.size(),txAntennaHeight_tag_begin)==0)
								{
									BS.TxAntennaHeight =atof(tmp3.substr(txAntennaHeight_tag_begin.size(),tmp3.size()-txAntennaHeight_tag_begin.size()-txAntennaHeight_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,rxAntennaHeight_tag_begin.size(),rxAntennaHeight_tag_begin)==0)
								{
									BS.RxAntennaHeight =atof(tmp3.substr(rxAntennaHeight_tag_begin.size(),tmp3.size()-rxAntennaHeight_tag_begin.size()-rxAntennaHeight_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,txAntennaGain_tag_begin.size(),txAntennaGain_tag_begin)==0)
								{
									BS.TxGain =atof(tmp3.substr(txAntennaGain_tag_begin.size(),tmp3.size()-txAntennaGain_tag_begin.size()-txAntennaGain_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,rxAntennaGain_tag_begin.size(),rxAntennaGain_tag_begin)==0)
								{
									BS.RxGain =atof(tmp3.substr(rxAntennaGain_tag_begin.size(),tmp3.size()-rxAntennaGain_tag_begin.size()-rxAntennaGain_tag_end.size()).c_str());
								}
								else if (tmp3.compare(0,txPower_tag_begin.size(),txPower_tag_begin)==0)
								{
									BS.TxPower =atof(tmp3.substr(txPower_tag_begin.size(),tmp3.size()-txPower_tag_begin.size()-txPower_tag_end.size()).c_str());
									BS.TxPower = 10 * std::log (BS.TxPower) / 2.302 + 30;
								}
								else if (tmp3.compare(0,link_station_tag_begin.size(),link_station_tag_begin)==0)
								{
									string tmp4=string("");
									while (tmp4.compare(0,link_station_tag_end.size(),link_station_tag_end)!=0)
									{
										tmp4="";
										getline(inpfile,line);
										strip(line,tmp4);
										//cout << "tmp4:" << tmp4 << endl;
										if ( tmp4.compare(0,link_station_location_tag_begin.size(),link_station_location_tag_begin)==0)
										{
											/*string CPE_Location = tmp4.substr(link_station_location_tag_begin.size(),
																				tmp4.size()-link_station_location_tag_begin.size()-link_station_location_tag_end.size());
											//CPE_Data CPE = CPE_Data();
											Node_Data CPE = Node_Data ();
											CPE.location = CPE_Location;
											CPE.sectNum = BS_Cnt+1;
											CPE.bs = false;
											CPEs_Info.push_back(CPE);
											cout <<  "Constructed a new CPE (total: " << CPEs_Info.size() << ") from BS Sector#" << BS_Cnt+1 << "; Link Station Location is " << CPE_Location << endl;
											*/
										}
									}
								}
							}
							BS_Info.push_back(BS);
							//cout << "BS_Info size=" << BS_Info.size() << endl;
							BS_Cnt++;
						}
					}
				}//if new station tag

			}//if bs_count
			else //CPE_Read_Data
			{
				if (tmp.compare(0,new_station_tag_begin.size(),new_station_tag_begin)==0)
				{
					cout << "CPE#" << CPE_Cnt << " ";
					//new_station = true;
					//CPE_Data CPE; //=CPE_Data();
					unsigned int CPE_Data_Loc;//location of CPE_Data in CPEs_Info
					std::string tmp2(""); // strip whitespaces from the beginning
					while (tmp2.compare(0,new_station_tag_end.size(),new_station_tag_end)!=0)
					{
						//cout << "tmp2=" << tmp2 << endl;
						tmp2="";
						getline(inpfile,line);
						strip(line,tmp2);
						if (tmp2.compare(0,location_tag_begin.size(),location_tag_begin)==0)
						{
							string CPE_Location = tmp2.substr(location_tag_begin.size(),
									tmp2.size()-location_tag_begin.size()-location_tag_end.size()
									);

							//cout << CPEs_Info.at(CPE_Cnt).location << " & " << CPE_Location << endl << endl;


							Node_Data CPE = Node_Data ();
							CPE.location = CPE_Location;
							//CPE.sectNum = BS_Cnt+1;
							CPE.bs = false;
							CPEs_Info.push_back(CPE);
							//cout /*<< "from BS Sector#" << BS_Cnt+1 */<< "; Link Station Location is " << CPE_Location << endl;

							CPE_Data_Loc = CPE_Cnt;
							CPEs_Info.at(CPE_Data_Loc).location = CPE_Location;

//							CPEs_Info.at(CPE_Data_Loc).Node_Name = BS_Info.at(CPEs_Info.at(CPE_Data_Loc).sectNum-1).location;
						}
						else if (tmp2.compare(0,latitude_tag_begin.size(),latitude_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).latitude = degree_to_Kmeter(atof(tmp2.substr(latitude_tag_begin.size(),
									tmp2.size()-latitude_tag_begin.size()-latitude_tag_end.size()).c_str()),true);
						}
						else if (tmp2.compare(0,longtitude_tag_begin.size(),longtitude_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).longtitude = degree_to_Kmeter(atof(tmp2.substr(longtitude_tag_begin.size(),
									tmp2.size()-longtitude_tag_begin.size()-longtitude_tag_end.size()).c_str()),false);
						}
						else if (tmp2.compare(0,elevation_tag_begin.size(),elevation_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).elevation = atof(tmp2.substr(elevation_tag_begin.size(),
									tmp2.size()-elevation_tag_begin.size()-elevation_tag_end.size()).c_str());
						}
						else if ( tmp2.compare(0,txFrequency_tag_begin.size(),txFrequency_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxFreq = atof(tmp2.substr(txFrequency_tag_begin.size(),
									tmp2.size()-txFrequency_tag_begin.size()-txFrequency_tag_end.size()).c_str());
							if(std::strcmp(FileName, "20131030PTMP INNISFIL DS modifiedRevA1.xml")== 0)
							{
								cout << "salam" << endl;
								if (CPEs_Info.at(CPE_Data_Loc).TxFreq == 1827.5)
									if (std::strcmp(CPEs_Info.at(CPE_Data_Loc).location.c_str(), "FENNEL MONITOR STN") == 0 || std::strcmp(CPEs_Info.at(CPE_Data_Loc).location.c_str(), "GILFORD MONITOR STN") == 0    || std::strcmp(CPEs_Info.at(CPE_Data_Loc).location.c_str(), "GOLDCREST PS")== 0  || std::strcmp(CPEs_Info.at(CPE_Data_Loc).location.c_str(), "COOKSTOWN M STN") == 0   )
										{
											CPEs_Info.at(CPE_Data_Loc).sectNum = 3;
											cout << "Salam111" << endl;
										}
									else
										{
											CPEs_Info.at(CPE_Data_Loc).sectNum = 1;
											cout << "Salam222" << endl;
										}
								else
									{
										CPEs_Info.at(CPE_Data_Loc).sectNum = 2;
										cout << "Salam333" << endl;
									}
							}
							else
							{
								for (int i=0; i<3; i++)
									if (CPEs_Info.at(CPE_Data_Loc).TxFreq == freq_sect_array[i])
										CPEs_Info.at(CPE_Data_Loc).sectNum = i+1;
							}

						}
						else if (tmp2.compare(0,rxFrequency_tag_begin.size(),rxFrequency_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxFreq = atof(tmp2.substr(rxFrequency_tag_begin.size(),
									tmp2.size()-rxFrequency_tag_begin.size()-rxFrequency_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,txAntennaHeight_tag_begin.size(),txAntennaHeight_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxAntennaHeight =atof(tmp2.substr(txAntennaHeight_tag_begin.size(),
									tmp2.size()-txAntennaHeight_tag_begin.size()-txAntennaHeight_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,rxAntennaHeight_tag_begin.size(),rxAntennaHeight_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxAntennaHeight =atof(tmp2.substr(rxAntennaHeight_tag_begin.size(),
									tmp2.size()-rxAntennaHeight_tag_begin.size()-rxAntennaHeight_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,txAntennaGain_tag_begin.size(),txAntennaGain_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxGain =atof(tmp2.substr(txAntennaGain_tag_begin.size(),tmp2.size()-txAntennaGain_tag_begin.size()-txAntennaGain_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,rxAntennaGain_tag_begin.size(),rxAntennaGain_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).RxGain =atof(tmp2.substr(rxAntennaGain_tag_begin.size(),tmp2.size()-rxAntennaGain_tag_begin.size()-rxAntennaGain_tag_end.size()).c_str());
						}
						else if (tmp2.compare(0,txPower_tag_begin.size(),txPower_tag_begin)==0)
						{
							CPEs_Info.at(CPE_Data_Loc).TxPower =atof(tmp2.substr(txPower_tag_begin.size(),tmp2.size()-txPower_tag_begin.size()-txPower_tag_end.size()).c_str());
							CPEs_Info.at(CPE_Data_Loc).TxPower = 10 * std::log (CPEs_Info.at(CPE_Data_Loc).TxPower) / 2.302 + 30;
						}
					}
					CPE_Cnt ++;
					//cout << "CPE_Info size=" << CPE_Info.size() << endl;
				}
			}
	}
}//XML_Read_Freq_Compatible


/*
int main() {
	cout << "!!!Hello World!!!" << endl; // prints !!!Hello World!!!
	vector<Ptr<BS_Data> > BS_Info;
	vector<Ptr<CPE_Data> > CPEs_Info;
	Xml_Read("20131030PTMP ALCONA WATER TREATMENT PLANT.xml", BS_Info, CPEs_Info);

	cout << BS_Info.size() << " BSs read, their information are:" << endl;
	for (unsigned int i=0;i<BS_Info.size();i++)
	{
		cout << i << ":" << endl;
		BS_Info[i]->print();
	}

	cout << CPEs_Info.size() << " CPESs read, their information are:" << endl;
	for (unsigned int i=0;i<CPEs_Info.size();i++)
	{
		cout << i << ":" << endl;
		CPEs_Info[i]->print();
	}


	return 0;
}
*/
}
