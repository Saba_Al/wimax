/*
 * Copyright (c) 2007,2008 INRIA
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Jahanzeb Farooq <jahanzeb.farooq@sophia.inria.fr>
 */

#include "bs-uplink-scheduler-simple.h"
#include "bs-net-device.h"
#include "ns3/simulator.h"
#include "cid.h"
#include "burst-profile-manager.h"
#include "ss-manager.h"
#include "ns3/log.h"
#include "ns3/uinteger.h"
#include "ss-record.h"
#include "service-flow.h"
#include "service-flow-record.h"
#include "bs-link-manager.h"
#include "bandwidth-manager.h"

NS_LOG_COMPONENT_DEFINE ("UplinkSchedulerSimple");

namespace ns3 {
NS_OBJECT_ENSURE_REGISTERED (UplinkSchedulerSimple);

UplinkSchedulerSimple::UplinkSchedulerSimple (void)
{
  SetBs (0);
  SetTimeStampIrInterval (Seconds (0));
  SetNrIrOppsAllocated (0);
  SetIsIrIntrvlAllocated (false);
  SetIsInvIrIntrvlAllocated (false);
  SetDcdTimeStamp (Simulator::Now ());
  SetUcdTimeStamp (Simulator::Now ());
}

UplinkSchedulerSimple::UplinkSchedulerSimple (Ptr<BaseStationNetDevice> bs)
{
  SetBs (bs);
  SetTimeStampIrInterval (Seconds (0));
  SetNrIrOppsAllocated (0);
  SetIsIrIntrvlAllocated (false);
  SetIsInvIrIntrvlAllocated (false);
  SetDcdTimeStamp (Simulator::Now ());
  SetUcdTimeStamp (Simulator::Now ());
}

UplinkSchedulerSimple::~UplinkSchedulerSimple (void)
{
  SetBs (0);
  m_uplinkAllocations.clear ();
}

void
UplinkSchedulerSimple::InitOnce ()
{

}

TypeId
UplinkSchedulerSimple::GetTypeId (void)
{
  static TypeId tid = TypeId ("ns3::UplinkSchedulerSimple").SetParent<Object> ();
  return tid;
}

std::list<OfdmUlMapIe>
UplinkSchedulerSimple::GetUplinkAllocations (void) const
{
  return m_uplinkAllocations;
}

void
UplinkSchedulerSimple::GetChannelDescriptorsToUpdate (bool &updateDcd,
                                                      bool &updateUcd,
                                                      bool &sendDcd,
                                                      bool &sendUcd)
{
  /*DCD and UCD shall actually be updated when channel or burst profile definitions
   change. burst profiles are updated based on number of SSs, network conditions and etc.
   for now temporarily assuming DCD/UCD shall be updated everytime */

  uint32_t randNr = rand ();
  if (randNr % 5 == 0 || GetBs ()->GetNrDcdSent () == 0)
    {
      sendDcd = true;
    }

  randNr = rand ();
  if (randNr % 5 == 0 || GetBs ()->GetNrUcdSent () == 0)
    {
      sendUcd = true;
    }

  // -------------------------------------
  // additional, just to send more frequently
  if (!sendDcd)
    {
      randNr = rand ();
      if (randNr % 4 == 0)
        {
          sendDcd = true;
        }
    }

  if (!sendUcd)
    {
      randNr = rand ();
      if (randNr % 4 == 0)
        {
          sendUcd = true;
        }
    }
  // -------------------------------------

  Time timeSinceLastDcd = Simulator::Now () - GetDcdTimeStamp ();
  Time timeSinceLastUcd = Simulator::Now () - GetUcdTimeStamp ();

  if (timeSinceLastDcd > GetBs ()->GetDcdInterval ())
    {
      sendDcd = true;
      SetDcdTimeStamp (Simulator::Now ());
    }

  if (timeSinceLastUcd > GetBs ()->GetUcdInterval ())
    {
      sendUcd = true;
      SetUcdTimeStamp (Simulator::Now ());
    }
}

uint32_t
UplinkSchedulerSimple::CalculateAllocationStartTime (void)
{
  return GetBs ()->GetNrDlSymbols () * GetBs ()->GetPhy ()->GetPsPerSymbol () + GetBs ()->GetTtg ();
}

void
UplinkSchedulerSimple::AddUplinkAllocation (OfdmUlMapIe &ulMapIe,
                                            const uint32_t &allocationSize,
                                            uint32_t &symbolsToAllocation,
                                            uint32_t &availableSymbols)
{
  ulMapIe.SetDuration (allocationSize);
  ulMapIe.SetStartTime (symbolsToAllocation);
  m_uplinkAllocations.push_back (ulMapIe);
  symbolsToAllocation += allocationSize;
  availableSymbols -= allocationSize;
}

void
UplinkSchedulerSimple::Schedule (void)
{
  m_uplinkAllocations.clear ();
  SetIsIrIntrvlAllocated (false);
  SetIsInvIrIntrvlAllocated (false);
  bool allocationForDsa = false;

  uint32_t symbolsToAllocation = 0;
  uint32_t allocationSize = 0; // size in symbols
  uint32_t availableSymbols = GetBs ()->GetNrUlSymbols ();
  uint32_t dlSymbols = GetBs ()->GetNrDlSymbols ();
  std::cout << "***BEGIN***UplinkSchedulerSimple::Schedule: Number of available UL symbols: "
  		<< availableSymbols << " Available DL symbols : " <<dlSymbols << ", Time:" << Simulator::Now().GetSeconds() << "s ************" << std::endl;

  AllocateInitialRangingInterval (symbolsToAllocation, availableSymbols);
  std::vector<SSRecord*> RTPSssRecords, NRTPSssRecords, BEssRecords;
  std::vector<SSRecord*> *ssRecords = GetBs ()->GetSSManager ()->GetSSRecords ();
  std::cout<< "ssRecords Size: " << ssRecords->size() << std::endl;
 // int num = 0;
  for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
    {
  //	std::cout << "Processing item " << num << std::endl;
  //	num++;
      SSRecord *ssRecord = *iter;

      if (ssRecord->GetIsBroadcastSS ())
        {
          continue;
        }
      Cid cid = ssRecord->GetBasicCid ();
      OfdmUlMapIe ulMapIe;
      ulMapIe.SetCid (cid);

      if (ssRecord->GetPollForRanging () && ssRecord->GetRangingStatus () == WimaxNetDevice::RANGING_STATUS_CONTINUE)
        {
      		std::cout<</*NS_LOG_DEBUG(*/"SS's ranging is not yet complete, allocating invited initial ranging interval"<<std::endl; //);
          // SS's ranging is not yet complete
          // allocating invited initial ranging interval
          ulMapIe.SetUiuc (OfdmUlBurstProfile::UIUC_INITIAL_RANGING);
          allocationSize = GetBs ()->GetRangReqOppSize ();
          SetIsInvIrIntrvlAllocated (true);

          if (availableSymbols >= allocationSize)
            {
          		std::cout << "calling AddUplinkAllocation" << ", allocation Size: " << allocationSize << std::endl;
              AddUplinkAllocation (ulMapIe, allocationSize, symbolsToAllocation, availableSymbols);
            }
          else
            {
          	//  std::cout << "not enough symbols to AddUplinkAllocation " << availableSymbols << "<" << allocationSize << std::endl;
              break;
            }
        }
      else
        {
          WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();

          // need to update because modulation/FEC to UIUC mapping may vary over time
          ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
                                                                                 WimaxNetDevice::DIRECTION_UPLINK));
          //std::cout<< "ssss" << std::endl;
          // establish service flows for SS
          if (ssRecord->GetRangingStatus () == WimaxNetDevice::RANGING_STATUS_SUCCESS
              && !ssRecord->GetAreServiceFlowsAllocated ())
            {
          		std::cout<</*NS_LOG_DEBUG(*/"allocating grant (with arbitrary size) to allow SS to send DSA messages DSA-REQ"<<std::endl; //);
              // allocating grant (with arbitrary size) to allow SS to send DSA messages DSA-REQ and DSA-ACK
              // only one DSA allocation per frame
              if (!allocationForDsa)
                {

                  allocationSize = GetBs ()->GetPhy ()->GetNrSymbols (sizeof(DsaReq), modulationType);
                  std::cout<<"dsa msg: availableSymbols=" << availableSymbols << ", allocationSize=" << allocationSize <<std::endl;
                  if (availableSymbols >= allocationSize)
                    {
                      AddUplinkAllocation (ulMapIe, allocationSize, symbolsToAllocation, availableSymbols);
                      allocationForDsa = true;
                    }
                  else
                    {
                      break;
                    }
                }
            }
          else
            {
              // all service flows associated to SS are established now
          	//	NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: all service flows associated to SS are established now");
          	//	NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
          	//	NS_LOG_DEBUG("calling ServiceUnsolicitedGrants (UGS)" << Simulator::Now ().GetSeconds ());
          		/*allocating grants for data transmission for UGS flows (Data Grant Burst Type IEs, 6.3.7.4.3.3)

               (grant has been referred by different names e.g. transmission opportunity, slot, uplink allocation, etc)*/
          		if (availableSymbols)
          	 {

          		//	std::cout << "At " << Simulator::Now () << std::endl;
          				NS_LOG_DEBUG("calling ServiceUnsolicitedGrants (UGS Grants)");
          		                  /*allocating grants for data transmission for UGS flows (Data Grant Burst Type IEs, 6.3.7.4.3.3)
          		                   (grant has been referred by different names e.g. transmission opportunity, slot,         uplink allocation, etc)*/
          			std::cout << "At " << Simulator::Now ().GetMilliSeconds() << std::endl;
          		     if (ssRecord->GetHasServiceFlowUgs ())
          		       	{
//          		    	 std::cout<< "SSSSSSSsssssalam! " << std:: endl;
          		    	 Time start_time =MilliSeconds((*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetStartTime());//start_time
          		    	 Time interval =  MilliSeconds((*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetUnsolicitedGrantInterval ());
          		    	 Time frame_duration = (GetBs ()->GetPhy ()->GetFrameDuration ());

          		    	// NS_ASSERT(false);

          		     	 if (Simulator::Now ()+frame_duration >= start_time)//
          		    	// if (((int)(Simulator::Now ().GetMilliSeconds ()) % start_time) <= 10	) //-frame_duration*1000
          		      	 {
          		     	std::cout << "At " << Simulator::Now ().GetSeconds () << ", Start_Time: " << start_time.GetSeconds() << ", interval: " << interval << std::endl;

												 ServiceUnsolicitedGrants (ssRecord,
																										ServiceFlow::SF_TYPE_UGS,
																										ulMapIe,
																										modulationType,
																										symbolsToAllocation,
																										availableSymbols);

          		     		 	 Time interval =  MilliSeconds((*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetUnsolicitedGrantInterval ());
          		     		   Time New_Start_Time = interval + start_time;//interval
          		     		 	 int Next_Start_Time = New_Start_Time.GetMilliSeconds();
          		     		// 	 std::cout << "At " << Simulator::Now ().GetMilliSeconds() << "SSSSSSserved. interval: " << interval.GetSeconds() << ", start_time: " << start_time.GetSeconds() << ", Next Start Time: " << Next_Start_Time << std::endl;
          		     		 	// NS_ASSERT(false);
												(*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->SetStartTime(Next_Start_Time);
												 if ((*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetMaxTrafficBurst() == 192)
												 {
														if (interval == 5)
															(*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->SetUnsolicitedGrantInterval (15);
														else if (interval == 15)
															(*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->SetUnsolicitedGrantInterval (5);
													}



          		      	 }

          		     		/* 	 std::cout << "First if, At: " << Simulator::Now().GetSeconds() << std::endl;
          		     																		 << "s, frame_duration= " << frame_duration.GetMicroSeconds()
          		     																		 << ", sum: " <<  Simulator::Now ().GetSeconds ()+frame_duration.GetSeconds()  << std::endl;
													Time interval =  MilliSeconds((*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetUnsolicitedGrantInterval ());

													Time
														timestamp =
														(*(ssRecord->GetServiceFlows (ServiceFlow::SF_TYPE_UGS).begin ()))->GetRecord ()->GetLastGrantTime ();

													Time a = NanoSeconds(interval);//-frame_duration
													Time b = (NanoSeconds(Simulator::Now ()-timestamp));

													if (a<= b)
														{
														//if (interval != MilliSeconds(10))
														std::cout << "At: " << Simulator::Now().GetSeconds() << "s, interval= " << interval.GetMilliSeconds()
																<< "ms, time stamp: " << timestamp.GetSeconds() << "s, frame_duration= " << frame_duration.GetMicroSeconds() <<
																"ms, a: " << a << ", b: " << b << std::endl;
													//	std ::cout << "YYYYYYYYYYYYYYYYYYYYYes" << std::endl;
														ServiceUnsolicitedGrants (ssRecord,
																ServiceFlow::SF_TYPE_UGS,
																ulMapIe,
																modulationType,
																symbolsToAllocation,
																availableSymbols);
														}
          		       	 }*/
          		    }//if (GetHasServiceFlowUGS)
          	 }//if(availablesymbols)

      /*    std::vector<SSRecord*>::iterator RTPSiter = RTPSssRecords.begin ();
          std::vector<SSRecord*>::iterator NRTPSiter = NRTPSssRecords.begin ();
          std::vector<SSRecord*>::iterator BEiter = BEssRecords.begin ();
          Ptr<UlJob> newJob =  CreateObject<UlJob> ();
          	if (ssRecord->GetHasServiceFlowRtps())
          	   {
										NS_LOG_DEBUG ("At " << Simulator::Now ().GetSeconds () << " offering rtps unicast polling");
										Ptr<UlJob> jobRTPSPoll = CreateUlJob (ssRecord, ServiceFlow::SF_TYPE_RTPS, UNICAST_POLLING);
										EnqueueJob (UlJob::HIGH, jobRTPSPoll);
          	    }
          	if (ssRecord->GetHasServiceFlowNrtps ())
								{
									NS_LOG_DEBUG ("At " << Simulator::Now ().GetSeconds () << " offering nrtps unicast polling");
									// allocate unicast polls for nrtPS flows if bandwidth is available
									Ptr<UlJob> jobNRTPSPoll = CreateUlJob (ssRecord, ServiceFlow::SF_TYPE_NRTPS, UNICAST_POLLING);
									EnqueueJob (UlJob::HIGH, jobNRTPSPoll);
								}
						 if (ssRecord->GetHasServiceFlowBe ())
						{
							NS_LOG_DEBUG ("At " << Simulator::Now ().GetSeconds () << " offering be unicast polling");
							// finally allocate unicast polls for BE flows if bandwidth is available
							Ptr<UlJob> jobBEPoll = CreateUlJob (ssRecord, ServiceFlow::SF_TYPE_BE, UNICAST_POLLING);
							EnqueueJob (UlJob::HIGH, jobBEPoll);
						}*/



/*          for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
             {
          	SSRecord *ssRecord = *iter;
          	WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
          	Cid cid = ssRecord->GetBasicCid ();
						OfdmUlMapIe ulMapIe;
						ulMapIe.SetCid (cid);
					  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
					                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // allocate unicast polls for rtPS flows if bandwidth is available
          		NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
              if (availableSymbols)
                {
             // 		NS_LOG_DEBUG("calling ServiceUnsolicitedGrants (RTPS polls)");
                  ServiceUnsolicitedGrants (ssRecord,
                                            ServiceFlow::SF_TYPE_RTPS,
                                            ulMapIe,
                                            modulationType,
                                            symbolsToAllocation,
                                            availableSymbols);
                }

          /*for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
					 {
          	SSRecord *ssRecord = *iter;
          	WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
          	Cid cid = ssRecord->GetBasicCid ();
          							OfdmUlMapIe ulMapIe;
          							ulMapIe.SetCid (cid);
          						  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
          						                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // allocate unicast polls for nrtPS flows if bandwidth is available
          		NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
              if (availableSymbols)
                {
           //   		NS_LOG_DEBUG("calling ServiceUnsolicitedGrants (NRTPS polls)");
                  ServiceUnsolicitedGrants (ssRecord,
                                            ServiceFlow::SF_TYPE_NRTPS,
                                            ulMapIe,
                                            modulationType,
                                            symbolsToAllocation,
                                            availableSymbols);
                }
					 //}
         /* for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
				 {
          SSRecord *ssRecord = *iter;
					WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
					Cid cid = ssRecord->GetBasicCid ();
											OfdmUlMapIe ulMapIe;
											ulMapIe.SetCid (cid);
										  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
										                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // finally allocate unicast polls for BE flows if bandwidth is available
              NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
              if (availableSymbols)
                {
             // 	NS_LOG_DEBUG("calling ServiceUnsolicitedGrants (BE polls)");
                  ServiceUnsolicitedGrants (ssRecord,
                                            ServiceFlow::SF_TYPE_BE,
                                            ulMapIe,
                                            modulationType,
                                            symbolsToAllocation,
                                            availableSymbols);
                }
				// }

        /*  for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
					 {
          	SSRecord *ssRecord = *iter;
          	WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
          	Cid cid = ssRecord->GetBasicCid ();
          							OfdmUlMapIe ulMapIe;
          							ulMapIe.SetCid (cid);
          						  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
          						                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // now allocating grants for non-UGS flows (i.e., in response of bandwidth requests)
              NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: now allocating grants for non-UGS flows (i.e., in response of bandwidth requests). available symbols= " << availableSymbols);
              if (availableSymbols)
							{
              		NS_LOG_DEBUG("calling ServiceBandwidthRequests (RTPS)");
                  ServiceBandwidthRequests (ssRecord,
                                            ServiceFlow::SF_TYPE_RTPS,
                                            ulMapIe,
                                            modulationType,
                                            symbolsToAllocation,
                                            availableSymbols);
							}
					// }
         /* for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
					 {
          	SSRecord *ssRecord = *iter;
          	WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
          	Cid cid = ssRecord->GetBasicCid ();
          							OfdmUlMapIe ulMapIe;
          							ulMapIe.SetCid (cid);
          						  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
          						                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // allocate unicast polls for nrtPS flows if bandwidth is available
              NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
              if (availableSymbols)
							{
              	NS_LOG_DEBUG("calling ServiceBandwidthRequests (NRTPS)");
								ServiceBandwidthRequests (ssRecord,
																					ServiceFlow::SF_TYPE_NRTPS,
																					ulMapIe,
																					modulationType,
																					symbolsToAllocation,
																					availableSymbols);
							}
					// }
         /* for (std::vector<SSRecord*>::iterator iter = ssRecords->begin (); iter != ssRecords->end (); ++iter)
					 {
          	SSRecord *ssRecord = *iter;
          	WimaxPhy::ModulationType modulationType = ssRecord->GetModulationType ();
          	Cid cid = ssRecord->GetBasicCid ();
          							OfdmUlMapIe ulMapIe;
          							ulMapIe.SetCid (cid);
          						  ulMapIe.SetUiuc (GetBs ()->GetBurstProfileManager ()->GetBurstProfile (modulationType,
          						                                                                                 WimaxNetDevice::DIRECTION_UPLINK));*/
              // finally allocate unicast polls for BE flows if bandwidth is available
              NS_LOG_DEBUG("UplinkSchedulerSimple::Schedule: available symbols= " << availableSymbols);
              if (availableSymbols)
                {
              	NS_LOG_DEBUG("calling ServiceBandwidthRequests (BE)");
                  ServiceBandwidthRequests (ssRecord,
                                            ServiceFlow::SF_TYPE_BE,
                                            ulMapIe,
                                            modulationType,
                                            symbolsToAllocation,
                                            availableSymbols);
                }
			//		 }
        			}//else

        }//else ranging status success
     }//end for ssRecords

  OfdmUlMapIe ulMapIeEnd;

  ulMapIeEnd.SetCid (Cid::InitialRanging ());
  ulMapIeEnd.SetStartTime (symbolsToAllocation);
  ulMapIeEnd.SetUiuc (OfdmUlBurstProfile::UIUC_END_OF_MAP);
  ulMapIeEnd.SetDuration (0);
  m_uplinkAllocations.push_back (ulMapIeEnd);

  // setting DL/UL subframe allocation for the next frame
 // GetBs ()->GetBandwidthManager ()->SetSubframeRatio ();
}

void
UplinkSchedulerSimple::ServiceUnsolicitedGrants (const SSRecord *ssRecord,
                                                 enum ServiceFlow::SchedulingType schedulingType,
                                                 OfdmUlMapIe &ulMapIe,
                                                 const WimaxPhy::ModulationType modulationType,
                                                 uint32_t &symbolsToAllocation,
                                                 uint32_t &availableSymbols)
{
	/*if (availableSymbols > 360)
		std::cout << "UplinkSchedulerSimple::ServiceUnsolicitedGrants, AvailableSymbols=" << availableSymbols << std::endl;*/
  uint32_t allocationSize = 0; // size in symbols
  uint8_t uiuc = ulMapIe.GetUiuc (); // SS's burst profile
  std::vector<ServiceFlow*> serviceFlows = ssRecord->GetServiceFlows (schedulingType);
/*
  std::cout<< "service flow size: " << serviceFlows.size() << std::endl;
  NS_ASSERT(serviceFlows.size()<=30);
*/

  for (std::vector<ServiceFlow*>::iterator iter = serviceFlows.begin (); iter != serviceFlows.end (); ++iter)
  {
 // 	std::cout << "AAAAAAAAAAAAAAAAAaa, simple scheduler called" << std::endl;
      ServiceFlow *serviceFlow = *iter;
      if (serviceFlow->GetDirection() == 1)
      {

					/* in case of rtPS, nrtPS and BE, allocating unicast polls for bandwidth requests (Request IEs, 6.3.7.4.3.1).
					 in case of UGS, allocating grants for data transmission (Data Grant Burst Type IEs, 6.3.7.4.3.3) (grant has
					 been referred in this code by different names e.g. transmission opportunity, slot, allocation, etc) */

					allocationSize = GetBs ()->GetBandwidthManager ()->CalculateAllocationSize (ssRecord, serviceFlow);

			 /*   if (serviceFlow->GetSchedulingType () == ServiceFlow::SF_TYPE_UGS && serviceFlow->GetSDUsize() == 240)
					{	serviceFlow->SetUnsolicitedGrantInterval(serviceFlow->GetMinTolerableTrafficRate()-10);
						std::cout << "At" << Simulator::Now().GetSeconds() << ", interval changed: " << serviceFlow->GetUnsolicitedGrantInterval() << std::endl;
					}*/

					// verifying that minimum reserved traffic rate of nrtPS flow is maintained
					if (serviceFlow->GetSchedulingType () == ServiceFlow::SF_TYPE_NRTPS)
						{
							Time currentTime = Simulator::Now ();
							ServiceFlowRecord *record = serviceFlow->GetRecord ();
							if (currentTime - record->GetGrantTimeStamp () > Seconds (1))
								{
									uint32_t bps = (record->GetBwSinceLastExpiry () * 8);
									if (bps < serviceFlow->GetMinReservedTrafficRate ())
										{
											ServiceBandwidthRequests (serviceFlow,
																								schedulingType,
																								ulMapIe,
																								modulationType,
																								symbolsToAllocation,
																								availableSymbols);
											record->SetBwSinceLastExpiry (0);
											record->SetGrantTimeStamp (currentTime);
										}
								}
						}

					if (availableSymbols < allocationSize)
						{
							allocationSize = availableSymbols;
							//break;

						/*//	allocationSize = availableSymbols;
							//NS_ASSERT(false);
							std::cout << "&&&&&&&&&&&&&&&&&&&availableSymbols " << availableSymbols << " Allocation Size " << allocationSize;
							NS_ASSERT(false);*/

						}

					if (allocationSize > 0)
						{
					//	std::cout << "At: " << Simulator::Now() << ", SFType: " << serviceFlow->GetSchedulingType () << " , " << allocationSize << ", SFID: " << serviceFlow->GetSfid () << std::endl;
							ulMapIe.SetStartTime (symbolsToAllocation);
						 // std::cout << "GGGGGGGGGGGGGrant Start Time: " << symbolsToAllocation << std::endl;
							if (serviceFlow->GetSchedulingType () != ServiceFlow::SF_TYPE_UGS)
								{
									// special burst profile with most robust modulation type is used for unicast polls (Request IEs)
									ulMapIe.SetUiuc (OfdmUlBurstProfile::UIUC_REQ_REGION_FULL);
								}
						}
					else
						{
							continue;
						}
					if (serviceFlow->GetSchedulingType () == ServiceFlow::SF_TYPE_UGS)
					{
						NS_LOG_DEBUG ("BS uplink scheduler, UGS allocation, size: " << allocationSize << " symbols, direction="
								<< serviceFlow->GetDirection() << " sfid="
								<< serviceFlow->GetSfid() << ", startTime=" << serviceFlow->GetStartTime());
					}
				else
					{
						NS_LOG_DEBUG ("BS uplink scheduler, " << serviceFlow->GetSchedulingTypeStr () << " unicast poll, size: "
																									<< allocationSize << " symbols" << ", modulation: BPSK 1/2");
					}
				/*  std::cout << NS_LOG_DEBUG ("At:" << Simulator::Now().GetSeconds() << "s, CID: " << serviceFlow->GetConnection ()->GetCid () <<
							", SFID: " << serviceFlow->GetSfid () << std::endl; //);*/
					serviceFlow->GetRecord ()->SetLastGrantTime (Simulator::Now ());
					AddUplinkAllocation (ulMapIe, allocationSize, symbolsToAllocation, availableSymbols);
					ulMapIe.SetUiuc (uiuc);
      }//end if (Direction)
    }//end for
}

void
UplinkSchedulerSimple::ServiceBandwidthRequests (const SSRecord *ssRecord,
                                                 enum ServiceFlow::SchedulingType schedulingType,
                                                 OfdmUlMapIe &ulMapIe,
                                                 const WimaxPhy::ModulationType modulationType,
                                                 uint32_t &symbolsToAllocation,
                                                 uint32_t &availableSymbols)
{
  std::vector<ServiceFlow*> serviceFlows = ssRecord->GetServiceFlows (schedulingType);

  for (std::vector<ServiceFlow*>::iterator iter = serviceFlows.begin (); iter != serviceFlows.end (); ++iter)
    {
      if (!ServiceBandwidthRequests (*iter,
                                     schedulingType,
                                     ulMapIe,
                                     modulationType,
                                     symbolsToAllocation,
                                     availableSymbols))
        {
          break;
        }
    }
}

bool
UplinkSchedulerSimple::ServiceBandwidthRequests (ServiceFlow *serviceFlow,
                                                 enum ServiceFlow::SchedulingType schedulingType,
                                                 OfdmUlMapIe &ulMapIe,
                                                 const WimaxPhy::ModulationType modulationType,
                                                 uint32_t &symbolsToAllocation,
                                                 uint32_t &availableSymbols)
{

	 uint32_t allocSizeBytes = 0;
	  uint32_t allocSizeSymbols = 0;
	 // uint16_t sduSize = 0;

	  ServiceFlowRecord *record = serviceFlow->GetRecord ();
	//  sduSize = serviceFlow->GetSduSize ();

	  uint32_t requiredBandwidth = record->GetRequestedBandwidth () - record->GetGrantedBandwidth ();

	  if (requiredBandwidth > 0)
	    {
	  	/*	std::cout << "UplinkSchedulerSimple::ServiceBandwidthRequests modulationType="
	  	  		<< modulationType << ", requiredBandwidth="<< requiredBandwidth <<
	  	  		", sduSize=" << sduSize <<std::endl;*/
	     /* if (sduSize > 0 && schedulingType == ServiceFlow::SF_TYPE_RTPS)
	        {
	          // if SDU size is mentioned, allocate grant of that size
	          allocSizeBytes = sduSize;
	          allocSizeSymbols = GetBs ()->GetPhy ()->GetNrSymbols (sduSize, modulationType);
	        }
	      else
	        {*/
	          allocSizeBytes = requiredBandwidth;
	          allocSizeSymbols = GetBs ()->GetPhy ()->GetNrSymbols (requiredBandwidth, modulationType);
	       // }

	      if (availableSymbols < allocSizeSymbols)
	      {
	      		allocSizeSymbols = availableSymbols;
	      		//std::cout << "mm333" << std::endl;
	      		allocSizeBytes = GetBs()->GetPhy()->GetNrBytes (availableSymbols, modulationType);
	      		//std::cout << "nn444" << std::endl;
	      		/*if(allocSizeBytes > serviceFlow->GetMinReservedTrafficRate())
	      			{
	      				allocSizeBytes = allocSizeBytes - allocSizeBytes % serviceFlow->GetMinReservedTrafficRate();
	      				allocSizeSymbols = GetBs ()->GetPhy ()->GetNrSymbols (allocSizeBytes, modulationType);
	      			}
	      		else
	      			{
	      				allocSizeBytes = 0;
	      				allocSizeSymbols = 0;
	      			}*/
	       }

	      if (availableSymbols >= allocSizeSymbols /*&& allocSizeSymbols > 0*/)
	        {
	      //	/*NS_LOG_DEBUG(*/std::cout<< "*******************Fariba2 :" << schedulingType << ", " << allocSizeSymbols << std::endl;//);
	          record->UpdateGrantedBandwidth (allocSizeBytes);

	          if (schedulingType == ServiceFlow::SF_TYPE_NRTPS)
	            {
	              record->SetBwSinceLastExpiry (allocSizeBytes);
	            }

	          AddUplinkAllocation (ulMapIe, allocSizeSymbols, symbolsToAllocation, availableSymbols);

	        }
	      else
	        {
	          return false;
	        }
	    }
	  return true;
}

void
UplinkSchedulerSimple::AllocateInitialRangingInterval (uint32_t &symbolsToAllocation, uint32_t &availableSymbols)
{
  Time ssUlStartTime = Seconds (CalculateAllocationStartTime () * GetBs ()->GetPsDuration ().GetSeconds ());
  SetNrIrOppsAllocated (GetBs ()->GetLinkManager ()->CalculateRangingOppsToAllocate ());
  uint32_t allocationSize = GetNrIrOppsAllocated () * GetBs ()->GetRangReqOppSize ();
  Time timeSinceLastIrInterval = Simulator::Now () - GetTimeStampIrInterval ();

  // adding one frame because may be the time has not elapsed now but will elapse before the next frame is sent
  if (timeSinceLastIrInterval + GetBs ()->GetPhy ()->GetFrameDuration () > GetBs ()->GetInitialRangingInterval ()
      && availableSymbols >= allocationSize)
    {
      SetIsIrIntrvlAllocated (true);
      OfdmUlMapIe ulMapIeIr;
      ulMapIeIr.SetCid ((GetBs ()->GetBroadcastConnection ())->GetCid ());
      ulMapIeIr.SetStartTime (symbolsToAllocation);
      ulMapIeIr.SetUiuc (OfdmUlBurstProfile::UIUC_INITIAL_RANGING);

      NS_LOG_DEBUG ("BS uplink scheduler, initial ranging allocation, size: " << allocationSize << " symbols"
                                                                              << ", modulation: BPSK 1/2");

      // marking start and end of each TO, only for debugging
      for (uint8_t i = 0; i < GetNrIrOppsAllocated (); i++)
        {
          GetBs ()->MarkRangingOppStart (ssUlStartTime + Seconds (symbolsToAllocation
                                                                  * GetBs ()->GetSymbolDuration ().GetSeconds ()) + Seconds (i * GetBs ()->GetRangReqOppSize ()
                                                                                                                             * GetBs ()->GetSymbolDuration ().GetSeconds ()));
        }

      AddUplinkAllocation (ulMapIeIr, allocationSize, symbolsToAllocation, availableSymbols);
      SetTimeStampIrInterval (Simulator::Now ());
    }
}

void
UplinkSchedulerSimple::SetupServiceFlow (SSRecord *ssRecord, ServiceFlow *serviceFlow)
{
 // uint8_t delayNrFrames = 1;
  uint32_t bitsPerSecond = serviceFlow->GetMinReservedTrafficRate ();
 // std::cout << "mmmmmmmmmmmmmin reserved: " << serviceFlow->GetMinReservedTrafficRate () <<std::endl;
  WimaxPhy::ModulationType modulation;
  uint32_t bytesPerFrame =
  uint32_t (std::ceil((double)(bitsPerSecond) * GetBs ()->GetPhy ()->GetFrameDuration ().GetSeconds () / 8));
//  Ptr<WimaxConnection> connection = serviceFlow->GetConnection ();
//  uint32_t FirstPacketSize = connection->GetQueue ()->GetFirstPacketHdrSize (MacHeaderType::HEADER_TYPE_GENERIC);
//  bytesPerFrame = (192+FirstPacketSize)*2;

  if (serviceFlow->GetSchedulingType() == ServiceFlow::SF_TYPE_UGS)
    {
    //	std::cout << "serviceFlow->GetSduSize()=" << (int) serviceFlow->GetSduSize() << ", FirstPacketSize=" << FirstPacketSize << std::endl;
    	std::cout << "bytes Per Frame: " << bytesPerFrame << std::endl;
    	std::cout << "serviceFlow->GetMaxTrafficBurst()=" << (int) serviceFlow->GetMaxTrafficBurst() << ", with interval = " << serviceFlow->GetUnsolicitedGrantInterval() << ", FrameDur=" << GetBs ()->GetPhy ()->GetFrameDuration ().GetMilliSeconds() << std::endl;
    	//NS_ASSERT(false);
    	if (serviceFlow->GetUnsolicitedGrantInterval() >= GetBs ()->GetPhy ()->GetFrameDuration ().GetMilliSeconds())
    		{
  				bytesPerFrame = serviceFlow->GetMaxTrafficBurst();
  				std::cout << "Yyyyyyyyyyyyyyes: bytes Per Frame: " << bytesPerFrame << std::endl;
  				/*if (bytesPerFrame == 192)
  					bytesPerFrame *= 2;*/
  			}

    }

 // uint32_t frameDurationMSec = GetBs ()->GetPhy ()->GetFrameDuration ().GetMilliSeconds ();

  switch (serviceFlow->GetSchedulingType ())
    {
    case ServiceFlow::SF_TYPE_UGS:
      {
        if (serviceFlow->GetIsMulticast () == true)
          {
            modulation = serviceFlow->GetModulation ();
          }
        else
          {
            modulation = ssRecord->GetModulationType ();
          }
        uint32_t grantSize = GetBs ()->GetPhy ()->GetNrSymbols (bytesPerFrame, modulation);

        if (bytesPerFrame==384)
        	grantSize += 1;//5 //3; //6; //8;
      //  else
        	if (serviceFlow->GetDirection() == 0)
        	grantSize = 9; //9;

        if (modulation > 0)
        	{
        		std::cout << "BBBBBBBBBBBBBBBBBBBBBBBBBBbBytes Per Frame: " << bytesPerFrame << ", Grant Size:"
        				<< grantSize << ", modulation type: " << modulation << "direction=:" << serviceFlow->GetDirection() << std::endl;
        	/*	if (serviceFlow->GetDirection()==0)
        			NS_ASSERT(false);*/
        	}

        serviceFlow->GetRecord ()->SetGrantSize (grantSize);

      //  uint32_t toleratedJitter = serviceFlow->GetToleratedJitter ();

       /* if (toleratedJitter > frameDurationMSec)
          {
            delayNrFrames = (uint8_t)(toleratedJitter / frameDurationMSec);
          }*/

//        uint16_t interval = delayNrFrames * frameDurationMSec;
    //    serviceFlow->SetUnsolicitedGrantInterval (interval);
     //   std::cout << "interval=" << interval <<std::endl;
      }
      break;
    case ServiceFlow::SF_TYPE_RTPS:
      {
        /*if (serviceFlow->GetSduSize () > bytesPerFrame)
          {
            delayNrFrames = (uint8_t)(serviceFlow->GetSduSize () / bytesPerFrame);
          }

        uint16_t interval = delayNrFrames * frameDurationMSec;
        serviceFlow->SetUnsolicitedPollingInterval (interval);*/
      	serviceFlow->SetUnsolicitedPollingInterval (5);
      }
      break;
    case ServiceFlow::SF_TYPE_NRTPS:
      {
      	uint16_t interval = 1000;
				serviceFlow->SetUnsolicitedPollingInterval (interval);
        // no real-time guarantees are given to NRTPS, serviced based on available bandwidth
      }
      break;
    case ServiceFlow::SF_TYPE_BE:
      {
        // no real-time guarantees are given to BE, serviced based on available bandwidth
      }
      break;
    default:
      NS_FATAL_ERROR ("Invalid scheduling type");
    }
}

void
UplinkSchedulerSimple::ProcessBandwidthRequest (const BandwidthRequestHeader &bwRequestHdr)
{
  // virtual function on UplinkScheduler
  // this is not necessary on this implementation
}

void
UplinkSchedulerSimple::OnSetRequestedBandwidth (ServiceFlowRecord *sfr)
{
  // m_grantedBandwidth must be reset to zero
  uint32_t grantedBandwidth = 0;
  sfr->SetGrantedBandwidth (grantedBandwidth);
}

} // namespace ns3



