/*
 * CVS_Reading.cc
 *
 *  Created on: Apr 10, 2014
 *      Author: faalamifar
 */
#ifndef CSV_F
#define CSV_F

#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include <iostream>
#include <vector>

using namespace std;
using namespace ns3;

std::map <const char*,ServiceFlow::SchedulingType> CSV_Reading(const char* , vector<Node_Data>,
		std::map <const char*,double>&);

void Read_New_Sites(const char*, vector<Node_Data> &);

#endif
