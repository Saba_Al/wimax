/* -*- Mode:C++; c-file-style:"gnu"; indent-tabs-mode:nil; -*- */
/*
 *  Copyright (c) 2007,2008, 2009 INRIA, UDcast
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License version 2 as
 * published by the Free Software Foundation;
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 * Author: Mohamed Amine Ismail <amine.ismail@sophia.inria.fr>
 *                              <amine.ismail@udcast.com>
 */
#include "ns3/core-module.h"
#include "ns3/network-module.h"
#include "ns3/applications-module.h"
#include "ns3/mobility-module.h"
#include "ns3/config-store-module.h"
#include "ns3/wimax-module.h"
#include "ns3/internet-module.h"
#include "ns3/global-route-manager.h"
#include "ns3/ipcs-classifier-record.h"
#include "ns3/service-flow.h"
#include "ns3/point-to-point-helper.h"
#include "ns3/cost231-propagation-loss-model.h"
#include "ns3/XML_Read.h"
#include "ns3/cost-Erceg.h"
#include <iostream>
#include <vector>
#include "ns3/CSV_Reading.h"
#include <string>     // std::string, std::to_string

#define MAX_APPS 50
#define FLOW_NUM 10
NS_LOG_COMPONENT_DEFINE("WimaxCapacityExample");

using namespace std;
using namespace ns3;

//UDP Traffic
//Uplink
int setup_udp1(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		double interval, int pkt_size, int startPort, WimaxHelper wimaxAlcona, WimaxPhy::ModulationType  modulation
		,UdpServerHelper* udpServer) {

	if (nbSS==0)
		return startPort;
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	UdpClientHelper* udpClient = new UdpClientHelper[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	double startTime = 2;
	double stopTime = duration; //100; //7;
	NS_LOG_DEBUG("stopTime: " << stopTime);
	for (int i = 0; i < nbSS; i++) {
		port = startPort + (i * 10);
		// set server port to 100+(i*10)
		udpServer[i] = UdpServerHelper(port);
		NS_LOG_DEBUG("port = " << port);
		serverApps[i] = udpServer[i].Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(duration+50));//duration//110

		udpClient[i] = UdpClientHelper(BSinterface.GetAddress(0), port);
		udpClient[i].SetAttribute("MaxPackets", UintegerValue(1000000000));
		udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		udpClient[i].SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i].SetAttribute("FlowId",UintegerValue(fid));

		clientApps[i] = udpClient[i].Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(startTime));
		clientApps[i].Stop(Seconds(stopTime));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	for (int i = 0; i < nbSS; i++) {
					IpcsClassifierRecord ulClassifierBe(
					SSinterfaces.GetAddress(i),
					Ipv4Mask("255.255.255.255"),Ipv4Address("0.0.0.0") ,
					Ipv4Mask("0.0.0.0"),
					0, 65000, startPort + (i * 10),
					startPort + (i * 10), 17, 1);
					NS_LOG_DEBUG("BS Address" << BSinterface.GetAddress(0));
			ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
					ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_UGS,
					ulClassifierBe);
			NS_LOG_DEBUG("SSinterface Address " << i << " = " << SSinterfaces.GetAddress(i));
		//	ulServiceFlowBe.SetMaximumLatency(10);
			ulServiceFlowBe.SetMinReservedTrafficRate(500000);//(double)pkt_size*12/(double)interval//400000
			//ulServiceFlowBe.SetModulation(modulation);
			ss[i]->AddServiceFlow(ulServiceFlowBe);
		}
	return port+10;
}

int setup_udp2(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		double interval, int pkt_size, int startPort, WimaxHelper wimaxAlcona,
		WimaxPhy::ModulationType modulation, UdpServerHelper* udpServer) {

	if (nbSS==0)
		return startPort;
	//UdpServerHelper* udpServer = new UdpServerHelper[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	UdpClientHelper* udpClient = new UdpClientHelper[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	double startTime = 2;
	double stopTime = duration; //7;//100
	for (int i = 0; i < nbSS; i++) {
		port = startPort + (i * 10);
		// set server port to 100+(i*10)
		udpServer[i] = UdpServerHelper(port);
		serverApps[i] = udpServer[i].Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(duration+50));//300

		udpClient[i] = UdpClientHelper(BSinterface.GetAddress(0), port);
		udpClient[i].SetAttribute("MaxPackets", UintegerValue(1000000000));
		udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		udpClient[i].SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i].SetAttribute("FlowId",UintegerValue(fid));

		clientApps[i] = udpClient[i].Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(startTime));
		clientApps[i].Stop(Seconds(stopTime));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	for (int i = 0; i < nbSS; i++) {
		IpcsClassifierRecord ulClassifierBe(
			SSinterfaces.GetAddress(i),
			Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),//
			Ipv4Mask("0.0.0.0"), 0, 65000, startPort + (i * 10),
			startPort + (i * 10), 17, 1
		);
  		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
			ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_BE,ulClassifierBe
		);
		ulServiceFlowBe.SetMaximumLatency(100);
	//	ulServiceFlowBe.SetMinReservedTrafficRate((double)pkt_size*12/interval);
	//	ulServiceFlowBe.SetModulation(modulation);
		ss[i]->AddServiceFlow(ulServiceFlowBe);

	}
	return port+10;
}
/*
int setup_udp3(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		double interval, int pkt_size, int startPort, WimaxHelper wimaxAlcona,
		WimaxPhy::ModulationType modulation, UdpServerHelper* udpServer) {

	//UdpServerHelper* udpServer = new UdpServerHelper[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	UdpClientHelper* udpClient = new UdpClientHelper[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	double startTime = 2;
	double stopTime = duration; //100; //7;
	for (int i = 0; i < nbSS; i++) {
		port = startPort + (i * 10);
		// set server port to 100+(i*10)
		udpServer[i] = UdpServerHelper(port);
		serverApps[i] = udpServer[i].Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(110));//duration

		udpClient[i] = UdpClientHelper(BSinterface.GetAddress(0), port);
		udpClient[i].SetAttribute("MaxPackets", UintegerValue(1000000000));
		udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		udpClient[i].SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i].SetAttribute("FlowId",UintegerValue(fid));

		clientApps[i] = udpClient[i].Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(startTime));
		clientApps[i].Stop(Seconds(stopTime));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	for (int i = 0; i < nbSS; i++) {
				IpcsClassifierRecord ulClassifierBe(
				SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + (i * 10),
				startPort + (i * 10), 17, 1);
		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_RTPS,
				ulClassifierBe);
		ulServiceFlowBe.SetModulation(modulation);
		ss[i]->AddServiceFlow(ulServiceFlowBe);
	}
	return port+10;
}
*/

int setup_udp4(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		double interval, int pkt_size, int startPort, WimaxHelper wimaxAlcona,
		WimaxPhy::ModulationType modulation, UdpServerHelper* udpServer) {

	if (nbSS==0)
		return startPort;
	//UdpServerHelper* udpServer = new UdpServerHelper[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	UdpClientHelper* udpClient = new UdpClientHelper[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
	double startTime = 2;
	double stopTime = duration; //100; //7;
	for (int i = 0; i < nbSS; i++) {
		port = startPort + (i * 10);
		// set server port to 100+(i*10)
		udpServer[i] = UdpServerHelper(port);
		serverApps[i] = udpServer[i].Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(110));//duration

		udpClient[i] = UdpClientHelper(BSinterface.GetAddress(0), port);
		udpClient[i].SetAttribute("MaxPackets", UintegerValue(1000000000));
		udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		udpClient[i].SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i].SetAttribute("FlowId",UintegerValue(fid));

		clientApps[i] = udpClient[i].Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(startTime));
		clientApps[i].Stop(Seconds(stopTime));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	for (int i = 0; i < nbSS; i++) {
				IpcsClassifierRecord ulClassifierBe(
				SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + (i * 10),
				startPort + (i * 10), 17, 1);
		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_NRTPS,
				ulClassifierBe);
		ulServiceFlowBe.SetModulation(modulation);
		ss[i]->AddServiceFlow(ulServiceFlowBe);
	}
	return port+10;
}

//Uplink
int setup_udp_onoff(int nbSS, int fid, NodeContainer ssNodes,
		Ipv4InterfaceContainer SSinterfaces, Ptr<SubscriberStationNetDevice>  ss[], NodeContainer bsNode,
		Ipv4InterfaceContainer BSinterface, double duration,
		string on_time, string off_time, int pkt_size, string data_rate,
		int startPort, WimaxHelper wimaxAlcona, WimaxPhy::ModulationType modulation) {
		//,PacketSinkHelper* udpServer

	if (nbSS==0)
		return startPort;
	PacketSinkHelper* udpServer[nbSS];
	ApplicationContainer* serverApps = new ApplicationContainer[nbSS];
	OnOffHelper* udpClient[nbSS];
	ApplicationContainer* clientApps = new ApplicationContainer[nbSS];
	int port;
/*	double startTime = 2;*/
	double stopTime = duration;
	for (int i = 0; i < nbSS; i++) {
		port = startPort + (i * 10);
		// set server port to 100+(i*10)
		Address sinkLocalAddress (InetSocketAddress (Ipv4Address::GetAny (), port));
		udpServer[i] = new PacketSinkHelper("ns3::UdpSocketFactory", sinkLocalAddress);;
		serverApps[i] = udpServer[i]->Install(bsNode.Get(0));
		serverApps[i].Start(Seconds(0.0));
		serverApps[i].Stop(Seconds(duration+50));//110


		udpClient[i] = new OnOffHelper("ns3::UdpSocketFactory",Address());
		AddressValue remoteAddress (InetSocketAddress (BSinterface.GetAddress(0), port));
		udpClient[i]->SetAttribute ("Remote", remoteAddress);
		//udpClient[i]->SetAttribute("MaxPackets", UintegerValue(1200));
		//udpClient[i].SetAttribute("Interval", TimeValue(Seconds(interval))); //1
		udpClient[i]->SetConstantRate (DataRate (data_rate));
		udpClient[i]->SetAttribute("PacketSize", UintegerValue(pkt_size)); //128
		udpClient[i]->SetAttribute("FlowId",UintegerValue(fid));
		udpClient[i]->SetAttribute	("OnTime", StringValue (on_time));
		udpClient[i]->SetAttribute	("OffTime", StringValue (off_time));
		NS_LOG_DEBUG("port = " << port);

		clientApps[i] = udpClient[i]->Install(ssNodes.Get(i));
		clientApps[i].Start(Seconds(2));
		clientApps[i].Stop(Seconds(stopTime));
	//	startTime = startTime + 6;
		//stopTime = startTime + 5;
	}
	for (int i = 0; i < nbSS; i++) {
				IpcsClassifierRecord ulClassifierBe(
				SSinterfaces.GetAddress(i),
				Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
				Ipv4Mask("0.0.0.0"), 0, 65000, startPort + (i * 10),
				startPort + (i * 10), 17, 1);
		ServiceFlow ulServiceFlowBe = wimaxAlcona.CreateServiceFlow(
				ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_UGS,
				ulClassifierBe);
		ulServiceFlowBe.SetMaximumLatency(10);
   	ulServiceFlowBe.SetMinReservedTrafficRate(504800);
   //	ulServiceFlowBe.SetMinTolerableTrafficRate(2000000);
   	ulServiceFlowBe.SetToleratedJitter(10);
		ulServiceFlowBe.SetModulation(modulation);
		ss[i]->AddServiceFlow(ulServiceFlowBe);
	}
	return port+10;
}

int main(int argc, char *argv[]) {
	int schedType = 0, simulation_time = 200, duration = 200; //60; //dlIntegStTime = 2.0,

		LogComponentEnable("WimaxCapacityExample", LOG_LEVEL_DEBUG);
//	LogComponentEnable ("OnOffApplication", LOG_LEVEL_ALL); //TCP
	//LogComponentEnable ("BulkSendApplication", LOG_LEVEL_ALL); //TCP
	//LogComponentEnable ("PacketSink", LOG_LEVEL_ALL); //TCP Sink
	//LogComponentEnable ("Cost231PropagationLossModel", LOG_LEVEL_ALL);
	//LogComponentEnable ("WimaxHelper", LOG_LEVEL_ALL);
	//LogComponentEnable ("Ipv4AddressHelper",LOG_LEVEL_ALL);
	//LogComponentEnable("SimpleOfdmWimaxPhy", LOG_LEVEL_DEBUG);///for SNRs
	//LogComponentEnable ("WimaxConnection",LOG_LEVEL_ALL);
	//LogComponentEnable ("SubscriberStationNetDevice",LOG_LEVEL_ALL);
	//LogComponentEnable ("BaseStationNetDevice",LOG_LEVEL_ALL);
//	LogComponentEnable ("SSLinkManager",LOG_LEVEL_ALL);
//	LogComponentEnable("UdpClient", LOG_LEVEL_ALL);
	//LogComponentEnable("SNRToBlockErrorRateManager",LOG_LEVEL_ALL);
	//LogComponentEnable("UdpServer", LOG_LEVEL_ALL);
	//LogComponentEnableAll(LOG_LEVEL_FUNCTION);
	LogComponentEnable ("UplinkSchedulerMBQoS", LOG_LEVEL_DEBUG);
	//	LogComponentEnable ("UplinkSchedulerSimple", LOG_LEVEL_DEBUG);
	//	LogComponentEnable("SSScheduler",LOG_LEVEL_ALL);
	WimaxHelper::SchedulerType scheduler;// = WimaxHelper::SCHED_TYPE_MBQOS;
	WimaxPhy::ModulationType modulation = WimaxPhy::MODULATION_TYPE_QAM64_34; //MODULATION_TYPE_QAM64_34;//MODULATION_TYPE_QAM16_12

	vector<Node_Data> BS_Info;
	vector<Node_Data> CPEs_Info;

	Xml_Read_Freq_Compatible("20140519PTMP LakeshoreWPCP.xml", BS_Info,	CPEs_Info);
	Read_New_Sites("NewSites19Aug2014.csv",CPEs_Info);
	NS_LOG_UNCOND("BS_Info.size()=" << BS_Info.size()<< "," << "CPEs_Info.size()=" << CPEs_Info.size());
	// NS_ASSERT(false);
	for (unsigned int i = 0; i < CPEs_Info.size(); i++) {
		NS_LOG_UNCOND( i<< "Name:" << CPEs_Info.at(i).location << "x,y,z" << CPEs_Info.at(i).latitude << "," << CPEs_Info.at(i).longtitude << "," << CPEs_Info.at(i).TxAntennaHeight);
	}

	CommandLine cmd;
	cmd.AddValue("scheduler", "type of scheduler to use with the network devices",
			schedType);
	cmd.Parse(argc, argv);

	NS_LOG_UNCOND("Start...");
	switch (schedType) {
	case 0:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
		break;
	case 1:
		scheduler = WimaxHelper::SCHED_TYPE_MBQOS;
		break;
	case 2:
		scheduler = WimaxHelper::SCHED_TYPE_RTPS;
		break;
	default:
		scheduler = WimaxHelper::SCHED_TYPE_SIMPLE;
	}

	NodeContainer ssNodesAlcona[3];
	vector<Node_Data> CPEs_Alcona[3];
	WimaxHelper wimaxAlcona[3];
	InternetStackHelper stack;
	Ipv4AddressHelper address;
	NetDeviceContainer ssDevsAlcona[3];
	Ptr<SubscriberStationNetDevice>* ss[3]; //= new Ptr<SubscriberStationNetDevice>[nbSS];

	Ipv4InterfaceContainer SSinterface_Alcona[3];
	MobilityHelper ssMobility;

	//NetDeviceContainer allNetDevs;

	address.SetBase("10.1.1.0", "255.255.255.0");
	NS_LOG_UNCOND("Sorting CPEs based on their sectNum...");
	for (unsigned int i = 0; i < CPEs_Info.size(); i++) {
		CPEs_Alcona[CPEs_Info.at(i).sectNum - 1].push_back(CPEs_Info.at(i));
		NS_LOG_UNCOND("i:" << i << ", sectNum:" << CPEs_Info.at(i).sectNum << ", TxFreq:" << CPEs_Info.at(i).TxFreq);
	}

	//int end_nums[4];
	//end_nums[0]=0;

	MobilityHelper mobility;

	NS_LOG_UNCOND("Assigning CPEs");
	double min_latitude = 10000, min_longtitude = 10000, min_elevation = 1000;
	double max_latitude = 0, max_longtitude = 0, max_elevation = 0;


	for (unsigned int sectNum = 0; sectNum < 1; sectNum++) {
		unsigned int num = CPEs_Alcona[sectNum].size();
	//	num = 1;
		NS_LOG_UNCOND("num="<<num);
		ssNodesAlcona[sectNum].Create(num);

//	  cout << "end_nums[" << sectNum+1 << "]=" << end_nums[sectNum+1] <<endl;
		//Building SS devices
		//channel = CreateObject<SimpleOfdmWimaxChannel> ();
		//channel->SetPropagationModel (SimpleOfdmWimaxChannel::COST231_PROPAGATION);
		ssDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install(ssNodesAlcona[sectNum],
				WimaxHelper::DEVICE_TYPE_SUBSCRIBER_STATION,
				WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler, &CPEs_Alcona[sectNum],
				true);
		//allNetDevs.Add(ssDevsAlcona[sectNum]);
		/*ObjectFactory queueFactory;
		 vector<Ptr<Queue> > queue = queueFactory.Create<Queue> ();
		 allNetDevs.Get(0)->SetQueue (queue);*/
	//	int nbSS = CPEs_Alcona[sectNum].size();
		ss[sectNum] = new Ptr<SubscriberStationNetDevice> [num];

		//activate loss
		for (unsigned int i = 0; i < num; i++) {
			ss[sectNum][i] = ssDevsAlcona[sectNum].Get(i)->GetObject<SubscriberStationNetDevice>();
			ss[sectNum][i]->SetModulationType(modulation);
			Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(ssDevsAlcona[sectNum].Get(i)->GetObject<SubscriberStationNetDevice>()->GetPhy());
		//	uint32_t datarate = ssPhy->DoGetDataRate(modulation);
//			ssPhy->SetFrameDuration(Seconds(0.005));
			//ssPhy->SetBandwidth(5e6);
		//	uint32_t bw = ssPhy->GetBandwidth();
		//	cout << "Yohoo, Bandwidth:" << bw << ", Modulation: " << datarate << endl;
			ssPhy->ActivateLoss(true);
		} //j

		// end_nums[sectNum+1] = allNetDevs.GetN();
		vector<double> ssAntennaHeightVector;
		vector<string> ssNodeNameVector;
		vector<double> ssTxFreqVector;
		Ptr<ListPositionAllocator> ssPositionAlloc = CreateObject<
				ListPositionAllocator>();

		for (unsigned int j = 0; j < num; j++) {
			ssPositionAlloc->Add(
					Vector(CPEs_Alcona[sectNum].at(j).latitude,
							CPEs_Alcona[sectNum].at(j).longtitude,
							(CPEs_Alcona[sectNum].at(j).elevation) / 1000.0));
			ssAntennaHeightVector.push_back(
					CPEs_Alcona[sectNum].at(j).TxAntennaHeight);
			ssNodeNameVector.push_back(CPEs_Alcona[sectNum].at(j).location);
			ssTxFreqVector.push_back(CPEs_Alcona[sectNum].at(j).TxFreq);

			if(CPEs_Alcona[sectNum].at(j).latitude < min_latitude)
				min_latitude = CPEs_Alcona[sectNum].at(j).latitude;

			if (CPEs_Alcona[sectNum].at(j).latitude > max_latitude)
				max_latitude = CPEs_Alcona[sectNum].at(j).latitude;

			if(CPEs_Alcona[sectNum].at(j).longtitude < min_longtitude)
				min_longtitude = CPEs_Alcona[sectNum].at(j).longtitude;

			if(CPEs_Alcona[sectNum].at(j).longtitude > max_longtitude)
				max_longtitude = CPEs_Alcona[sectNum].at(j).longtitude;

			if ((CPEs_Alcona[sectNum].at(j).elevation / 1000.0) > max_elevation)
				max_elevation = (CPEs_Alcona[sectNum].at(j).elevation / 1000.0);

			if ((CPEs_Alcona[sectNum].at(j).elevation / 1000.0) < min_elevation)
				min_elevation = (CPEs_Alcona[sectNum].at(j).elevation) / 1000.0;
		}


		ssMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
		ssMobility.SetPositionAllocator(ssPositionAlloc);
	  ssMobility.m_antennaHeightVector = ssAntennaHeightVector;
		ssMobility.m_nodeNameVector = ssNodeNameVector;
		ssMobility.m_txFreqVector = ssTxFreqVector;
		ssMobility.Install(ssNodesAlcona[sectNum]);

		/*mobility.SetPositionAllocator("ns3::UniformDiscPositionAllocator",
		  		"X",StringValue("0.0"),"Y",StringValue("0.0"),"rho",StringValue("0.1"));

		mobility.Install (ssNodesAlcona[sectNum]);*/

		//print
		/*for (unsigned int k = 0; k < num; k++) {
			Ptr<MobilityModel> position = ssNodesAlcona[sectNum].Get(k)->GetObject<MobilityModel>();
			NS_ASSERT(position != 0);
		//	Vector pos = position->GetPosition();
			if (k == 0)
				position->SetPosition(Vector(CPEs_Alcona[sectNum].at(k).latitude, CPEs_Alcona[sectNum].at(k).longtitude, (CPEs_Alcona[sectNum].at(k).elevation)/1000));
			if (k == 1)
							position->SetPosition(Vector(32.6804,42.57,0.225));//33.1, 42.5767, 0.225
			if (k == 2)
							position->SetPosition(Vector(32.2, 42.26, 0.221));
			if (k == 3)
							position->SetPosition(Vector(32.62, 42.27, 0.221));
			if (k == 4)
							position->SetPosition(Vector(32.61, 42.26, 0.221));
			if (k == 5)
							position->SetPosition(Vector(32.61, 42.26, 0.221));
			if (k == 6)
							position->SetPosition(Vector(32.61, 42.26, 0.221));//
			if (k == 7)
							position->SetPosition(Vector(32.61, 42.26, 0.221));
			if (k == 8)
							position->SetPosition(Vector(32.61, 42.26, 0.221));
			NS_LOG_UNCOND( "CPE#" << k << " at location " << CPEs_Alcona[sectNum].at(k).location << " TX Freq " << CPEs_Alcona[sectNum].at(k).TxFreq << " at Sector#" << CPEs_Alcona[sectNum].at(k).sectNum << ":, x=" << position->GetPosition().x << ", y=" << position->GetPosition().y << ", z=" << position->GetPosition().z << ", ant=" << position->m_antennaHeight );
		} //for k*/

		//install IP stack on all ss nodes in this sector.
		stack.Install(ssNodesAlcona[sectNum]);
		//address assignment for ss nodes: (added Jul. 25, 2014)
		SSinterface_Alcona[sectNum] = address.Assign(ssDevsAlcona[sectNum]);
	} // for sectNum

	NS_LOG_UNCOND("Done assigning SSNodesAlcona, starting BS assignments...");

	vector<Node_Data> BS_Alcona[3];
	Ipv4InterfaceContainer BSinterface_Alcona[3];
	for (unsigned int i = 0; i < 3; i++) {
		NS_LOG_UNCOND("i="<<i);
		BS_Alcona[i].push_back(BS_Info[i]);
	}
	NodeContainer bsNodesAlcona[3];
	NetDeviceContainer bsDevsAlcona[3];

	for (unsigned int sectNum = 0; sectNum < 1; sectNum++) {
		NS_LOG_UNCOND("sectNum="<<sectNum);
		bsNodesAlcona[sectNum].Create(1);
		vector<double> bsAntennaHeightVector;
		vector<string> bsNodeNameVector;
		vector<double> bsTxFreqVector;

		//mobility.Install (bsNodesAlcona[sectNum]);
		Ptr<ConstantPositionMobilityModel> BSPosition;
		BSPosition = CreateObject<ConstantPositionMobilityModel> ();
/*		BSPosition->SetPosition (Vector(BS_Info.at(sectNum).latitude, BS_Info.at(sectNum).longtitude,
				(BS_Info.at(sectNum).elevation) / 1000));*/
		//bsNodesAlcona[sectNum].Get (0)->AggregateObject (BSPosition);

		NS_LOG_UNCOND("here 2");
		bsDevsAlcona[sectNum] = wimaxAlcona[sectNum].Install(
				bsNodesAlcona[sectNum].Get(0), WimaxHelper::DEVICE_TYPE_BASE_STATION,
				WimaxHelper::SIMPLE_PHY_TYPE_OFDM, scheduler,  &BS_Alcona[sectNum],
				true);

		//allNetDevs.Add(bsDevsAlcona[sectNum]);

		NS_LOG_UNCOND("here 3");
		// bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->
		//  SetDuplex(BS_Alcona[sectNum].at(0).TxFreq,BS_Alcona[sectNum].at(0).RxFreq);
		NS_LOG_UNCOND("BS tx freq" << BS_Alcona[sectNum].at(0).RxFreq);
	  //bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy()->SetChannelBandwidth(
		//		5e6);

		Ptr<SimpleOfdmWimaxPhy> bsPhy = DynamicCast<SimpleOfdmWimaxPhy,WimaxPhy>(bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>()->GetPhy());
		bsPhy->ActivateLoss(true);
//		bsPhy->SetFrameDuration(Seconds(0.005));
		Ptr<ListPositionAllocator> bsPositionAlloc = CreateObject<
				ListPositionAllocator>();
		bsPositionAlloc->Add(
				Vector(BS_Info.at(sectNum).latitude, BS_Info.at(sectNum).longtitude,
						(BS_Info.at(sectNum).elevation) / 1000.0));
		NS_LOG_UNCOND("here 1");
		bsAntennaHeightVector.push_back(BS_Info.at(sectNum).TxAntennaHeight);
//		bsNodeNameVector.push_back(BS_Info.at(sectNum).location);
		bsNodeNameVector.push_back("The_BS");
		bsTxFreqVector.push_back(BS_Info.at(sectNum).TxFreq);
		MobilityHelper bsMobility;
		bsMobility.SetMobilityModel("ns3::ConstantPositionMobilityModel");
	  bsMobility.SetPositionAllocator(bsPositionAlloc);
		//bsNodesAlcona[sectNum].Get (0)->AggregateObject (BSPosition);
		bsMobility.m_antennaHeightVector = bsAntennaHeightVector;
		bsMobility.m_nodeNameVector = bsNodeNameVector;
		bsMobility.m_txFreqVector = bsTxFreqVector;
		bsMobility.Install(bsNodesAlcona[sectNum]);
		Ptr<MobilityModel> position = bsNodesAlcona[sectNum].Get(0)->GetObject<
				MobilityModel>();
		NS_ASSERT(position != 0);
		Vector pos = position->GetPosition();
		NS_LOG_UNCOND("ss Area, min-latitude = " << min_latitude << ", min-longtitude = " << min_longtitude << ", min_elevation = " << min_elevation);
		NS_LOG_UNCOND("max-latitude" << max_latitude << ", max-longtitude = " << max_longtitude << ", max_elevation = " << max_elevation);
		NS_LOG_UNCOND("BS position x = " << pos.x << ", y= " << pos.y << ",Z = " << pos.z);

		NS_LOG_UNCOND("Assigning IP to bs#" << sectNum);
		stack.Install(bsNodesAlcona[sectNum]);
		NS_LOG_UNCOND("Building the BS#" << sectNum << "'s Interface");
		BSinterface_Alcona[sectNum] = address.Assign(bsDevsAlcona[sectNum]);

		/*double txPowerDbm = 30;
		ErcegPropagationLossModel propModel;

		for (unsigned int i = 0; i < CPEs_Alcona[sectNum].size(); i++) {

				ss[sectNum][i] =  ssDevsAlcona[sectNum].Get(i)->GetObject<SubscriberStationNetDevice>();
				Ptr<SimpleOfdmWimaxPhy> myPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(ss[sectNum][i]->GetPhy());
				myPhy->SetTxPower(txPowerDbm);
				myPhy->SetTxGain(12);
				myPhy->SetRxGain(12);
				Ptr<SimpleOfdmWimaxPhy> ssPhy = DynamicCast<SimpleOfdmWimaxPhy, WimaxPhy>(
						 ssDevsAlcona[sectNum].Get(i)->GetObject<SubscriberStationNetDevice>()->GetPhy());
				DoubleValue noiseFigure;
				ssPhy->GetAttribute("NoiseFigure",noiseFigure);
				double rxPower = txPowerDbm + propModel.GetLoss (ssNodesAlcona[sectNum].Get(i)->GetObject<MobilityModel>(), bsNodesAlcona[sectNum].Get (0)->GetObject<MobilityModel>());
				double Nwb = -174 + 10*std::log(ssPhy->GetBandwidth())/2.303 + noiseFigure.Get();
				double SNR = rxPower - Nwb;

				NS_LOG_DEBUG("SNR Value for " << ssNodesAlcona[sectNum].Get(i)->GetObject<MobilityModel>()->m_nodeName << " is " << SNR);

				//FARIBA: Find the SNR Thresholds from MATLAB and assign the corresponding SNR:
				if (SNR < 6.4345 ) //2.3045 < SNR &&
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_BPSK_12);
				}
				else if ( 6.4345 <= SNR && SNR < 9.4939 )
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_12);
				}
				else if ( 9.4939 <= SNR && SNR < 12.2789 )
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QPSK_34);
				}
				else if ( 12.2789 <= SNR && SNR < 15.500)
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_12);
				}
				else if ( 15.500 <= SNR && SNR < 20.300 )
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM16_34);
				}
				else if ( 20.300 <= SNR && SNR < 21.500)
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_23);
				}
				else
				{
					ss[sectNum][i]->SetModulationType(WimaxPhy::MODULATION_TYPE_QAM64_34);
				}
				myPhy->ActivateLoss(true);
		}*/
	} // for base station Alcona sectNum

	NS_LOG_UNCOND("Done assigning BSNodesAlcona.");

	//### Uplink Traffic-TCP--integrity poll response
	//###
	//duration = simulation_time;
//  std::string on_time = "ns3::ConstantRandomVariable[Constant=2.0]";
//  std::string off_time = "ns3::ConstantRandomVariable[Constant=0.0]";
//  uint32_t fid = 0;
	//ServiceFlow::SchedulingType schedulingType = ServiceFlow::SF_TYPE_UGS;

	double unit_data_rate_per_rtu = 0.129; // kb/s
	std::string data_rate; // = "0.129kb/s";// Data rate here needs to be updated according to the number of devices attached to one device
	//uint32_t pktSize = 242; //256;  //242 is for integrity polling, 256 is fro CPE traffic
//  double integStTime = dlIntegStTime + 0.008 - CPEs_Info.size()*0.033;
	//uint32_t maxBytes = 0;
	//std::map <const char*,ServiceFlow::SchedulingType> serviceProfile = CSV_Reading ("20140625 XML_SCADA_MatchSiteNames3Col.csv",CPEs_Info);
	//std::map<const char*, double> numRTUs;
	int tmpNum1 = 3;
	UdpServerHelper* udpServer1 = new UdpServerHelper[tmpNum1];

	int tmpNum2 = 3;
	UdpServerHelper* udpServer2 = new UdpServerHelper[tmpNum2];


	int tmpNum3 = 2; //2; //3 ;
/*	PacketSinkHelper* udpServer3 = new PacketSinkHelper[tmpNum3];
*/

	int tmpNum4 = 0;
	UdpServerHelper* udpServer4 = new UdpServerHelper[tmpNum4];

	for (int sectNum = 0; sectNum < 1; sectNum++)  //3
			{
		//integStTime = integStTime + 0.033;
		int nbSS = ssNodesAlcona[sectNum].GetN();
		NS_LOG_UNCOND("sectNum="<<sectNum<<",nbSS="<<nbSS);
		stringstream s;
		s << unit_data_rate_per_rtu
		/* numRTUs[(const char*)CPEs_Info.at(cpe_num).location.c_str()] */<< "kb/s";
		data_rate = s.str();
		int startPort = 100;
		int fid;
		double interval;
		int pkt_size;
		//integrity polling (can be considered as situational awareness) fid=0
		NodeContainer tmpNodes1, tmpNodes2, tmpNodes3, tmpNodes4;
		Ipv4InterfaceContainer tmpIfs1, tmpIfs2, tmpIfs3, tmpIfs4;
		Ptr<SubscriberStationNetDevice> sstmp1[3], sstmp2[3], sstmp3[3], sstmp4[3];
		for (int i=0; i < tmpNum1; i++)
		{
				tmpNodes1.Add(ssNodesAlcona[sectNum].Get(i));
				tmpIfs1.Add(SSinterface_Alcona[sectNum].Get(i));
				sstmp1[i]=ss[sectNum][i];
		}
		fid=0;
		interval = 60;//60;//0.009375//0.1
		pkt_size = 240;//242

		//tmpNodes1.Get(0)->GetDevice(0)->GetObject<SubscriberStationNetDevice>();
		startPort = setup_udp1(tmpNum1, fid, tmpNodes1 , tmpIfs1, sstmp1,
					bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
					interval, pkt_size, startPort, wimaxAlcona[sectNum], modulation, udpServer1);
		//BE traffic (configuration + Engineering Access Types1,2) fid=1
		for (int i=tmpNum1; i < tmpNum1+tmpNum2; i++)
		{
				tmpNodes2.Add(ssNodesAlcona[sectNum].Get(i));
				tmpIfs2.Add(SSinterface_Alcona[sectNum].Get(i));
				sstmp2[i-tmpNum1]=ss[sectNum][i];
		}
		fid = 1;
		interval = 0.004; //0.004; //0.0027; //0.0082; //0.0027;//0.1
		pkt_size = 512;//512//240
	  startPort = setup_udp2(tmpNum2, fid, tmpNodes2 , tmpIfs2,sstmp2,
					bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
					interval, pkt_size, startPort, wimaxAlcona[sectNum], modulation, udpServer2);
		//Alarm (overnight reports)		fid =2;
		/*tmpNodes = NodeContainer();
		tmpIfs = Ipv4InterfaceContainer();*/
/*		for (int i=tmpNum1+tmpNum2; i < tmpNum1+tmpNum2+tmpNum3; i++)
		{
				tmpNodes3.Add(ssNodesAlcona[sectNum].Get(i));
				tmpIfs3.Add(SSinterface_Alcona[sectNum].Get(i));
				sstmp3[i-tmpNum1-tmpNum2]=ss[sectNum][i];
		}*/
		for (int i=tmpNum1+tmpNum2; i < tmpNum1+tmpNum2+tmpNum3; i++)
		{
				tmpNodes3.Add(ssNodesAlcona[sectNum].Get(i));
				tmpIfs3.Add(SSinterface_Alcona[sectNum].Get(i));
				sstmp3[i-tmpNum1-tmpNum2]=ss[sectNum][i];
		}
		fid = 2;
		//interval = ; //0.004; //0.0027; //0.0082; //0.0027;//0.1
		pkt_size = 1000; //512;
		data_rate = "200kb/s";
		string on_time  = "ns3::ConstantRandomVariable[Constant=3]";
		string off_time = "ns3::ConstantRandomVariable[Constant=40]"; //"ns3::TriangularRandomVariable[Min=0,Mean=300,Max=400]";
		startPort = setup_udp_onoff(tmpNum3, fid, tmpNodes3 , tmpIfs3, sstmp3,
					bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
					on_time, off_time, pkt_size, data_rate, startPort, wimaxAlcona[sectNum],
					modulation);

		for (int i=tmpNum1+tmpNum2+tmpNum3; i < tmpNum1+tmpNum2+tmpNum3+tmpNum4; i++)
		{
				tmpNodes4.Add(ssNodesAlcona[sectNum].Get(i));
				tmpIfs4.Add(SSinterface_Alcona[sectNum].Get(i));
				sstmp4[i-tmpNum1-tmpNum2-tmpNum3]=ss[sectNum][i];
		}
		fid = 3;
		interval = 0.1; //0.004; //0.0027; //0.0082; //0.0027;
		pkt_size = 240;//512

		setup_udp4(tmpNum4, fid, tmpNodes4 , tmpIfs4,sstmp4,
									bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
									interval, pkt_size, startPort, wimaxAlcona[sectNum], modulation, udpServer4);
		//interval = 0.0027;
		/*pkt_size = 1000; //512;
		data_rate = "200kb/s";
	  string on_time  = "ns3::ConstantRandomVariable[Constant=3]";
		string off_time = "ns3::ConstantRandomVariable[Constant=40]"; //"ns3::TriangularRandomVariable[Min=0,Mean=300,Max=400]";
		startPort = setup_udp_onoff(tmpNum3, fid, tmpNodes3 , tmpIfs3, sstmp3,
					bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum], duration,
					on_time, off_time, pkt_size, data_rate, startPort, wimaxAlcona[sectNum], modulation);*/
/*		startPort = 100;
		for (int i = 0; i < nbSS; i++) {
			IpcsClassifierRecord DlClassifierBe (Ipv4Address ("0.0.0.0"),
			 Ipv4Mask ("0.0.0.0"),
			 SSinterfaces.GetAddress (i),
			 Ipv4Mask ("255.255.255.255"),
			 0,
			 65000,
			 startPort + (i * 10),
			 startPort + (i * 10),
			 17,
			 1);
			 ServiceFlow DlServiceFlowBe = wimax.CreateServiceFlow (ServiceFlow::SF_DIRECTION_DOWN,
			 ServiceFlow::SF_TYPE_BE,
			 DlClassifierBe);
			 ss[i]->AddServiceFlow (DlServiceFlowBe);

			IpcsClassifierRecord ulClassifierBe(
					SSinterface_Alcona[sectNum].GetAddress(i),
					Ipv4Mask("255.255.255.255"), Ipv4Address("0.0.0.0"),
					Ipv4Mask("0.0.0.0"), 0, 65000, startPort + (i * 10),
					startPort + (i * 10), 17, 1);
			ServiceFlow ulServiceFlowBe = wimaxAlcona[sectNum].CreateServiceFlow(
					ServiceFlow::SF_DIRECTION_UP, ServiceFlow::SF_TYPE_BE,
					ulClassifierBe);
			ss[sectNum][i]->AddServiceFlow(ulServiceFlowBe);
		}*/
		// wimaxAlcona[sectNum].EnableAsciiForConnection(
		wimaxAlcona[sectNum].EnableAscii("bs-devices", bsDevsAlcona[sectNum]);
		// 	wimaxAlcona[sectNum].EnableAscii ("bs-devices-bs", bsDevsAlcona[sectNum].Get(0)->GetObject<BaseStationNetDevice>());
		wimaxAlcona[sectNum].EnableAscii("ss-devices", ssDevsAlcona[sectNum]);
		wimaxAlcona[sectNum].EnablePcapAll("wimax-program-pcap", false);
		/*	  for (int j = 0; j < nbSS; j++)
		 {

		 IpcsClassifierRecord ulClassifierBe (SSinterface_Alcona[sectNum].GetAddress (j),
		 Ipv4Mask ("255.255.255.255"),
		 Ipv4Address ("0.0.0.0"),
		 Ipv4Mask ("0.0.0.0"),
		 0,
		 65000,
		 startPort ,//+ (j * 10)
		 startPort,// + (j * 10)
		 17,
		 1);
		 ServiceFlow ulServiceFlowBe = wimaxAlcona[sectNum].CreateServiceFlow (ServiceFlow::SF_DIRECTION_UP,
		 ServiceFlow::SF_TYPE_UGS,
		 ulClassifierBe);
		 ssDevsAlcona[sectNum].Get(j)->GetObject<SubscriberStationNetDevice> ()->AddServiceFlow (ulServiceFlowBe);

		 }*/

		/*	//  setup_tcp(nbSS, ssNodesAlcona[sectNum], SSinterface_Alcona[sectNum],
		 bsNodesAlcona[sectNum], BSinterface_Alcona[sectNum],
		 duration, on_time, off_time,
		 fid, startPort);*/
	}

	/*for (int sectNum=0; sectNum <3; sectNum++)
	 cout << "number of nodes in sector" << sectNum << "=" << ssNodesAlcona[sectNum].GetN() << endl;
	 */

	NS_LOG_UNCOND (":-)");  // << stTi=acdghinu[]me);
	NS_LOG_UNCOND ("Starting simulation.....");
	//NS_ASSERT(false);
	Simulator::Stop(Seconds(simulation_time));

	Simulator::Run();

	//bs = 0;
	NS_LOG_INFO ("Done.");

	uint32_t losst1[tmpNum1], sum1 = 0;
	//
	for (int i=0; i < tmpNum1; i++)
	{
		losst1[i] = udpServer1[i].GetServer()->GetLost();
		sum1 += losst1[i];
	}
	cout << "sum1=" << sum1 << endl;

	uint32_t losst2[tmpNum2], sum2 = 0;
	uint32_t sentt2[tmpNum2], sumSent2=0;
	uint32_t rcvdd2[tmpNum2], sumRcvd2=0;
	//
	for (int i=0; i < tmpNum2; i++)
	{
		losst2[i] = udpServer2[i].GetServer()->GetLost();
		sum2 += losst2[i];

		rcvdd2[i] = udpServer2[i].GetServer()->GetReceived();
		sumRcvd2 += rcvdd2[i];

		Ptr<Node> myNode = ssNodesAlcona[0].Get(i+tmpNum1);
		sentt2[i] = myNode->m_Sent[1];
		sumSent2 += sentt2[i];
	}
    cout << "sumSent2=" << sumSent2 << endl << "sumRcvd2=" << sumRcvd2 << endl <<
    		    "sumLost2=" << sum2 << endl;
	/*uint32_t losst3[tmpNum3], sum3 = 0;
	//
	for (int i=0; i < tmpNum3; i++)
		{
			losst3[i] = udpServer3[i]-> //    GetServer()->GetLost();
			sum3 += losst3[i];
		}*/

	uint32_t losst4[tmpNum4], sum4 = 0;
	//
	for (int i=0; i < tmpNum4; i++)
		{
			losst4[i] = udpServer4[i].GetServer()->GetLost();
			sum4 += losst4[i];
		}
	cout << "sum4=" << sum4 << endl;

	//Print Statistics:
	int reliable_received_all_nodes[FLOW_NUM];
	//ns3::Time latency_all_nodes[FLOW_NUM];
	Time Delays_all_nodes[FLOW_NUM];
	Time ReliableDelays_all_nodes[FLOW_NUM];
	int Sent_all_nodes[FLOW_NUM];
	int Recv_all_nodes[FLOW_NUM];
	//int sent_all_nodes[FLOW_NUM];  m_Sent
	for (int flowid = 0; flowid < FLOW_NUM; flowid++) {
		reliable_received_all_nodes[flowid] = 0;
		Delays_all_nodes[flowid] = Time(0.0);
		ReliableDelays_all_nodes[flowid] = Time(0.0);
		Recv_all_nodes[flowid] = 0;
		Sent_all_nodes[flowid] = 0;
	}

	//for 0<=flowid<5 (UL) we need to sum over bsNodes:
	for (int sectNum = 0; sectNum < 1; sectNum++) {
		Ptr<Node> myNode = bsNodesAlcona[sectNum].Get(0);
		for (int flowid = 0; flowid < 5; flowid++) {
			//NS_LOG_UNCOND(sectNum << 	":" << flowid  );
			reliable_received_all_nodes[flowid] += myNode->reliable_received[flowid];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[flowid] += myNode->Delays[flowid];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid] << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[flowid] += myNode->ReliableDelays[flowid];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid]<< "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[flowid] += myNode->Recv[flowid];
			//Sent_all_nodes[flowid] += myNode->m_Sent[flowid];
			//NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
		}
	}

	for (int sectNum = 0; sectNum < 1; sectNum++) {
		for (int flowid = 0; flowid < 5; flowid++) {
				for (uint32_t cpe_num = 0; cpe_num < ssNodesAlcona[sectNum].GetN();
					cpe_num++) {
				Ptr<Node> myNode = ssNodesAlcona[sectNum].Get(cpe_num);
				Sent_all_nodes[flowid] += myNode->m_Sent[flowid];
			}
		}
	}
	//for flowid=5 (DL) we need to sum over all ssNodes:
	int flowid = 5;
	for (int sectNum = 0; sectNum < 1; sectNum++) {
		for (uint32_t cpe_num = 0; cpe_num < ssNodesAlcona[sectNum].GetN();
				cpe_num++) {
			Ptr<Node> myNode = ssNodesAlcona[sectNum].Get(cpe_num);
			//NS_LOG_UNCOND(sectNum << ":" << cpe_num << ":" << flowid  );
			reliable_received_all_nodes[flowid] += myNode->reliable_received[flowid];
			//NS_LOG_UNCOND("relrx: " << myNode->reliable_received[flowid] << "," << reliable_received_all_nodes[flowid]);
			Delays_all_nodes[flowid] += myNode->Delays[flowid];
			//NS_LOG_UNCOND("delays: " << myNode->Delays[flowid].ToDouble(Time::MS) << "," << Delays_all_nodes[flowid]) ;
			ReliableDelays_all_nodes[flowid] += myNode->ReliableDelays[flowid];
			//NS_LOG_UNCOND("relDelays: " << myNode->ReliableDelays[flowid].ToDouble(Time::MS) << "," << ReliableDelays_all_nodes[flowid] );
			Recv_all_nodes[flowid] += myNode->Recv[flowid];
			//NS_LOG_UNCOND("rcv: " << myNode->Recv[flowid] << "," << Recv_all_nodes[flowid] );
		}
	}

	cout << "flow ID\t\tsent\t\treliable rcvd\treliable delay\tall rcvd\tall delay"
			<< endl;
	//cout << Delays_all_nodes[0].ToDouble(Time::S) << endl;
	for (int flowid = 0; flowid < 6; flowid++) {
		cout << flowid << "\t\t"  << Sent_all_nodes[flowid] << "\t\t" << reliable_received_all_nodes[flowid] << "\t\t"
				<< ReliableDelays_all_nodes[flowid].ToDouble(Time::MS)
						/ (double) reliable_received_all_nodes[flowid] << "\t\t"
				<< Recv_all_nodes[flowid] << "\t\t"
				<< Delays_all_nodes[flowid].ToDouble(Time::MS)
						/ (double) Recv_all_nodes[flowid] << endl;
	}

	int Throughput[3];

	for (int sectNum = 0; sectNum < 1; sectNum++) {
		Throughput[sectNum] = 0;
		for (int flowid = 0; flowid < 5; flowid++) {
			Throughput[sectNum] +=
					bsNodesAlcona[sectNum].Get(0)->receivedBytes[flowid];
		}
		for (uint32_t i = 0; i < ssNodesAlcona[sectNum].GetN(); i++) {
			Throughput[sectNum] += ssNodesAlcona[sectNum].Get(i)->receivedBytes[5];
		}
		cout << "Throughput" << sectNum << " = " << (Throughput[sectNum] * 8) / (duration-2) //99
				<< endl;
	}

	int SentSect[3], RecvSect[3];

	for (int sectNum = 0; sectNum < 1; sectNum++) {
		SentSect[sectNum] = 0;
		RecvSect[sectNum] = 0;
		//UL Sent
		for (uint32_t i = 0; i < ssNodesAlcona[sectNum].GetN(); i++) {
			for (int flowid = 0; flowid < 5; flowid++) {
				SentSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->sentBytes[flowid];//m_Sent[flowid]
			}
		}
		//UL recv
		for (int flowid = 0; flowid < 5; flowid++) {
			RecvSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->receivedBytes[flowid];//Recv[flowid]
		}

		//DL sent
		SentSect[sectNum] += bsNodesAlcona[sectNum].Get(0)->sentBytes[5];//m_Sent[5]
		//DL recv
		for (uint32_t i = 0; i < ssNodesAlcona[sectNum].GetN(); i++) {
			RecvSect[sectNum] += ssNodesAlcona[sectNum].Get(i)->receivedBytes[5];//Recv[5]
		}
		cout << "SentNum" << sectNum << " = " << SentSect[sectNum] << "\t RecvNum"
				<< RecvSect[sectNum] << endl;
		cout << "Loss Ratio" << 1-(RecvSect[sectNum] / float(SentSect[sectNum]))
				<< endl;
	}

	Simulator::Destroy();

	return 0;
}
